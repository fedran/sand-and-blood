# *Sand and Blood* by D. Moonfire

This is the public repository of the novel *Sand and Blood* written by [D. Moonfire](https://d.moonfire.us/). It is the first of three novels in the [Rutejìmo](https://fedran.com/rutejimo/) series and starts the young man's path on his rite of passage, a brutal test of exposure to elements, other clans seeking revenge, and his own inner demons.

To read the formatted chapters or download it in various forms, go to the [Fedran project page](https://fedran.com/sand-and-blood/).

> **Can the power of the weak save them all?**
>
> Growing up a disappointment, Shimusogo Rutejìmo has always struggled with proving himself worthy to his family and clan. All he wants is the magic to run faster than the strongest warrior, emulating his brother's strength and courage. When he is once again caught showcasing his poor decisions and ineptitude, he's sent on a quest for his manhood, a discovery of his true bravery and worth.
>
> His journey proves perilous and contrived as the elders who were to guide his endeavors abandon him in the dead of the night, forcing him to forge on without the tutelage he needs to succeed. When danger begins to envelop him, it's up to Rutejìmo to find a way to not only gain inner courage and confidence, but to bravely save the friends he's encountered along way. But he'll need the clan spirit's ultimate speed to conquer the impossible. Can a meek man find the strength to fight for himself?

# Trigger and Contents Warnings

Some themes that appear in this book: bullying, death of named characters, death of anonymous animals, graphical violence, and verbal abuse. There is sexual attraction but no explicit scenes. There is no rape.

# Support

This novel is released for free. If you like it, consider supporting me by reviews or subscribing as a patron:

* [Patreon](https://patreon.com/dmoonfire)
* [Liberapay](https://liberapay.com/dmoonfire)
* [PayPal](https://paypal.me/dmoonfire)
* [Ko-Fi](https://ko-fi.com/dmoonfire)
* [Typewriter Press](https://store.typewriter.press/) (my store)

# Versioning

The current version is **3.1.0**. This book follows [semantic versioning](https://semver.org/) and there is a [detailed list of changes](versions.markdown).

# License

This book is distributed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International license. More info can be found at [https://creativecommons.org/licenses/by-nc-sa/4.0/](https://creativecommons.org/licenses/by-nc-sa/4.0/).

The preferred attribution for this novel is:

> "[Sand and Blood](https://fedran.com/sand-and-blood/)" by [D. Moonfire](https://d.moonfire.us/) is licensed under [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)
