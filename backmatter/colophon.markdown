---
title: Colophon
---

Each chapter of this book was written and edited using Emacs, Atom, and LibreOffice. The files for the novel were managed in a Git repository on a private GitLab instance. Each chapter was an individual file formatted using Markdown. Information and working notes about the chapters were placed into a YAML header at the top of each chapter.

These individual chapters were combined together using a cobbled-together collection of Perl, Python, and Javascript tools that transform the results into XeLaTeX (PDF and print version), EPUB, MOBI, or Microsoft Word.

The cover was created using Inkscape. The color scheme uses eight colors of a monochromatic scale. These colors are shared among all of the Rutejìmo novels. Likewise, the "0100-00" along the spine indicates this is the first published book ("00") with Rutejìmo as the main character ("0100").

The font used on the cover and interior is Corda in various weights and styles.
