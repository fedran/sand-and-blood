---
title: About D. Moonfire
---

D. Moonfire is the remarkable intersection of a computer nerd and a scientist. He inherited a desire for learning, endless curiosity, and a talent for being a polymath from both of his parents. Instead of focusing on a single genre, he writes stories and novels in many different settings ranging from fantasy to science fiction. He also throws in the occasional romance or forensics murder mystery to mix things up.

In addition to having a borderline unhealthy obsession with the written word, he is also a developer who loves to code as much as he loves writing.

He lives near Cedar Rapids, Iowa with his wife, numerous pet computers, and a pair of highly mobile things of the male variety.

You can see more work by D. Moonfire at his website at [https://d.moonfire.us/](https://d.moonfire.us/). His fantasy world, Fedran, can be found at [https://fedran.com/](https://fedran.com/).
