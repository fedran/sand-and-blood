---
title: Fedran
---

Fedran is a world caught on the cusp of two great ages.

For centuries, the Crystal Age shaped society through the exploration of magic. Every creature had the ability to affect the world using talents and spells. The only limitation was imagination, will, and the inescapable rules of resonance. But as society grew more civilized, magic became less reliable and weaker.

When an unexpected epiphany seemingly breaks the laws of resonance, everything changed. Artifacts no longer exploded when exposed to spells, but only if they were wrapped in cocoons of steel and brass. The humble fire rune becomes the fuel for new devices, ones powered by steam and pressure. These machines herald the birth of a new age, the Industrial Age.

Now, the powers of the old age struggle against the onslaught of new technologies and an alien way of approaching magic. Either the world will adapt or it will be washed away in the relentless march of innovation.

To explore the world of Fedran, check out [https://fedran.com/](https://fedran.com/). There you'll find stories, novels, character write-ups and more.
