---
title: Credits
---

I used to do acknowledgements but then I realized there were a lot of people who went into helping me write this book, more than would comfortably fit in a few paragraphs. At the same time, I felt the need to thank them because without their help, this book would remain only in my imagination.

### Alpha Readers

If it wasn't for the Noble Pen writing group, I would have never gotten that far.

* Bill H.
* Ciuin F.
* Mark H.
* Nick T.
* Tyree C.
* Laura W.

### Beta Readers

* Chandrakumar M.
* Kenneth E.
* Marta B.
* Mike K.

### Editors

* Blurb Bitch
* Ronda Swolley
* JoSelle Vanderhooft

### Family

* Susan
* Eli
* Bruce
