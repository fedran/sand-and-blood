---
title: Patrons
---

This book is freely available on the Fedran website at [https://fedran.com/sand-and-blood/](https://fedran.com/sand-and-blood/). It can be reformatted for any device, shared, and even reposted on other websites (with attribution and a link to the original). If someone wants to write a fanfic or create art inspired by the book, they are allowed to do so with relatively few limitations, which are set down by the license described in the previous section.

It is hard to compete with "free" in this day and age. Releasing a book under a Creative Commons license is one way of doing that, but there are still costs associated with producing the results. There are hundreds of hours put into writing it, hiring editors to go through it, and even hosting it on a website. As economics will tell you, there is no such thing as a free lunch. Most of the time, you pay for a book before reading it. Sometimes you have a sample of a few chapters to give you a hint, other times just a blurb. Here, you get the entire piece. If you like it, please consider supporting my writing.

The cheapest way of helping is simply to talk about the book. Post opinions on social networks, write a review and put it up on Amazon or Goodreads, or give a copy to someone who might like it.

The second way of helping is to donate money. Even a dollar helps. There are quite a few ways of doing this: you can buy a print copy; the tip jar at Broken Typewriter Press ([https://broken.typewriter.press/dmoonfire/](https://broken.typewriter.press/dmoonfire/)); or even consider becoming a patron. Patronage provides advance access to works-in-progress, votes on new stories and titles, and input into the world and my writing. You can read more about patrons at [https://fedran.com/patrons/](https://fedran.com/patrons/).

I can only hope that if you like it, you'll help me write the next one.

Thank you.
