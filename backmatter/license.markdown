---
title: License
---

This book is distributed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International license. More info can be found at [https://creativecommons.org/licenses/by-nc-sa/4.0/](https://creativecommons.org/licenses/by-nc-sa/4.0/). This means:

### You are free to:

* Share — copy and redistribute the material in any medium or format
* Adapt — remix, transform, and build upon the material

The licensor cannot revoke these freedoms as long as you follow the license terms.

### Under the following terms:

* Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
* NonCommercial — You may not use the material for commercial purposes.
* ShareAlike — If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.

No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

### Preferred Attribution

The preferred attribution for this novel is:

> "Sand and Blood" by D. Moonfire is licensed under CC BY-NC-SA 4.0

In the above attribution, use the following links:

* Sand and Blood: [https://fedran.com/sand-and-blood/](https://fedran.com/sand-and-blood/)
* D. Moonfire: [https://d.moonfire.us/](https://d.moonfire.us/)
* CC BY-NC-SA 4.0: [https://creativecommons.org/licenses/by-nc-sa/4.0/](https://creativecommons.org/licenses/by-nc-sa/4.0/)
