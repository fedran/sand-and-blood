## [4.0.2](https://gitlab.com/fedran/sand-and-blood/compare/v4.0.1...v4.0.2) (2020-01-02)


### Bug Fixes

* corrected colophon because we use four-digit POV now ([1324043](https://gitlab.com/fedran/sand-and-blood/commit/13240434667281ee6ac919596710cddf93ae20b3))

## [4.0.1](https://gitlab.com/fedran/sand-and-blood/compare/v4.0.0...v4.0.1) (2020-01-02)


### Bug Fixes

* **theme:** updating theme package ([adf28f6](https://gitlab.com/fedran/sand-and-blood/commit/adf28f6f3a7e8af76cff125ba166a4b5e14cc865))

# [4.0.0](https://gitlab.com/fedran/sand-and-blood/compare/v3.1.0...v4.0.0) (2020-01-02)


### Features

* updated theme to newest version ([17ca658](https://gitlab.com/fedran/sand-and-blood/commit/17ca658a14baec4ed7c4e728ec6c8fa443e70c8c))


### BREAKING CHANGES

* theme version is a major change and caused page numbers
to change

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/) and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- Added initial information and formatting files.

## 3.1.0 - 2018-04-08

### Changed

- Removed italics around Miwāfu words in both print and ebook. This was done because they are native words for Rutejìmo and therefore wouldn't be seen or heard differently from his point of view.
- Updated the cover to match the standards from [fedran-covers](https://gitlab.com/fedran/fedran-covers/).

### Removed

- Removed teasers for other books, they don't age well.

### Fixed

- Various typo and formatting changes.

## 3.0.0

### Changed

- Switching publication system to mfgames-writing-js.
- Removed the table of contents from the printed version.
- Simplified the contents for EPUB.
- Removed italics for words since they are native to the main character.
- `chapters/chapter-03.markdown`: Found a plot hole, no one would have talked to Rutejìmo about his mother (see [Raging Alone](https://fedran.com/raging-alone/) and [Sand and Ash](https://fedran.com/sand-and-ash/)).

### Fixed

- `chapters/chapter-01.markdown`: Corrected epigraph attribute in chapter one from "Funikogo Ganósho" to "Funikogo Ganóshyo".

## 2.0.0

### Changed

- Switched over to Creative Commons BY-NC-SA 4.0 International license.

## 1.2.1

### Changed

- Added fancy ligatures back for the titles.
- Changed the build system which had some very minor spacing changes.
- Changed font on chapter and title pages.

## 1.2.0

### Changed

- Switched to Ingram for distribution with a new print ISBN: 978-1-940509-06-8.
- Removed rare ligatures from print versions for readability.
- Italicized the Miwāfu words in both print and ebook.

### Fixed

- `chapters/chapter-03.markdown`: "shimu dépa" to "shimusogo dépa". Thank you, David B.

## 1.1.0

### Changed

- After a review pointed out a number of errors, a second copy editor was acquired and a large number of changes (almost a thousand) were done to improve readability and grammar.

## 1.0.0

### Added

- Initial version.
- Print version was published with Lulu with ISBN 978-1-940509-01-3.
