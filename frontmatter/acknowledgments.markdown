---
title: Acknowledgments
---

Despite the popular image of writers working in dark-lit rooms hunched over a typewriter, it took more than a few people to get this book in your hands.

Firstly, I have to thank Susan. Without her patience and tolerance, I would never have gotten the courage to return to writing. Or to keep writing for over a decade.

To the Nobel Pen writers group, thank you for pushing me, suffering through endless revisions, and for listening to my grand schemes. And to the folks on Reddit who were just as helpful with the final steps. And to Shannon, Mike, Jo, Chandrakumar, and Marta who graciously read the entire piece and gave me feedback.

To Shannon, Allison, Dylan, and Melanie: If it wasn't for your kind words and encouragement, I wouldn't have kept chasing after you.

And finally to JoSelle and Ronda for editing, Dan for the cover illustration, and Matt for putting together the cover.
