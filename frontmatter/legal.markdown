---
title: Legal
---

Copyright © 2014 D. Moonfire\
Some Rights Reserved\
Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International

Cover art by D. Moonfire

All characters, events, and locations are fictitious. Any resemblance to persons, past, present, and future is coincidental and highly unlikely.

Some themes that appear in this book: bullying, death of named characters, death of anonymous animals, graphical violence, and verbal abuse. There is sexual attraction but no explicit scenes. There is no rape.

Broken Typewriter Press\
5001 1st Ave SE\
Ste 105 #243\
Cedar Rapids, IA 52402

Broken Typewriter Press\
[https://broken.typewriter.press/](https://broken.typewriter.press/)

{% if edition.isbn %}
ISBN {{ edition.isbn }}
{% endif %}

Version {{ edition.version }}
