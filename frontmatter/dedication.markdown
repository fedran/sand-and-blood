---
title: Dedication
---

To the English teachers of Prospect High School, Mount Prospect, Illinois.

It took me twenty years to finally thank you properly.
  
Class of '93
