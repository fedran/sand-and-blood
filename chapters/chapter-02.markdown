---
availability: public
when:
  start: 1471/3/28 MTR 4::75
  duration: 0::25
date: 2012-02-19
title: Confession
locations:
  primary:
    - Shimusogo Valley
  referenced:
    - Mifuno Desert
characters:
  primary:
    - Rutejìmo
  secondary:
    - Somiryòki
    - Tejíko
    - Gemènyo
  referenced:
    - Hyonèku
    - Chimípu
    - Rador (Epigraph)
    - Byodenóre
    - Desòchu
    - Shimusògo
organizations:
  secondary:
    - Shimusògo
purpose:
  - Introduce Tejíko
  - Introduce Gemènyo
  - Introduce Rutejìmo lack of talent
  - Introduce style of punishment
  - Second chapter to get used to names
summary: >
  Rutejìmo returned to his grandmother's cave. He tried to sneak around, but she caught him entering. When she asked what he had done, he told her that he had been up at the shrine. His grandmother got upset and began to berate and beat him, chasing him out of the cave.

  Gemènyo interrupted her beating to ask if it was justified. When Tejíko explained the reason, he agreed but she had lost her anger. As she went back into the cave, Gemènyo sat down with Rutejìmo and asked questions. The discussion came to the upcoming rite of passage and how Rutejìmo would gain magical powers once he experienced it. Gemènyo wouldn't give details about the rite, but he did tell Rutejìmo that magic wouldn't change him.

  Rutejìmo snapped back, saying that Gemènyo wasn't the greatest warrior in the clan. Gemènyo didn't seem bothered, but suggested that if Rutejìmo wanted to be the best, he needed to do something. Rutejìmo agreed, even knowing he wouldn't.
---

> It takes a strong man to confess with the knowledge of the punishment that will follow. --- Rador Malastin

Like most of the other clan homes in the northern part of the Mifuno Desert, the Shimusogo Valley ran east-west along the rocky mountains. The valley itself was two miles long with caves cut out of the living rock and paths leading from opening to opening. No one lived along the bottom of the valley among the crops, livestock, and common areas.

Rutejìmo's home was near the top at the middle of the valley. Sun-charged crystals lit up pools of orange and blue illumination along the trail. He jogged as he headed home, running but not hurrying. He wasn't ready to face his grandmother. She had ordered Rutejìmo to bed hours before, and beatings were her favorite form of punishment.

He slowed as he headed up the steep trail leading to his grandmother's home; until he would be considered old enough to live on his own, he slept in one of its side caves. Light poured around the curtains that covered the entrance of the cave. Rutejìmo stopped, took a deep breath, pushed aside the curtain, and peeked inside.

His grandfather, Somiryòki, rarely moved from his favorite chair and spent his days huddling underneath a blanket and drinking tea. He sat only a few feet from the fire that heated the cave, but the years had left their mark on him and he shivered constantly. His back was to Rutejìmo, and Rutejìmo knew he could easily sneak past the former clan warrior.

It was his grandmother Rutejìmo worried about. Tejíko spent her nights sorting through the maps she had created during a lifetime of running for the clan. Her map room had been carved out just inside the entrance to her home, and he could hear the scuff of paper as she moved. Fear shivered down his spine. Where his grandfather was deaf to the world, his grandmother had managed to remain alert late into her twilight years.

Taking a deep breath, he inched past the curtain and crept along the far stone wall. He hoped she wouldn't catch him and he could retreat to his room. He would tell her in the morning before Hyonèku spoke with her.

"Boy," called out his grandmother, "why are you up?"

For a moment, Rutejìmo debated whether or not to respond. He glanced over his shoulder at the opening in the cave that led into his grandmother's den. Not a single bit of stone was visible behind the papers that covered every wall of the square-cut room.

His grandmother sat in the middle of the floor. Bound into a thick tail, her long, white hair snaked down to the ground where she had tied the tip to a carved wooden ring. She wore her sleeping outfit, a heavily embroidered cotton top and bottom. The fabric was white except for the orange trim highlighting her bare feet and hands. She didn't look at him, nor did she stop going through papers, but Rutejìmo knew she was waiting for an answer.

"I…"

She placed a page on a pile. "Speak up, boy, I can't hear through the mumbling."

"I"---he took a deep breath---"I went out."

His grandmother stopped sorting her maps and held herself in mid-motion. Her grip tightened and she crumpled the page in her hand.

Rutejìmo's skin crawled as his stomach twisted. The sudden stillness worried him.

"Did you meet anyone?" Her rough voice was threatening and quiet. A calm before the sand storm.

He straightened and clasped his hands. He took a long, deep breath and squirmed from the tightness in his chest. "Yes, Great Shimusogo Tejíko."

"Who?"

"Great Shimusogo Hyonèku."

"Hyonèku was on shrine duty this evening. He would not be wandering the valley."

Rutejìmo's insides clenched violently. He wanted to throw up or run away. He gulped and forced the words out. "Yes, Great Shimusogo Tejíko."

She peered over her shoulder at him. She had pale-green eyes, the color of the rare leaf that sprouted in the desert. Everyone Rutejìmo knew had green eyes---it was a mark of the desert---but his grandmother's were brighter than most.

For a long moment, she said nothing.

Rutejìmo squirmed as he waited for her response.

His grandmother finished setting down the page. She made a soft, grunting noise as she staggered to her feet. She leaned one hand against a wooden frame as she swayed, then she turned the rest of her body to face him. "Boy," she sighed, "why were you at the shrine?"

"I---" He hoped that honesty would lessen the beating she would give him. "I was trying to take great-grandfather's ashes."

His grandmother's eyes darkened. "You were trying to steal papa's ashes?" Her voice was a growl, rough with age but brimming with the threat of violence. She stepped forward. Rutejìmo stared down at her hands, which were balling into fists.

"Y-Yes, Great Shimusogo Tejíko," he said as respectfully as he could.

She hit him across the face with her palm. The second and third blow caught him on the shoulder and throat. "You inconsiderate, moon-choked bastard of a sand snake!" She yelled as she continued to smack him rapidly.

He staggered back toward the entrance of the cave.

"You don't deserve your clan! Get out! Get out of my home!"

His grandfather looked up, blinked once, and returned to his cup. Any hope for rescue wouldn't come from him.

Rutejìmo's grandmother continued to smack him as she shoved him out the entrance.

"Of all the sun-dazed, childish, self-serving things---" she continued to rail.

Rutejìmo backed away, shielding his head with his arms. His back foot slipped off the ledge of the trail. He grabbed the wooden railing, but almost let go when his grandmother continued to beat him.

"Excuse me," a man interrupted her ranting, "Great Shimusogo Tejíko?"

His grandmother stopped, panting lightly. She spun around to face the newcomer.

Gemènyo's dark-skinned form welled out of the darkness. In the lantern light, the clan courier was a blot of shadows except for bright teeth and the whites around his eyes. Smoke rose from a pipe he held with three fingers. In his other hand, he carried a half-full bottle of what appeared to be fermented milk, the strongest alcoholic drink in the valley. He was slightly taller than Rutejìmo, with curly black hair. Unlike many of the other adult men in the valley, he kept no beard along his brown chin. He wore a pair of trousers but no shirt, his usual outfit for wandering along the valley. The trousers were a deep red, one of the two colors of Shimusògo.

Rutejìmo's grandmother let out an exasperated sigh. "This is none of your business, Gemènyo."

"I just wanted to make sure the screams of a little child were for a good reason."

"He tried to steal Byodenóre's ashes."

"Oh, did he succeed or fail?"

"Failed, of course."

Gemènyo waved his pipe in the air. "Then I agree, a beating is appropriate here. Please, go right ahead, Great Shimusogo Tejíko."

Tejíko turned back to Rutejìmo, who cowered against the railing. The furrows in her brow and the tension in her body faded, leaving only an old woman. She waved her hand. "Bah, he's just a pathetic little worm."

Taking a draw from his pipe, Gemènyo nodded. "Yes. He is." As he spoke, smoke curled from the corner of his mouth.

Rutejìmo blushed at the insult, but said nothing.

Gemènyo turned slightly to Rutejìmo and gave him a wink, stunning the young boy. Then he returned his attention to Tejíko before gesturing to Rutejìmo. "May I?"

Rutejìmo's grandmother narrowed her eyes, but consented with a nod.

Gemènyo strolled over to Rutejìmo. Rutejìmo tensed up, waiting for a blow, but Gemènyo just sat down on the ground next to him and leaned against the railing. "Sit, boy."

Rutejìmo sank to the ground, panting from his efforts. He watched as his grandmother disappeared into the cave. "Sorry."

"For what?"

"Trying to steal great-grandfather's ashes."

Gemènyo chuckled. "Not really. You're sorry you got caught."

Rutejìmo blushed. "Maybe."

"What happened?"

Focusing on the cave entrance in case his grandmother came out, Rutejìmo described his attempt to crawl into the shrine. He stalled when he got to the point where Hyonèku caught him.

Gemènyo nodded as Rutejìmo finished. He tapped his pipe upside down to knock out the remains. Once it was clean he slipped it into his trousers and handed the bottle to Rutejìmo. "Should have gone up the back of the roof."

"I know that now." Rutejìmo paused as he toyed with it. Even from a foot away, he could smell the strong fumes wafting from the bottle. "Wait, does everyone know that?"

Gemènyo grinned and said, "Only those who got caught."

Rutejìmo stared in shock. "You got caught?"

"Yeah, all three times. I only made it out of the shrine once, but they tackled me before I was a chain's distance."

Surprised, Rutejìmo said nothing for a long moment. "I… I just want to show them I'm ready to be a man. That I'm not just…"

"Useless?"

Rutejìmo flushed again and he nodded. He brought up the bottle and sniffed at it. His eyes watered from the smell. He took a tentative taste, pulling a face as it burned down his throat. The second gulp wasn't as bad. He let out a soft gasp as he finished. "I heard that Chimípu has done it twice."

"Three times, actually. That girl is quite good at sneaking. Last time, she also stole Hyonèku's knife when she ran by."

Rutejìmo rolled his eyes and took another gulp. The drink burned in his stomach and he got the urge to cough. "Why can't I be as good as her? Why did she get all the talent?"

Gemènyo raised one eyebrow as he stared at Rutejìmo. He was beginning to go gray along his eyebrows and the sides of his head. "Because you suck rocks."

Rutejìmo froze as he stared in shock at Gemènyo. He was expecting something other than a harsh response.

Gemènyo shrugged and held up his hand. "It's true. You aren't as good as Chimípu. You're a fast enough runner, but you just don't have her strength and determination. I had the same problem. Can you imagine what it was like to grow up with your brother around? To hear the elders going on about how he would be the greatest warrior since your grandfather ran the sands? Like having your face ground into the sand time after time. It never stopped even after we became adults."

"I can be just as good."

"No, you can't."

Rutejìmo folded his arms over his chest. "Yes, I can."

"Then do it. You aren't a man yet." Gemènyo chuckled.

"I will, once I finish the rites."

"Becoming a man doesn't magically change you. What you are today is what you'll be tomorrow. You might make a few changes here and there, but ultimately, you are still going to be the same Rutejìmo you were yesterday. The only difference is that you'll hear Shimusògo and you'll be able to use the clan gifts. But, it won't make you a better man. It won't make you stronger or faster. It will just---"

Rutejìmo scrambled to his feet. "I don't have to listen to this."

"No," said Gemènyo as he looked up at Rutejìmo, "but if you want to be more than just a courier in this clan, you should listen. If you want to be greater than Chimípu, you have to change."

"But you're nothing but a courier, Gemènyo. You aren't the best or even the second best here. You aren't a warrior."

Gemènyo stood up with a grunt. He reached out for Rutejìmo. Rutejìmo flinched, but Gemènyo just patted him on the shoulder.

"Maybe that means I know what I'm talking about, Jìmo. Just think about it. I'm heading up to the shrine to take over for Hyonèku. I'll tell him that your grandmother beat you."

Rutejìmo turned to watch Gemènyo head down the trail toward the shrine house. He balled his fists as he struggled with his emotions, then looked up at the cloudless sky and the lace of stars above him.

He always knew he could be better than Chimípu. Right now, she beat him every time when they raced. And she won every wrestling match. Even when they sparred with knives, she won. The only thing he almost beat her at was hunting bolas.

"Tomorrow, I'll wake up early and train."

Even as he said it, he knew he wouldn't. He made the same promise every time Chimípu bested him. But no matter how passionately he promised, every morning he rolled over and went back to sleep.

With a sigh, he headed back inside to go to bed.
