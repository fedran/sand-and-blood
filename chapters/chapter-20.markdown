---
when:
  start: 1471/3/47 MTR 16::22
date: 2012-09-09
title: Shimusogo Karawàbi
locations:
  primary:
    - Nisanto Finger Stop
characters:
  primary:
    - Rutejìmo
  secondary:
    - Chimípu
    - Pidòhu
    - Karawàbi
  referenced:
    - Mistan (Epigraph)
    - Tsubàyo
    - Mikáryo
topics:
  referenced:
    - The Iron King's Betrayal (Epigraph)
summary: >
  Fueled by his humiliation and guilt when he couldn't fight Mikáryo, Rutejìmo took the weight of Pidòhu's stretcher on himself. When Chimípu tried to take her turn, he refused.

  They planned on stopping at the same rocky outcropping that Rutejìmo attacked Karawàbi. When they got there, they found vultures circling over the camp and Karawàbi sitting in a pool of blood with his throat cut.

  At first, they thought Mikáryo had killed Karawàbi, but Mikáryo used a different weapon. Rutejìmo got sick seeing the violence.

  They took the remaining supplies and left in a hurry.
---

> In the end, the cruel get their comeuppance, but rarely do victims cheer. --- Mistan Palarin, *The Iron King's Betrayal* (Act 3, Scene 2)

By midday, Rutejìmo was exhausted. He strained to pull Pidòhu. His back screamed out in agony, and his legs were on fire. But he couldn't stop pulling.

"Damn it, Jìmo, let me carry Pidòhu."

"No!" he gasped, and forced his feet forward.

"You've been dragging him all morning. You need to let me---" Chimípu reached out for the handles.

Rutejìmo lurched to the side to avoid her and almost fell over. Sweat ran down his face, and he regained his footing. Glaring at her, he forced himself to drag the frame farther along. They were almost up to the point where Tsubàyo, Karawàbi, and he stopped the first night. He knew there was shelter, and this time, when he arrived, it would be with pride instead of shame.

"Damn the sands, Jìmo. Let me!"

"No!" he said.

"Why not!?" Her voice was shrill and tense.

"Because you need your strength."

"For what?"

"Mikáryo."

Chimípu stopped and stared. "Is this what this is about? Look, Jìmo, everyone gets scared, and it isn't your fault that you---"

Rutejìmo closed his eyes tightly. "Please don't finish that sentence."

Chimípu sighed and paced him. "What is it then?"

"You…." He gasped and trudged forward. His foot slipped, and he dropped to one knee. With a sigh, he slumped. "I… can't do that." He looked up, his heart tearing as he spoke. "I can't fight for us. I can't do the same things as you. But I can do this. And if I'm going to be helpful, then let me do what I need to do."

She crouched down next to him. "Jìmo, you don't---"

"No. I do," he pleaded, "Please. Let me do this. You can't do everything."

Chimípu's gaze softened, then the corner of her lips quirked up. "Pidòhu's been gossiping, hasn't he?"

Pidòhu craned his neck to look at them. "Just making observations."

Chimípu leaned over and smacked him playfully on the shoulder.

With a chuckle, Pidòhu batted her back, but it was a weak, helpless strike.

"So," Chimípu asked both of them, "if I'm going to be the great defender of this pathetic group of clan members, what should I do?"

Rutejìmo shrugged and caught his breath. "I don't know. I'm still working on holding up my share."

She smiled at him and gave his shoulder a smack. "Not doing that bad at all, Jìmo."

Rutejìmo's heart skipped a beat with joy. He smiled and rubbed his shoulder where it stung.

"Well, if you are done beating on each other," Pidòhu said as he pointed past them, "maybe Great Shimusogo Chimípu could find out why there are vultures circling over the rocks we're heading for."

Rutejìmo and Chimípu looked in the direction he pointed. Six vultures sailed in a lazy spiral and a dozen more hopped on the rocks. They were staring down at the camp. Occasionally one would flap its wings and cry out.

Chimípu stood up. "You said the camp was there, right?" She asked as if she hoped Rutejìmo would say no. "Maybe it's just food rotting."

"M-Maybe." But Rutejìmo had a bad feeling in his gut.

"I---" Chimípu stroked the knife at her belt. "Why don't I go check?"

She jogged forward about a rod, then accelerated in a blast of air. Her sprinting left a trail of dust behind her, and it bloomed into a cloud before the desert wind dispersed it.

Rutejìmo sighed and grabbed the handles. A blister on his hand broke, and he winced at the pain, but still wrapped his fingers around the wood and lifted it up. "Come on, Dòhu."

Pidòhu grunted. "Thank you, Rutejìmo."

Rutejìmo smiled and dragged the stretcher along, watching the rocks with fascination.

A few moments after Chimípu arrived, the vultures took off. They rose and joined the others, spiraling like a miniature tornado over the rocks. Their screeches were loud and piercing, and soon there were more circling around.

Chimípu came back at a high-speed sprint. She came to a long halt, exploding one dune before she stopped less than a few feet in front of Rutejìmo. She held out her hand and shook her head. Her face was pale, and she looked shaken. "No, circle around. We can't go there."

Rutejìmo stopped. "Why?"

"It was… it's…." Her face was pale and she gulped. "Karawàbi. He's dead."

Rutejìmo dropped to his knees in shock. He almost dropped Pidòhu, but clutched the handles tightly at the last minute. "D-Dead? How?" The world spun around him, and he almost threw up.

"S-Someone cut his throat." There was a terrified look on her face.

Rutejìmo gasped, and the blood drained from his face. "Who?"

Gulping, Chimípu shook her head and clutched her stomach.

"H-How? Why?"

"I-I don't know. There is blood everywhere and… and…." She shook her head again. "No, I can't."

Rutejìmo knelt there, stunned for a long moment.

Pidòhu broke the silence. "Are the tents still there?"

Surprised, Rutejìmo stared at him.

Pidòhu, already pale, shrugged. "I've seen a lot of blood lately. Most of it mine. If it wasn't for you, I'd be dead, so…. I guess I'm being practical here. If there is something that can help us, one of us has to go and get it. I had medicine in my pack and I need it."

Chimípu whimpered. "I can't go back, not without…." The words failed her.

Rutejìmo took a deep breath. "What if we all go?"

She looked at him with hope.

Rutejìmo shrugged as casually as he could, but his stomach was twisting left and right with every passing second. "Come on," he grunted as he picked up the frame again, "before I lose my courage."

All three of them headed into the camp. There was a bittersweet smell in the air, a tickling sweetness of spoiled meat. It surrounded the rocks and fouled the air.

As he dragged Pidòhu in, Rutejìmo stopped by the three tents. One flap fluttered in the air and sand had piled inside it, but he could see rations and a blanket inside.

The smell was stronger around the corner, in the shadows under the rocks. Rutejìmo set down the stretcher and headed for the tents. He stopped at the first one and emptied it out, setting the supplies on the ground before working to take down the tent itself.

Chimípu started on another in silence. Neither said anything about the sickening smell or the insects that buzzed around them. Shadows circled around the corpse: the vultures waiting for their dinner, and they were impatient.

Rutejìmo found Pidòhu's pack. Inside, there were a number of medicine packets. He carried them over to Pidòhu and set them down. He didn't want to speak.

Pale and shaking, Pidòhu dumped them out and began to sort through them.

Once he was sure Pidòhu was set, Rutejìmo headed to his tent and tore it down. Chimípu finished and joined him.

The heat bore down on him, and he was sweating as they finished the third tent. He stood up to stretch and caught sight of Karawàbi's corpse.

The large boy was leaning against the shade of the rock as if he was taking a nap. His head lulled to the side but the angle was wrong. Someone had cut across his throat, slicing deep enough that Rutejìmo saw the flash of white bone. Karawàbi's shirt was soaked in dried blood, and it stained the sand in all directions. More splatters discolored the sand over a yard away.

Surprised by the sight, Rutejìmo spun around as bile rose up in his throat. He staggered to the side and vomited on the ground. A few yards away, he noticed that Chimípu had done the same thing. Sobbing, he closed his eyes as he emptied out his stomach. Soon nothing came but dry heaves.

Chimípu knelt next to him. "Calm down, Jìmo. Calm down." She was almost tender as she patted his back. "He's dead. Just look away. Don't think about it."

Gasping, Rutejìmo braced himself. "Who could have done that? Mikáryo?"

"No," said Chimípu, "I don't think so. When we were fighting, she used the tazágu and I didn't see any knives. Whoever killed Karawàbi used a straight blade; the wound is too clean and deep."

Rutejìmo looked at her, surprised and fearful of the haunted tone in her voice. "How close did you get?"

"Close enough," she said in a tone that didn't encourage questions.

Rutejìmo started to glance at the corpse again, but Chimípu grabbed his head and turned him away.

"No, Jìmo. Don't look."

"Who could have done it? Tsubàyo?"

Chimípu frowned. "He wouldn't kill clan."

"He isn't clan," Rutejìmo said, remembering Tsubàyo's bitter words outside the camp. "He was turning his back on Shimusògo."

Her lips tightened into a thin line. "I forgot you told me that. But he can't walk away. Even if he did, Karawàbi was his friend. He would have asked Wàbi to join him, not cut his throat."

"I don't know, Mípu. I don't know."

From the other side of the camp, Pidòhu called out, "Um, could we get out of here… now?"

No answer was needed.
