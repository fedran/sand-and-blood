---
when:
  start: 1471/3/46 MTR 11::58
date: 2012-09-06
title: Pushing Forward
locations:
  primary:
    - Three Falls Teeth
characters:
  primary:
    - Rutejìmo
  secondary:
    - Chimípu
    - Pidòhu
    - Shimusògo
  referenced:
    - Byotsúma (Epigraph)
    - Karawàbi
organizations:
  secondary:
    - Shimusògo
topics:
  secondary:
    - Kyōti Voting
summary: >
  Pidòhu started to hallucinate from his injuries. Knowing that he was going to die in the desert, he begged Rutejìmo to take him home. Rutejìmo agreed. Chimípu returned from getting supplies and demanded to know what was going on. Pidòhu negotiated his case and she agreed. They made a makeshift stretcher to pull him along and decided to take turns pulling him across the sand.
---

> Revealing one's voting stones to the sun is a deeply personal decision that can never be taken back. --- Ryugamiku Byotsúma

Rutejìmo yawned as he came back from answering the call of nature behind the far rocks of the camp. He was exhausted from the night, both from being awake during his watch and the uneasy sleep plagued with guilt. His feet crunched on the sand, and he wished he was back at home in the valley, ignorant of the last few days. If he had to do it again, he would try harder to listen to the lessons everyone had been trying to teach him. But, even as he walked across the sands, he knew he wouldn't have. Just as he never woke up early to train after Chimípu humiliated him.

He stopped when he caught sight of Pidòhu. The frail-looking boy was huddled underneath blankets, wiping the sweat from his brow with a shaking hand. A second later, he did it again and stared at the droplets running down his hands with unfocused eyes.

Fear clutched Rutejìmo. He wondered if he was seeing someone die in front of him. He knelt in front of Pidòhu. "Are you okay?"

"I---" Pidòhu looked over Rutejìmo's shoulder, but when Rutejìmo glanced over expecting to see Chimípu, he saw nothing but sun-baked sand. "I keep seeing shadows. They are running across the desert, but they never get to me. I-I'm so cold." He shivered and clutched himself.

A frown marring his brow, Rutejìmo rested the back of his hand against Pidòhu's forehead. It was soaked with sweat and searing hot. The heat rolled off the injured boy, but it was a wet, sick heat instead of the burning dryness of the desert sun.

Pidòhu bit back a sob, tears shimmering his eyes. "All I see are shadows, Jìmo."

"I-I think I need to get Chimípu." Rutejìmo started to his feet, but Pidòhu grabbed him.

"No, Jìmo. Don't go."

Feeling himself on the edge of tears, Rutejìmo knelt back down. "Dòhu, I don't know what to do. I… don't know anything."

"I'm getting sick."

"Okay, that part I figured out." Rutejìmo rolled his eyes, "But what do I do with this? With you?"

Pidòhu frowned and then wiped his face. "I… move me. Take me home."

"We're days away. Won't it be safe to stay here?"

Pidòhu gave him a weak smile, his body swaying. "I think I know what happens if I stay. Don't you?"

Rutejìmo gulped. He'd heard stories about couriers dying, but it was always a dramatic death in delivering a final message. There was never a heroic story about a Shimusògo dying in the shadow of a rock, unable to move. Rutejìmo squirmed uncomfortably.

"Jìmo, I know we can't make it. But, if I stay here, I'm going to die. I'm going to get weaker. I don't want this to be my grave." He gestured up to the rock.

"We won't make it."

"Better to die on the move than in the shadows of a rock. But"---he gave Rutejìmo a weak smile---"I don't want to die. There is a chance to make it, to get home. I know I'll be okay if we do."

Looking into Pidòhu's pleading eyes broke his hesitation. "I'll do it, Great Shimusogo Pidòhu." It felt a little easier to be respectful.

Pidòhu's eyes trailed to the side. "Shadows. All I see are shadows across the sand." He slumped back and closed his eyes. "Please, Jìmo," he said in a broken whisper, "just take me home."

Rutejìmo got up and eased the tent from around him. He pulled out the poles and began to lash them together into a narrow frame. Pidòhu was too heavy to carry and with his broken leg, he wouldn't be able to ride on Rutejìmo's back.

Pidòhu woke up after a few minutes. He gave short, gasping directions when Rutejìmo faltered.

He was almost done when Chimípu came running up in a cloud. Rutejìmo could almost see a dépa in the dust, jumping and sprinting ahead of her. As she slowed down next to him and the wind of her passing enveloped Rutejìmo, the dépa disappeared.

"What are you doing?" she asked angrily.

Rutejìmo couldn't look at her. He continued working the remains of his pack, which he had torn apart and formed into a pad, between the ropes binding the rods together. "Taking Pidòhu home."

Chimípu tossed down two gutted snakes. She turned on Pidòhu. "Dòhu!" Her voice was brimming with frustration and exhaustion. "We can't move you!"

Pidòhu looked up, his eyes rolling slightly. "I'm not going to make many more nights."

"Then stay here with Jìmo. I'll run and get help."

"Even you know that lesson. Shimusògo never travel alone across the desert. An hour out, maybe, but never the long days alone."

"Damn that! You can't survive the trip!"

The wind kicked up around them, peppering Rutejìmo with tiny grains of sands. He saw a flash of movement, but when he spun around, there was nothing but sun and sand and rock.

Pidòhu wiped his brow again. "I might not survive the night. Mípu, please, at least let me die while moving."

She stood there, hands balled into fists. "Don't give up, Dòhu. We'll make it. I'll find something that will help, I prom---"

"I'm not giving up, Mípu. And that is why we're moving." A look crossed his face and he smiled. "Pretend I'm a package, if you want."

A tear ran down her cheek. "No, don't do that. Don't make me."

He smiled, the pale brown of his skin looking uncomfortably like a skull to Rutejìmo. "Shimusògo always delivers." It was the clan's motto on the job, a phrase Rutejìmo never understood until he heard Pidòhu speaking and realized he meant his body, alive or not.

Pidòhu dug into his pocket and pulled out a few pyābi. There were enough red coins to buy a sweet from a market but nothing else. The metal glinted in the sun. "Please, Great Shimusogo Chimípu. Don't let me die here."

Rutejìmo held his breath as he watched Chimípu shake her head. "No, I can't lose you."

"Please?"

She held out her hand. It shook violently. Her fingertips caressed the metal, and then she pulled back. "No, I will not take money from my clan."

Pidòhu sighed.

Rutejìmo stood up, ready to take it.

"But." Her words were soft, but they stopped Rutejìmo with the force of a punch. "But, Pidòhu, I will borrow your money until we get home safely."

Pidòhu smiled and held up his change.

She dropped to her knees. Taking his hand, she kissed his palm as she took the money. "I promise, Pidòhu. You'll make it home, one way or the other. Not as a delivery but as clan."

Rutejìmo turned his back on them, his stomach twisting uncomfortably. He returned to the frame and tested each wrapping before moving to the next.

A minute later, Chimípu knelt down on the far side. She tossed a small bag of salted meat and a water skin at his feet. "Eat, boy."

"Chimípu…." Her red-rimmed glare silenced him. "Yes, Great Shimusogo Chimípu."

He ate in silence, watching as Chimípu finished up his work.

"We'll drag in shifts," she said in a terse voice. "Ten minutes, a half hour tops. Stop before it begins to hurt too much. We stop at the top of dunes when we trade off." She didn't look at him, and he felt a prickle of annoyance at her commands but shoved it aside. She wasn't giving orders for herself. They were for Pidòhu.

"What if we take each end?"

"Not over the sands, it is too hard to keep balance with three, and we might tip him. Do you remember the route you took?"

Rutejìmo nodded. "Yes, but what about Karawàbi?"

"We'll deal with him when we get there. But, you remember where there is shelter? Rocks, outcroppings?"

He thought back the last few days before. "I do, but we were only a day away."

Chimípu ran her hand down the padding of the frame. "It will take us two, maybe three, days to get that far. At least we'll know where we can take shelter."

Rutejìmo nodded, the food sinking into his stomach like a rock. He finished gathering the remains of the packs, using the ropes on the frame to secure their supplies.

Pidòhu caught his attention and he went over. Kneeling down, he stared as Pidòhu poured his voting stones into Rutejìmo's palms.

"Don't lose them," Pidòhu whispered.

Rutejìmo nodded. He pulled his own rocks from his pocket, which he had discretely transferred when he ripped his pack open. Blushing hotly, he held the two sets in his palm. His own voting stones were plain, a gray rock with an interesting pattern of white that looked like ribs of a bird. It was a stark contrast to Pidòhu's rocks with the embedded gears.

He knew he should keep his a secret. His choice of stones was intimate, a personal decision that had somehow been exposed to the sunlight. He gulped as he stared down at the rocks; it felt forbidden to show them to anyone before he earned the right.

Chimípu dropped three silver rings into his hand. Each one depicted in incredible detail a shimusogo dépa running in endless circles. He had seen them before---they were Chimípu's mother's voting stones. Surprised, he looked up at Chimípu.

She gave him a sad smile. "Keep them all together. Like a clan, we all come home or none of us do."

Speechless, Rutejìmo nodded. He crawled over to the frame and secreted the rocks and rings in a secure pocket. He doubled a spare shirt over the stones to ensure they wouldn't be lost even if the frame was upended.

He and Chimípu carried Pidòhu to the stretcher. When they set him down on it, Pidòhu hissed from pain and clutched at his knee.

"Sorry," Rutejìmo said.

"No, just continue."

"Lean back," ordered Chimípu.

When Pidòhu did, they bound him against the frame. They wrapped ropes around his chest and legs, careful to immobilize his broken leg as much as possible while giving him freedom to move his arms and head.

Pidòhu bore the discomfort in silence, a nervous look on his face. When Rutejìmo stepped back, Pidòhu gave him a thin smile. "Not so bad. I feel like a king with you two."

"Well, king," Chimípu said with a sly grin, "if you give too many orders, you can walk home."

It was a weak joke, but they all laughed anyways.

Rutejìmo pressed the full water skins into the crook of Pidòhu's arm.

Chimípu took the stretcher first, wrapping her hands around the ends and grunting as she picked them up. She gave it a hesitant tug. When it didn't move, she leaned into pulling it across the sand.

It made a loud scraping sound, but then began to slide.

She took a deep breath and bore down, dragging him up the dune.

Rutejìmo followed behind the two, to catch either if they fell.
