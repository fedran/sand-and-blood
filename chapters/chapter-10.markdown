---
when:
  start: 1471/3/43 MTR 18::68
date: 2012-06-22
title: Separation Anxiety
locations:
  primary:
    - Ojyukafumi Passage
characters:
  primary:
    - Rutejìmo
  secondary:
    - Karawàbi
    - Tsubàyo
    - Shimusògo
  referenced:
    - Marfun (Epigraph)
    - Pidòhu
    - Chimípu
organizations:
  secondary:
    - Shimusògo
topics:
  referenced:
    - The Failure of Innovation (Epigraph)
summary: >
  Tsubàyo, Karawàbi, and Rutejìmo ran toward home. Rutejìmo fell behind and the other two didn't help him. Instead, he struggled until he saw a bird that only appeared if he ran fast enough. It was enough to keep him going and he found that he could run faster, fast enough to catch up with the others. When neither Tsubàyo or Karawàbi could see the bird, Rutejìmo realized it was the clan spirit, Shimusògo.
---

> Inspiration rarely comes when waiting for it. --- Marfun Golem, *The Failure of Innovation*

"I can't believe that bitch!" Tsubàyo led the way along a dune. He was in high spirits, despite the fresh bruises on his arms and a scratch that bisected his scar tissue. He swung Pidòhu's bag in his hand as he headed along a route only he could see.

Karawàbi followed closely behind. He had his own pack over one shoulder and Tsubàyo's over the other. He didn't have any bruises or scratches, but Rutejìmo noticed a faint limp when he hit the ground at the wrong angle. The large teenager moved in relative silence, only grunting when Tsubàyo asked a question.

Rutejìmo could only imagine that both of them took on Chimípu while Rutejìmo was trying to stop Pidòhu's bleeding. None of them talked about it, but he could see it in their injuries and the way they avoided referring to what happened with Chimípu.

He wasn't sure why they had fought her or why they insisted on leaving, other than Chimípu's saying they had to keep together---his brother used to say something similar, that the clans clung to themselves at night for safety. He tried to tell himself that he had to choose sides and that Chimípu had rejected him. It didn't ring true and he remained silent, lost in guilty thoughts and regret.

It was a few hours after noon. The sun bore down on them, and Rutejìmo could feel it searing along his skin. There was a breeze, but it was hot and thick with sand. It left a foul taste in his mouth, and he had to pretend it wasn't from leaving behind two clan members in the shadows of the Wind's Teeth.

He peeked over his shoulder. The rocks were only a dark dot on the horizon. Their path was almost hidden with the searing winds that blew across the dunes. To his right, the air hazed over with the heat.

"Don't give her another thought, Jìmo."

Tsubàyo stood on top of a ridge and watched him with a dark look in his eyes. The scars from his childhood injury gave his face a rippled pattern not unlike the dunes they had been climbing for the last few hours.

Rutejìmo licked his lips, feeling the cracked edges with his tongue. "What about Pidòhu?"

Karawàbi shrugged. "He's dead, he just doesn't have the sense to know it."

Tsubàyo nodded in agreement.

Rutejìmo glanced back the way they had come.

"Jìmo," warned Tsubàyo, "I said stop thinking about it."

"Did we do the right thing?" Rutejìmo knew the answer before Tsubàyo opened his mouth.

"Got rid of dead weight?" Tsubàyo mimed thinking as he tapped his forehead. "Yes, we did. Now we don't have to deal with a worthless runner and a stuck-up bitch that thinks she knows everything. We are better off."

Rutejìmo didn't have the same confidence.

"Come on, we have another good hour and we can set up camp by that rock." Tsubàyo pointed to the south where rocks stuck out from the sand like shattered toenails.

Rutejìmo almost looked back again, but caught Karawàbi watching him. Blushing, he forced himself to trudge forward.

"You know what?" Tsubàyo declared, "We're Shimusògo, let's run." Without waiting for a response, Tsubàyo jogged down the dune.

Karawàbi groaned, and both he and Rutejìmo ran after Tsubàyo to catch up. With their physiques and endurance, their run quickly became a line with Karawàbi following right behind Tsubàyo as Rutejìmo struggled to keep up a few rods behind. And he felt more alone than he had ever felt before.

He missed Pidòhu. No matter how slow Rutejìmo ran, he knew he would beat the slender boy. It was different being the slowest runner, and he felt a bit of sympathy for Pidòhu.

As he stumbled down one of the slopes of sand and rock, he caught movement in the corner of his eye. He stopped and looked, but he saw nothing but the dark swirls of sand on sand. He turned in the other direction but there was nothing.

"Jìmo?"

Rutejìmo shook his head. "Thought I saw something."

"Probably just a snake," said Tsubàyo. "Hurry up."

Shaking his head again, Rutejìmo returned to jogging. He struggled on the upward slope of the next dune. By the time he reached the top, he was sweating and gasping for breath.

Karawàbi and Tsubàyo were already ahead of him, and he let out a groan before chasing after them. His legs hurt from the effort but he knew there was no way they would slow down for him. Annoyance prickled along his thoughts as he glared at Tsubàyo's back.

He caught a hint of movement again. This time, he didn't stop but glanced over without slowing.

It was a bird racing him, just a few paces ahead of him and to the right. The avian glided across the sand on long legs. Its three-toed claws didn't leave a trail behind it or disturb the sands with the breeze of its passing. It kept its short wings tight to its body as it ran. A brown-and-white speckled pattern ran from its crest down to the end of the long feathers that formed the tail. It was a shimusogo dépa, the dune-runner bird Shimusògo took his name from.

Startled, Rutejìmo slowed down, and between one step and the next, he lost sight of the bird. Desperate, he tried to maintain his speed while looking around but the dépa was gone. He slowed down further, peering over his shoulders for the bird.

As he came to a stumbling halt, all the joy fled out of him. In one moment, he was experiencing a high and in the next it was gone. It wasn't until it was missing that he realized he was experiencing it. In its wake, a longing burned inside him. He wanted to run, something he had never felt before, and the urge sang through his veins.

Rutejìmo frowned and turned in a circle. The dépa was gone and Tsubàyo and Karawàbi were quickly outpacing him. The need to run burned hotter. In the back of his mind, he knew that if he just ran fast enough, the dépa would return. The knowledge, however, frightened him since he had never had an urge to run before, nor had he ever seen a dépa while running.

The other two out-distancing him finally pushed him to take the next step. He did and the longing burned brighter. He stumbled before moving into a jog, but it wasn't enough. He needed to run; it was a foreign craving that screamed to him to move faster. Disturbed and frightened, Rutejìmo gave in to his needs and threw himself into a full sprint.

Between one step and another, the dépa rushed past him and settled into place a rod before him. He found his rhythm and chased after it, but the bird continued to run just ahead of him. With every step, he felt a bubbling excitement stretch up his legs. It wasn't the burn of effort but something else, a drug that came from chasing a bird that didn't exist.

The sound of feet on sand drew his attention away from the dépa. In his attempts to catch it, he had caught up with Karawàbi. Rutejìmo wondered if they had slowed down, but both of them appeared to be running as fast as before.

He stumbled over a rock and came to a halt. As he did, the dépa disappeared before his eyes. Panting, he watched as Tsubàyo and Karawàbi continued to race ahead, neither looking back for him.

Rutejìmo started to jog after them. This time, he concentrated on the fleeting sensations inside him. With every step, the excitement returned and encouraged him to run faster. He pushed himself and the thrill of anticipation was almost as strong as the euphoria he felt when the dépa reappeared. The aches and pains of running for days faded away, leaving only a rush of excitement and a wind in his face

For the briefest moment, he felt a connection to something incredible, a wordless sensation of being part of a force far larger than himself. It was a connection to the unknown and every step brought him closer.

As the epiphany was about to reveal itself, he tripped.

Rutejìmo gasped as he landed face-first into the searing-hot dune. The sun-baked sand burned his chest, throat, and arms. He choked on it as he struggled to his knees. Automatically, he looked for the dépa but it was gone. He sighed and got back on his feet.

He started forward again, racing for his new-found rush and the appearance of what he assumed was his clan spirit, Shimusògo.
