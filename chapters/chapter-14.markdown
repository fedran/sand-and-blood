---
when:
  start: 1471/3/44 MTR 17::28
date: 2012-08-26
title: Coming Back
locations:
  primary:
    - Three Falls Teeth
  referenced:
    - Shimusogo Valley
    - Wamifuko City
characters:
  primary:
    - Rutejìmo
  secondary:
    - Chimípu
    - Pidòhu
    - Shimusògo
  referenced:
    - Desòchu
    - Tsubàyo
    - Karawàbi
organizations:
  secondary:
    - Shimusògo
  referenced:
    - Metokāchyu # Scorpion builders
    - Pabinkúe
    - Kosòbyo (Epigraph)
topics:
  secondary:
    - Rabedájyo # The Knife
summary: >
  Rutejìmo returned to the Wind's Teeth. Pidòhu was happy to see him, but Chimípu was furious. She berated him for leaving until Rutejìmo offered his throat, to give her a choice to kill him or let him stay. Chimípu saved him and sullenly let her help.

  Rutejìmo got to see the damage done to Pidòhu, it was a compound fracture. When Chimípu ran off to find some help, Pidòhu helped Rutejìmo change his bandage.
---

> It takes a strong man to admit a mistake to a strong woman. --- Kosòbyo Proverb

Even following the dépa, Rutejìmo didn't think he would ever make it back to Chimípu and Pidòhu. Alone in the desert with the sun baking down and body aching from head to toe, every step was a struggle. But, if he dared to slow down, the dépa disappeared and he was forced to run bereft of the bird's company. Only when he pushed himself to run near his limits would the bird appear.

When he saw the Wind's Teeth, he almost sobbed with relief. It was early afternoon as far as he could tell, but he couldn't stop to look at the sun. Every stop meant he had to struggle to move fast enough to summon the dépa again. He bore down, pushed past the exhaustion, and drove himself toward the tiny, dark marks that would grow into the towering rocks.

He recognized his approach as he crested the dune. It was the same route they had taken the first time they approached the Wind's Teeth. Desòchu and the rest of the clan ran with him then; now he was alone. The contrast of the two days was painful and Rutejìmo wished he was still struggling to catch up instead of coming back to two people he abandoned.

Coming to a stop a few rods away from the Tooth, he called out, "Hello?"

There was no answer.

Panting for breath, Rutejìmo headed straight to where they had set up the tents two nights before. His feet scrunched against the sand blown up against the rocks, and the wind teased his face. As he walked, he felt the muscles in his neck and chest tightening---not from his run, but from anticipation and fear.

He hurried over to where the tents had been. The rocks prevented the wind from erasing the tracks. He could see his own faint footsteps from when he took down his tent, a swirl from where the others fought, and even a fresher trail going back and forth from Chimípu's tent and toward the rock where Pidòhu fell.

Rutejìmo stared down at the last trail. There was evidence of more than a few trails back and forth. He followed the paths, keeping his eyes on the footsteps precariously imprinted on the shifting sand. He didn't know what he would do if there was no tent or---his stomach lurched at the thought---if Pidòhu didn't make it through the night and he was walking toward a corpse.

Doubt burned brightly, and he clutched his stomach from the pain. He was afraid of everything he would find around the corner: blood, death, or even Chimípu accusing him of abandoning her. He wanted to run way, to grab his small pack and just start running. But, there weren't enough supplies to make it to the Shimusogo Valley or Wamifuko City.

He came around the edge of the Tooth and saw Chimípu's tent. It was pitched against the rock with one stake caught in a crack a yard above the ground. A small alchemical fire burned a few feet to the side, with smoke rising up from four small birds cooking over the flames. A cloth was spiked to the ground with a rock in the middle; he knew there would be fresh water collecting underneath it.

Next to the fire sat Pidòhu. The young man was huddled underneath one blanket and had another neatly wrapped around his legs. Three sticks---the rods from Chimípu's tent---ran along his leg; Rutejìmo could see them peeking out of the folds. A large stain centered over his injury, but there was no blood dripping to the sand below.

As he stood there, Pidòhu's head jerked up and he looked around. Slowly, their eyes met, and Rutejìmo felt more ashamed than he had ever felt before. The hurt and betrayal in Pidòhu's gaze bore right to his bones, etching the guilt and shame into Rutejìmo's soul.

"Dòhu…." Rutejìmo stepped forward, holding out his hand. He didn't have the words to ask for forgiveness.

Around him, the wind kicked up and tore at his exposed skin. He blinked at the tears that formed in his eyes. It rose into a familiar howl, of a clan runner racing toward him. He managed to turn just as Chimípu's scream tore through the air.

"Bastard!"

Rutejìmo snapped up his arm in time to catch Chimípu's fist. The impact threw him back into the cloud of dust and sand that came with her charge. He choked on it and tried to stand up, but the dizziness caught him and he bent over.

Chimípu's kick caught him in his gut. The force knocked him into the air.

He landed hard and crumpled to the ground. Gasping, he planted his hands on the ground and tried to push up.

Her knee drove down on his neck, shoving his face into the rocks. The sharp edges cut at his cheeks and forehead.

Chimípu almost growled as she leaned over him. "Why shouldn't I kill you right now?" The scrape of a knife being unsheathed sent a sharp bolt of fear through Rutejìmo, but he couldn't get leverage to push her away.

"I didn't mean---"

"You abandoned us, you sand-cursed bastard! Your own clan!" The knife pressed against his throat, the sharp point digging into his skin. He stared at the bright blade. It was her mother's blessed knife, Shimusogo Rabedájyo, and the letters that named it shone in the sunlight.

"I-I---" He wanted to deny it, but he couldn't. With a long breath, Rutejìmo slumped down and stopped fighting. "Yes, Great Shimusogo Chimípu."

The point jerked, and he felt a pinch against his neck. Blood ran down his neck and he tensed with fear.

"Why," she said in a barely controlled voice, "did you come back?"

"Because…." He realized he didn't have a good answer.

She bore down on his neck. "Why!?"

"Because it was wrong!"

"What? Abandoning an injured clan member to the sands," she said sardonically, "or knocking him off the rocks in the first place?"

"I didn't throw a rock."

"You were there!" The knife twisted, and more blood trickled down. "You could have stopped him!"

"I tried. I really did."

Chimípu yanked the blade away and used her foot to shove him onto his back. She took a step back. "You're pathetic, bastard. Weak-willed and soft of spine. I'd rather see you burn in the desert than have you stay here."

Rutejìmo started to crawl to his feet but stopped. He knelt down in front of her and bowed deeply. "I'm sorry, Great Shimusogo Chimípu."

"Stop that. I don't want your false respect." She stormed closer and brandished the weapon again. "By all rights, I should cut your throat right here and now."

Rutejìmo stared at the weapon, his body trembling with fear. He could still feel it against his throat, pricking the skin. A dribble of blood ran down his neck, and he felt the hot liquid soaking into the fabric of his shirt. By all accounts, she could have killed him seconds ago and he would have no chance to beg for forgiveness.

In the stories, the brave warrior would bare his neck and surrender his life to the person in front of him. It was the ultimate way to ask for forgiveness. When he was a little boy, he shuddered as he listened to the whispered tales around the bonfires. Most of the stories ended with a cut throat and someone's death.

Letting out a sobbing gasp, Rutejìmo forced his chin up. His body trembled, and every muscle, from his sphincter to his scalp, tightened in fear. Fresh tears ran down his cheeks as he lifted his head as far as he could, exposing his throat to her blade. "As," he choked, "Great Shimusogo Chimípu wishes." Unable to look at her, he closed his eyes and waited for the end.

For a long moment, the only thing he heard was the wind and his heart. He tried not to move. He imagined her pulling back the blade to slash at him and wondered what it would feel like. He had never seen anyone killed this way, but it sickened him to think that he would find out firsthand.

"Damn it," she whispered angrily, "put your neck down."

He didn't move until he heard her sheathe her dagger. Slowly, he opened his eyes to see Chimípu striding back to Pidòhu. She dropped down next to him, her back to Rutejìmo.

Shaking, Rutejìmo got to his feet and padded over to the fire. He knelt down and bowed to Pidòhu. "I'm sorry, Great Shimusogo Pidòhu."

Pidòhu waved his hand, the movement frail. "Forgiven, Rutejìmo."

Rutejìmo lifted his head and settled back.

Looking him over, Pidòhu frowned. "What happened?"

"I… I don't know where to start."

"Why are you back?" snapped Chimípu.

"Tsubàyo tried to steal a horse."

Both Pidòhu and Chimípu inhaled sharply.

Rutejìmo continued in a halting tale, telling them about the mechanical scorpion, the two clan members in black who were guarding the horses, and the other members. He told them about sleeping through the night in the dark, and Karawàbi's attempt to beat him into submission.

When he finished, Pidòhu asked in his quiet voice, "Where is Tsubàyo?"

"I don't know."

"And you left Karawàbi behind?" asked Pidòhu.

Chimípu grunted. "You do that a lot, bastard. Leaving your clan."

"At least," snapped Rutejìmo, "you didn't try to kill me."

Chimípu opened her mouth to respond. But then a smile curled the corner of her lip. She glanced down at Rutejìmo's pack. "Have you eaten?"

"Only trail rations last night. Nothing today."

"Idiot." She leaned over and plucked one of the four birds from the fire. She pulled out one of the boards and placed the meat on it. She bowed to Pidòhu before setting it down next to him.

Rutejìmo watched silently as she served herself.

To his surprise, she gestured for him to take the next one. "Eat both if you need to. You're probably starving."

His stomach gurgling, Rutejìmo fought the urge to snatch it from the flames. He picked it up and set it down on his board. For a long moment, none of them touched their food, then Chimípu bowed her head and whispered a prayer to Shimusògo.

Rutejìmo joined in and, for the first time, he meant the words.
