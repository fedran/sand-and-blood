---
when:
  start: 1471/4/2 MTR 17::64
date: 2012-09-19
title: One Mistake
locations:
  primary:
    - "Oijogushi's Abandoned Valley"
characters:
  primary:
    - Rutejìmo
  secondary:
    - Chimípu
    - Pidòhu
    - Tateshyúso
    - Shimusògo
  referenced:
    - Tsubàyo
    - Karawàbi
    - Mikáryo
organizations:
  secondary:
    - Shimusògo
    - Tateshyúso
summary: >
  After eight hours of dragging Pidòhu across the desert, Rutejìmo was able to help Pidòhu with using his powers.

  Chimípu on the other hand had gone ahead and was using her powers to set up the camp. She was still obviously nervous and afraid of Tsubàyo. When Pidòhu called her out on it, she snapped back. He insisted she ran and, reluctantly, she did.

  When she came back, Rutejìmo went on a run himself. It was a long run where he realized and tested his powers. But, when he came back, the camp had been destroyed. Chimípu had been knocked out and Tsubàyo had kidnapped Pidòhu.

  Chimípu recovered and burst into rage, but she struggled between her desire to rescue Pidòhu and remained behind to protected Rutejìmo. Rutejìmo insisted she go after Pidòhu and he would remain at the camp.
---

> It only takes a second for everything to change. --- Proverb

Rutejìmo struggled to set one foot in front of the other. After a day of dragging Pidòhu across the desert, each agony had become painfully familiar. The muscles in his back screamed and his legs shuddered with the effort to pull the wooden frame along the rolling rocks and sharp stones.

Every time the wooden frame slipped in his palms, the memory of Pidòhu's fall slammed into him. He clenched tightly to the handles, despite the blisters and the sharp pains. Memories drove him to keep pulling. If it wasn't the heart-stopping fall, it was Karawàbi's empty gaze or the smell of his corpse. He was haunted by the terrible things that had happened and the guilt that burned in his throat. He was responsible, and even if it took them a month to get home, he wouldn't stop dragging.

A shadow circled around them---Tateshyúso. Rutejìmo followed the spirit's passing with his eyes as he forced himself to step forward. His heart quickened with anticipation as the shadow spiraled closer.

Pidòhu groaned softly, and the frame shifted in Rutejìmo's grip. "Come on, please, come on," he whispered.

"Don't force it," Rutejìmo said. He gripped tighter.

Pidòhu chuckled. "You're an expert now?"

"After listening to you for eight hours?" Rutejìmo grinned. "Yes, I am. Every time you start struggling and talking, that shadow"---he gestured with his chin---"slides away. And when you relax and doze, it stays above us."

"It isn't dozing, it's…." Pidòhu sounded hurt and frustrated. "I can't describe it, how it feels."

Rutejìmo struggled to drag the frame over a larger ridge. "Give me a better word."

"Word for what?"

"When you find that all your fears fade away and you have this… this… presence inside you. When you feel Shimusògo running in your heart and there is no time or place to be upset, angry, or even hurt. I-I… I can't explain it, either."

"Rapture." Pidòhu chuckled dryly, then groaned in pain. "Though I'll take your word for Shimusògo. When I doze as you said, it is the same thing. My leg doesn't hurt, and I stop thinking about all the things I should have done instead of getting up on that rock."

Rutejìmo froze in mid-step. "You too?"

"Yes. But then I realize that if I never broke my leg, I may have never heard Tateshyúso. I was never for Shimusògo, no matter how much I hoped. The idea that Tateshyúso would be there for me, to hear her whisper in the back of my head and feel that shadow, was the furthest thing from my dreams. But, now that I feel her, I have a craving to feel her shadow on my skin."

As he spoke, Tateshyúso's shadow spiraled out and then dove back in. It sailed past Rutejìmo and Pidòhu before coming up.

As the blanket of coolness draped over them, Rutejìmo let out a soft moan of relief. "That feels good."

"I know." Pidòhu's voice was strained. "But I don't think I can hold her much longer. I'm getting tired."

"As long as you can." Rutejìmo bore down on the pain, gripped the handles tightly, and pulled forward. He couldn't move fast enough for Shimusògo, but he needed to take advantage of the dark comfort of Tateshyúso as long as it would last.

He followed Chimípu's half-hidden footsteps as the wind erased her passing. Her path took him up to the crest of a dune. He knew she was on the other side; it was only a few chains, but it felt like miles. Gasping for breath, he drove his feet into the shifting sands and concentrated on each step.

He had almost made it to the ridge when the shadow disappeared.

"I'm sorry," gasped Pidòhu, "I can't hold her any longer."

The heat bore down on Rutejìmo, but he shook his head. "It's only another chain. I can make it."

"Damn the sands," Pidòhu said with a sniff. "I feel useless."

"I know the feeling."

Every step was agony, but Rutejìmo drove himself to keep pulling. He jammed his feet into the sand and balanced himself before dragging Pidòhu up the side of the dune. His back screamed in pain, and he felt the burn in his thighs and shoulders, but the memories shoved him farther. When he reached the top, he let out a cry of relief and stood there, fingers frozen around the wood, and his body shaking with the effort.

The valley was long and steep-edged, empty except for four boulders arranged in a crescent at one end. Sand had piled up along the rocks, but the crescent formed a shelter from the wind.

Inside the rocks, Chimípu had already pitched the tents and started dinner. She moved quickly, rushing from a tent to the fire to the rocks. Her body blurred with her speed, and Shimusògo flickered into existence and faded away.

Rutejìmo stopped in shock. After a second, he pulled Pidòhu around so he could also see. "What's she doing?"

Pidòhu lifted himself with a groan to look. "Running, or at least trying."

"But she keeps stopping. That isn't a good run. We need distance to feel Shimusògo." Rutejìmo frowned. "She seems…." He struggled for the word. "Anxious? Desperate?"

Pidòhu gestured with his fingers. "She's scared. Look at her. She keeps looking at the shadows and holding her knife when she stops. She's afraid to be alone."

"Tsubàyo?"

"Probably."

Rutejìmo watched Chimípu flit around the camp. The dust rose up in a spiral, and the dépa was always ahead of her: standing by the tent, near the fire, on the rock. Everywhere Shimusògo stopped, she raced toward him.

"Jìmo, Mípu needs to run. Run straight and long. I don't know what else to say."

"Shimusògo run." Rutejìmo chuckled, "But she won't. Not knowing Tsubàyo is near."

Chimípu stopped suddenly, and the dust settled around her. She jerked her head to the side and her fingers tightened around the hilt of the blade. Slowly, she circled in place and peered along the ridges of the valley until her gaze focused on Rutejìmo and Pidòhu. She disappeared in a blast of movement, a cloud of dust and rocks exploding in a plume.

Rutejìmo watched as she covered the distance between the camp and him in a heartbeat. He braced himself and turned to shield Pidòhu when she stopped.

The searing-hot air struck his back, pummeling his skin with shards of rocks. "Is there something wrong?" snapped Chimípu.

"No, just talking."

"Come down from the open, please?"

Pidòhu whispered from underneath Rutejìmo's arm. "It's okay, Mípu."

"Please?" she asked.

Getting a nod from Pidòhu, Rutejìmo gripped the frame and started down the curve of the valley.

Chimípu ran off, her form blurring with her speed until she stopped near the camp. She bounced back and forth, checking everything impatiently as Rutejìmo made his way down. When he dragged Pidòhu to her, she snatched the frame from Rutejìmo and eased it down.

Rutejìmo gasped to catch his breath, then said, "You need to run."

"Absolutely not," snapped Chimípu. "We can't leave Dòhu alone."

Pidòhu grabbed Chimípu's arm to stop her. His knuckles grew white when she tried to pull away. "Mípu, he said you," he forced out the word, "need to run. Not the both of you, just you."

Chimípu froze and shook her head. "No, I can't."

"You're being a bitch. And those little sprints aren't going to help you feel better."

Chimípu growled. "I'm not trying to feel better. I'm trying to protect you! I can't… I couldn't stop Tsubàyo. And he could be anywhere!"

"But he needs shadows, right? You said he disappeared in the darkness."

"There are shadows everywhere!" Her shrill voice echoed against the rocks.

"But," Pidòhu said as he continued to grip her arm, "there is still sunlight."

She spun on her heel, but Pidòhu wasn't done. "Besides," he said with a grin, "Great Shimusogo Chimípu is being rather difficult right now. And I'd like to enjoy dinner without her running around and kicking sand in my food."

"I…." She worried her lip. "I already ran. This morning. And I ran ahead to set up." She pointed back to the camp.

"No," Rutejìmo said with a grim smile, "you didn't. You had a little stroll. But you know that a few miles run isn't enough for you anymore. I can see that---"

"I---" She screwed her face up, "I already ran."

"And yet," Pidòhu added, "you feel the longing to do it again."

Chimípu looked guilty.

Rutejìmo pointed out to the desert. "Go on."

"But Great Shimusogo Chimípu," Pidòhu added in mock begging, "please don't accelerate until your blast won't rip my bandages off."

Chimípu grinned. "All right, but promise me you'll be okay." She looked directly at Rutejìmo. "Promise me."

Rutejìmo felt a prickle of fear, but he bowed. "I promise, Great Shimusogo Chimípu. I will keep Tateshyuso Pidòhu safe."

From the corner of his eyes, he saw Pidòhu flinch.

She didn't say anything, her eyes flickering back and forth as she stared at him.

He gulped. "With my life, if needed."

Chimípu stepped back. "It won't come to that. I'll be back, I promise. Just a little run."

Pidòhu grinned. "And don't say twenty minutes like last time. You never come back that fast. Run until the sun turns red, then come back. We'll… well, Jìmo will have dinner ready. Right?"

Rutejìmo nodded.

"O-Okay. I feel like---"

Rutejìmo pointed again. "Run."

Chimípu bowed to both of them, then stepped back farther. She turned on her heels and jogged out a few chains before accelerating in a cloud of sand and dust.

Pidòhu chuckled as they watched her disappear. "You know, Jìmo. It sounded right when you called me Tateshyuso Pidòhu. But it scared me a lot."

Rutejìmo patted Pidòhu on the shoulder before removing the ropes. "But it is who you are. Right? I won't call you that, if you don't want."

Pidòhu stretched and smiled. "No, I'm pretty sure that I can hear Tateshyúso now. It is fair enough… at least until the elders say otherwise. Now, drag some food over, and I'll start preparing."

Chimípu came back when the sun was kissing the horizon and the red streaks of light stretched across the sky. The valley was in shadows, and everything was cast in a dark-red glow. She was sweating lightly as she bounded into the campsite with a smile on her face. Panting, she sat gracefully down on a rock.

Pidòhu handed her a board with food. "Good run?"

She nodded. She turned to Rutejìmo. "You should run too."

A longing filled him. He looked at the red sunlight.

"Just don't go as far," Pidòhu said.

"Well, if Great Shimusogo Chimípu isn't allowed to resist, I won't, either." He stood up, his heart pounding faster with anticipation. "I'll be back soon." With a grin, he looked over Chimípu from head to toe. "I'm not fast enough to run home and back."

She stood up. "I didn't…." The outrage turned to a smile. "Shimusògo run."

Rutejìmo bowed. "Shimusògo run."

He jogged to the ridge of the valley, then sprinted forward. The dépa appeared, and he threw himself into chasing it. Running came with a rush, a pleasure that coursed through every vein in his body. It felt right to run, to let his feet pound the sands and to feel the wind on his face. Even the relief of Tateshyúso's shadow was nothing compared to racing along the desert. Rapture was a good word for it.

With a grin, he threw himself into a slide. His feet dug into the ground, leaving a deep trench. As soon as he stopped, he kicked off and accelerated in the opposite direction.

The dépa remained with him, spinning around when he stopped and sprinting ahead as he turned.

Rutejìmo threw himself into running and sliding, Just pushing himself to the limits of his flagging strength. He loved every moment of it: his feet striking the ground, the wind blowing on his face, and the rush of running faster than he had ever run before. He felt alive out in the desert, breathing in the wind that whipped at his face and feeling the thrill that coursed through his legs.

The sun was almost below the horizon when he stopped. Looking out over the trails he left in the desert, he smiled. There was nothing around him. Not a rock, not a valley. Just the fading light and fine sand between his toes.

The dépa raced past him, heading back to their campsite.

Rutejìmo dug his feet into the sand and raced after it. Moving in a straight line, he blew across the desert as fast as his feet could carry him. Magic flowed through him as the world blurred into a haze of speed and light.

He was gasping when he reached the valley, but there was a smile on his face. He came to a skidding halt, blasting through the top ridge before landing on the side. He used his momentum to slide down to the bottom and then a rod up the far slope.

And then he saw one of the tents burning.

All the joy and pleasure left him in a rush. He sprinted to the campsite, calling out to the others. "Mípu!? Dòhu!?"

His tent was on fire. The alchemical fire had rolled into it. The sand and rock on the ground were torn up. Pidòhu's tent had collapsed, and the fabric fluttered in the window.

"Pidòhu!?"

He spun around, looking for the others. His heart pounded, the steady thump sending waves of agony across his ribs.

"Chimípu!?"

The sun hadn't set, but there was barely enough light to see. He accelerated into a sprint and raced down the valley, looking for any sign of Chimípu or Pidòhu. Finding neither, he spun on his heel and raced down the other length. He called out for them as he ran the length of the valley.

He almost missed the body in the shadows. He dug his feet into the ground to stop, and then raced back. The wind howled around him as he came to a halt next to Chimípu's prone form.

"Mípu!?" He grabbed her and pulled her up to his chest. He was terrified that she would be dead, her throat cut just like Karawàbi. Without thinking, he reached for her neck, his stomach clenching in fear, but he only found soft skin underneath his fingers. "Oh, thank the sands."

Chimípu moaned, her eyelids fluttering. "J-Jìmo?"

"Mípu? Are you okay?"

"I was…." She held up her hand to a bruise forming on her forehead. Her eyes widened. Chimípu surged to her feet. "The bastard hit me with a horse! A sands-damned horse!" Her body began to glow with golden flames. She spun around. "Where is Pidòhu!?"

"I couldn't find him." Rutejìmo cringed anticipating the response he knew would come.

She stepped back from him. "No, he couldn't have. Bàyo, um, Tsubàyo would never take him." She spun on her heel and then disappeared in a streak of golden flames. The air exploded around him, sucking sand and wind after her as she reached the valley in a heartbeat.

She was already tearing apart the tents when he caught up to her. "Damn it, he took him! That bastard took my clan!" She grabbed her knife from near the fire and stopped in front of Rutejìmo. "We're going after him."

Rutejìmo held out his hand. "Mípu, I---"

"Hurry up, Jìmo." She turned on him. "We have to go."

"Mípu!"

She glared at him.

"Do you know where he is?"

She frowned and shook her head. She spun once and pointed down the valley. "That way. He's there, I know it." She stepped forward. "Come on, it shouldn't be more than an hour."

Rutejìmo froze with the memory of Tsubàyo telling him almost the same thing. He remembered the darkness that surrounded him as they walked across the sand. And the suffocating loneliness as he wandered back. He looked around at the shadows filling the valley. "Mípu, it's night."

"The dark doesn't scare me." Her voice was proud and furious. She tapped her foot and glanced in the direction she pointed.

"I---" He gulped. He wanted to help, but he knew he was useless in the night. "I-I can't go with you."

"Jìmo! It is Pidòhu! He needs us."

"No," he said, stepping back, "he needs you. Mípu. If anyone can save him, it will be you."

"No, I can't leave you behind."

"And I can't keep up, Mípu. Not in the dark. No matter how much you want, can you defend us both?"

She let out a groan. "Damn it."

"I'll build a fire and keep still. In the valley, no one will find me, and Tsubàyo wouldn't bother coming back for me. He has Great Tateshyuso Pidòhu and is probably going to give him to Mikáryo."

Chimípu held out her hand, gesturing him to stay. "J-Just be safe. I'll be back soon."

Rutejìmo bowed. "Yes, Great Shimusogo Chimípu."

And then she was gone in a rush of wind and sand.
