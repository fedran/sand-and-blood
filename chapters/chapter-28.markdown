---
when:
  start: 1471/4/4 MTR 8::55
date: 2012-10-17
title: The Offer
locations:
  primary:
    - Pabinkue's Lost Tears
characters:
  primary:
    - Rutejìmo
  secondary:
    - Mikáryo
    - Tsubàyo
  referenced:
    - Jastor (Epigraph)
    - Pidòhu
    - Chimípu
    - Tachìra
    - Karawàbi
    - Shimusògo
organizations:
  secondary:
    - Shimusògo
    - Pabinkúe
topics:
  referenced:
    - A Tactical Analysis of Kyōti Politics (Epigraph)
summary: >
  Rutejìmo woke up. Tsubàyo had bound him for delivering to Mikáryo. He was in pain, with cracked bones and struggling to breathe.

  As they waited, Tsubàyo talked about gaining his powers, how it felt and his abilities.

  When Mikáryo arrives, Tsubàyo offered Rutejìmo's life to her. Mikáryo made a show of looking at Rutejìmo, but then checked on him before turning him down. She told Tsubàyo that she waited for him and that he would be coming with her. He refused.
---

> Over the centuries, the clans have specialized not only in their powers but what services they offer the world. --- Jastor, *A Tactical Analysis of Kyōti Politics*

Rutejìmo woke up crying. He could feel the tears running down his cheeks and his chest shuddering with every gasping sob, but he couldn't figure out how to stop. It felt as though his body was disconnected from his mind and he could only listen to himself as he pitifully cried.

He tried to lift his hand to wipe the tears from his face, but nothing happened. He slumped forward and winced as he felt bones grinding. Gasping for breath, he continued to sob as he tried to focus on his chest. It rose and fell with his ragged breathing, but someone had bound his arms to his sides with rope.

There was only one person who would have tied him up: Tsubàyo.

With a struggle, he lifted his head and looked around him. His eyes were blurred from the tears, and a fire in front of him blinded him. He slumped back, hitting his head against rock, and looked up. It took a moment for him to focus on the stars and not the pain of hitting his head.

Moving helped with his crying, and he managed to calm his ragged breaths. He gulped to ease his dry throat and looked back down at his bindings.

Tsubàyo had tied ropes around him: one around the pectorals and below his shoulders, another at his waist, and two on his legs. They dug into his sides, and the pressure ground his ribs together. Morbidly, he focused on his arm. A bruise had already formed on the skin and it was swollen. He concentrated on moving his fingers, but stopped when pain shot up into his shoulder.

Memories of Pidòhu's broken leg flashed through his mind, of the ragged wound and blood pooling underneath. His breath quickened, and he leaned to the side, looking for signs of an open wound. When he saw no puddles of blood or stains on the sand, he let out a sob of relief.

"Please tell me you are done babbling," grumbled Tsubàyo.

Rutejìmo peered across a fire at the teenager on the other side. The shadows cast Tsubàyo in relief, highlighting the scar tissue on the side of his head. He had bruises and cuts on his face. Only a tiny arc of Tsubàyo's glare was visible through Tsubàyo's swollen right eye.

"W-What?" Rutejìmo's voice was broken and raspy.

Tsubàyo shook his head. "You've been moaning and babbling for the last six hours. I thought if I hit you in the head again, you'd shut up, but I was afraid of killing you."

Rutejìmo groaned through a piercing headache and the agony of his broken bones. "Did Mípu get away?"

"Yes, and she took my sacrifice with her."

"Sacrifice?" He let out a sigh. "Pidòhu got away?"

"Yeah, which means you'll be Pabinkúe's sacrifice. You aren't really worth more than that."

He tensed. "Mikáryo?"

Tsubàyo let out a groan as he stood up. "You said three nights, but she hasn't shown up. I've been awake all night waiting for her." He sighed and stretched. "The strange part is that I'm not tired at night. It feels"---he smiled---"good. And I see things I never saw before. The desert is alive, Jìmo, but you'll never find out."

"She won't…." His voice trailed off with the feeling that he shouldn't say anything.

"Won't what?"

"I hope she kills you," he finished uncomfortably.

"Well, if she doesn't come before the sun rises"---Tsubàyo pulled out his knife---"then I'll make sure it will be your last."

Rutejìmo stared at the blade through the flames. It was named, but Tsubàyo had scraped off the runes for Shimusògo. It was one more sign that Tsubàyo had turned away from the clan. In the blade, he saw the light of the rising sun reflected in the metal. It was less than an hour before morning.

"Have no fear, Jìmo. I'll cut your throat nice and clean. I'll even give you a chance to see Tachìra one last time."

Memories slammed into Rutejìmo, and he choked back a fresh sob. "Did you kill Karawàbi?"

"Wàbi?" Tsubàyo frowned. "What are you talking about? I don't care what that slow-witted rock is doing. He would have just slowed me down."

"He's dead."

Tsubàyo hesitated, and then he shrugged. "The world is better without him."

"Why? We were all clan."

"You are an idiot, Jìmo, but you didn't listen. Instead of going with me, you ran back to Mípu like a little baby boy. Maybe Mikáryo's sister would be alive if you hadn't abandoned me."

"I didn't abandon you, Bàyo." Rutejìmo shifted and cringed at the pain. "Shimusògo didn't abandon us. I can run. Have you seen---?"

"Yes, you can run. But I found something better." He gestured to the three horses standing just outside of the circle of flames. One of them had fabric wrapped around its chest; it was stained crimson from Chimípu's attack. Each time it inhaled, it struggled to breathe. Another, the mare, leaned against the injured horse while the third rested on the ground.

"There is power in the herd. I can feel them in my head. It's a song," Tsubàyo's voice grew dazed, "and it's beautiful. Better than running in the sun or that pitiful life we had in the valley. No, as soon as I'm done with her, I'm going to take my little herd and I'm going to run. I don't care about Shimusògo or Pabinkúe."

"And," said Mikáryo as she stepped out of the darkness, "what if Pabinkúe won't let you run?" Her voice was tense and threatening, but didn't rise above a low tone.

Behind her, her horse circled around the light, moving with the same shadowy silence it always did. The other three horses lifted their heads as Mikáryo's mount approached them.

Tsubàyo hissed and brandished his knife at her. His foot scraped sand as he prepared for an attack.

Mikáryo circled the flames after him with a grim smile on her face. The dark fabric wrapping her body shifted with her movements, but didn't even make a rustle of noise. She held her left hand against her side, and Rutejìmo noticed she had the tazágu half-hidden in the folds of cloth.

Tsubàyo continued to back away from her. "What do you want?" Sweat glistened on his brow as he stared at her.

"Oh," Mikáryo said, "I've been listening to you screaming into the dark for a few hours now. Well, you have my attention, boy. What do you want?"

Tsubàyo stepped over Rutejìmo's legs. He waved the knife at him in a dismissive gesture. "You demanded a life. There he is."

Mikáryo didn't even look at Rutejìmo. "That boy? He's pathetic. He peed his own pants when I first met him."

Tsubàyo chuckled. "Sounds like Jìmo. He was always the weakest next to Dòhu… Pidòhu."

Rutejìmo flushed in humiliation.

"Yes. But," Mikáryo said as she stepped over to Rutejìmo, "that would make him a pathetic sacrifice, wouldn't it?"

Tsubàyo clenched the hilt of his knife tighter. "A life is a life."

"There are many different types of lives in the world, boy." Mikáryo strolled after Tsubàyo. She moved with an unhurried grace, her body swaying as she stalked after him. "And I'm thinking that Pabinkúe would be greatly interested in the"---she smiled---"richness of your life."

Tsubàyo stepped back and shook his head. "No, not me. Take him."

Mikáryo stopped. She finally looked at Rutejìmo. But he didn't see compassion in her eyes, only a cold, calculating look.

His stomach clenched at her dark eyes, the pupils hiding the whites completely. There was death in Mikáryo's gaze, and he wondered if the compassion she showed earlier was just a lie. Stories of the clan of night rushed through his mind, and a soft whimper rose in his throat.

She glided across the sands and knelt, one knee on either side of his. Her eyes looked over him and he wanted to crawl away from her cruel gaze.

"You," she said in a whisper, "are adorably pathetic, Jìmo."

He gasped and his eyes widened as he stared at her.

"But keep looking frightened. Otherwise, I'll have to gut you."

He whimpered.

"Just like that," she said with the barest hint of a smile.

Rutejìmo gulped, trying to ease the tightness in his throat. He trembled as she reached out and touched his leg. Her skin was cool and dry against his heated flesh.

"You have a fever." She trailed two fingernails up his leg.

He squirmed and she chuckled. He stared in shock as she pressed against his stomach, then up to his ribs. At the pressure, he hissed before she could reach his broken rib.

"That hurts?"

He whimpered and whispered, his voice cracking. "A-And my arm."

"Pathetic." She leaned into him as she stood up.

Rutejìmo froze when he felt the brush of her lips against his, but then she was turning away from him. He blushed hotly as he stared at her, unsure of what to say or what to do.

"He's too weak for Pabinkúe. My sister was worth twice him."

Tsubàyo snarled. "Then kill the bastard." He hefted his knife.

Rutejìmo tried to push away, but his feet just dragged through the sand and rocks. Every movement sent agony coursing up his arms and along his ribs. He ached from the inside all the way out, but the murderous look in Tsubàyo's eyes allowed no compassion.

Tsubàyo circled around the fire.

Mikáryo looked up sharply. A frown ghosted across her face, and then she smiled broadly. "Tsubàyo," she said. She lowered her head, and the smile dropped from her face. "You have other concerns to worry about."

"Like what?"

"As much as you pretend that Pabinkúe doesn't talk to you, I know you can hear her. That song? That's her. Your lovely horses that are coming to you? Those were my sister's."

Tsubàyo froze, his blade only a few feet away from Rutejìmo. He looked over his shoulder.

"Yes, the woman you killed. Someone has to lead her herd, and we have a job to do. The Kidokūku clan should have that scorpion of theirs ready to move tomorrow, and we must be ready."

Tsubàyo turned back to Rutejìmo. He gripped the knife tighter, staring at Rutejìmo.

"We have a job, Pabinkue Tsubàyo," Mikáryo said as she stepped closer, "and your little pissing match with Jìmo isn't going to help anyone."

Tsubàyo spun on his heels, brandishing his knife. "I won't go with you! I am not Pabinkúe!"

Mikáryo chuckled. "We'll find out in about an hour." She turned and headed toward the edge of light.

He shook his head. "An hour!? What's in an hour?"

She only smiled as she disappeared into the darkness.

Tsubàyo's feet thudded the ground as he stalked toward where she had stood. "What's in an hour!? Damn it! What's in an hour?"

"Kill Jìmo," her voice drifted from the dark, "and you'll find out sooner. Otherwise, in an hour, you'll be Pabinkue Tsubàyo or dead."

"Come out!" Tsubàyo yelled, "And tell me what is going on!"

There was only silence, except for the hiss and pop of the flames.

Tsubàyo spun back to Rutejìmo. He stormed over, his knife flashing.

Rutejìmo cringed away, but he couldn't move fast enough to avoid Tsubàyo as he crouched in front of him. He felt the cool blade on his neck, the sharp edge pressing against his delicate skin.

"I should kill you right here." He pushed harder, and Rutejìmo felt the flesh parting. A trickle of blood ran down his neck.

Terror pounded in Rutejìmo's ears. His heart slammed against his chest, every beat drew agony from his cracked ribs and broken arm. He gulped and tried to put on a brave face, but he wanted to beg for mercy.

There was no compassion in Tsubàyo's gaze. There was never love, but now Rutejìmo only saw hatred in the dark eyes. The knife pressed harder against his neck, and more blood dribbled down.

"Damn it!" Tsubàyo yanked away the blade as he whirled to look in the direction Mikáryo had taken. "What did she mean!?"

Rutejìmo gasped for breath, fighting back the sobs that threatened to rise up.

Tsubàyo turned back to Rutejìmo. "You're lucky. You're going to live long enough to see the sun."
