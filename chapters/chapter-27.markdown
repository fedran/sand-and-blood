---
when:
  start: 1471/4/3 MTR 19::11
date: 2012-10-13
title: Pabinkue Tsubàyo
locations:
  primary:
    - Pabinkue's Lost Tears
characters:
  primary:
    - Rutejìmo
  secondary:
    - Tsubàyo
    - Chimípu
    - Pidòh
    - Tsubàyo
    - Shimusògo
    - Ryachuikùo # Horse
    - Ganifúma # Horse
  referenced:
    - Ralador (Epigraph)
    - Mikáryo
    - Karawàbi
organizations:
  secondary:
    - Shimusògo
    - Pabinkúe
topics:
  referenced:
    - "A Girl's Secret (Epigraph)"
summary: >
  The attack against Tsubàyo. As Chimípu charged, Rutejìmo supported by firing makeshift shots against him and his horses. Rutejìmo missed, but one of the misses came near Chimípu who plucked the shot, accelerated it, and threw it at Tsubàyo. A horse jumped to shield him, but the explosion threw them apart.

  As Chimípu and Tsubàyo fought, Rutejìmo saw a chance to rescue Pidòhu. He ran over and was relieved to find Pidòhu was glad to see him. However, one of Tsubàyo's horses stepped out of the shadows and attacked Rutejìmo. Tsubàyo joined in and almost killed Rutejìmo, but Chimípu's fireball caught him and they were fighting.

  In the chaos, Rutejìmo ended up fighting Tsubàyo as Chimípu rescued Pidòhu. In the end, Tsubàyo knocked Rutejìmo out with a horse kick.
---

> It takes only a single word to ruin a surprise. --- Ralador Markin, *A Girl's Secret*

Rutejìmo came sliding to a halt along the flat patch of sand Chimípu had pointed out. It had a thin crust over the top and, when he stopped, it cracked from his weight. He sank into it and felt the softer sand beneath clinging to his bare feet. Some of the projectiles spilled out of his arms and hit the ground in a series of thumps.

He glanced over his shoulder and watched Chimípu circle around to the far end of the arch. A plume of sand and dust rose behind her, billowing out and adding to the haze from a light breeze rippling across the desert. She straightened into a line and sprinted for the arch, her body blurring as she accelerated for an attack.

Bile rose in his throat, and he turned away sharply. As much as he hated Tsubàyo for what he did, kidnapping Pidòhu and killing Mikáryo's sister, he couldn't bear the thought of seeing the teenager's death. Karawàbi's corpse still haunted him. In the last few days, he had realized he didn't have the taste to even imagine killing someone.

He looked back just as Chimípu reached the arch. From their plan, she would draw Tsubàyo out of the darkness and into the sands where he didn't have shadows to hide in. Rutejìmo imagined Chimípu watching the shadows warily as she approached. Her tazágu glinted in the sunlight as she held it ready to strike.

As she came in, the shadows bulged out and Tsubàyo came barreling out of the darkness on the back of his horse. He crouched low on the black animal with a spear in his hand. They moved as a single creature. Rutejìmo didn't know where Tsubàyo had gotten a spear until he realized it was Tsubàyo's knife tied to the end of a tent pole, crude but no doubt effective.

Tsubàyo's horse lashed out at Chimípu, teeth snapping on air.

She dodged to the side. Her momentum blasted her through the sand as she planted her foot on one of the fallen rocks. The impact stopped her instantly, and the rock cracked in half from her speed. Before the sand settled down, she kicked off, rolled backward into a jump, and landed on her feet. Sand rose in the air as she dove underneath the horse and accelerated away from the arch, but at a far slower rate than her top speed.

Tsubàyo charged after her. As he and his horse ran along the dunes, he readied his spear to attack.

Feeling guilty for not responding faster, Rutejìmo grabbed the sling and a shot. He set the bundle in the center of the sling, made sure it was secure, and then spun on his heels. He could feel the shot tugging away from him, but he wasn't moving fast enough. Trying to remember the sensation from when he practiced earlier, he threw himself into each rotation.

When Shimusògo appeared at his feet, his spin accelerated, and the sand rose up in a vortex around him.

Between the rotations, he saw Tsubàyo reach Chimípu and attack. His spear slashed out in a wide swath. It left a faint blue haze as it cut through the air.

Chimípu ducked at the last minute, and the blade narrowly avoided her. She spun on her heels and grabbed at the shaft. She missed but yanked her tazágu up before Tsubàyo's backswing struck her.

Rutejìmo felt himself getting up to speed. With a grunt, he released the end of the sling, and the shot rocketed across the sands. The speed tore up the ground, sucking sand and dust behind it in a long trail as it cracked through the air.

He stumbled as he watched it. It flew straight, but it would miss. Swearing, he fumbled as he grabbed another shot. He jammed it into the sling and violently spun up to speed.

The first shot slammed into the ground a rod behind Tsubàyo. Neither Tsubàyo nor Chimípu would have noticed the miss, but the impact launched shrapnel and sand everywhere. A cloud of rocks and camping supplies blossomed out.

In the rain of sand, Tsubàyo ducked against the horse, and the equine spun on his forelegs. The creature's back legs snapped out and caught Chimípu in the chest. The impact hit with an explosion of air.

Chimípu rocketed out of the cloud, flying backward. She hit the ground on her feet and leaned back to dig her hands into the sand, gouging out a long line as she slowed. As soon as she came to a stop, she shot forward again, holding her spiked weapon with both hands.

Tsubàyo sprinted back for the shadows. He reached them and disappeared just as Chimípu caught up to him.

Chimípu raced into the darkness of the arch, her body igniting in golden flames. But, as the light peeled back the darkness, Tsubàyo was gone. She continued under the arch and came out the other side, the wind howling around her as she circled around it in a wide loop.

Tsubàyo stepped back out of the shadows a few feet away from Pidòhu. A cruel smile was on his face as he looked around. He stopped when he met Rutejìmo's gaze and then gave a little wave as if they were friends.

With a snarl, Rutejìmo fired the shot in his sling.

Tsubàyo shook his head. Just before the shot hit him, the horse stepped back, and Rutejìmo missed. It hit the rock near Pidòhu and exploded, showering him in rock dust.

Pidòhu cowered against the rock, and his wail rose up over the thuds of falling stones.

Rutejìmo gasped and cringed. "Sorry," he said, even though Pidòhu couldn't hear him.

The shadows swelled before peeling away from Tsubàyo. From the back of the horse, Tsubàyo yelled out cheerfully, "Don't hit Dòhu, Jìmo!"

Pidòhu continued to cry out in pain. Even in the shadows, Rutejìmo could see him clutching his broken leg as he tried to cower against the rocks.

Guilt slammed into Rutejìmo, and he wiped the tears from his eyes. He wasn't a warrior, he couldn't fight.

Chimípu jumped through the darkness as a translucent dépa appeared over her body. The flames around her became blindingly bright as she landed on the back of the horse. She slashed with her weapon, a blow aimed for Tsubàyo's throat.

Tsubàyo's spear flashed. He parried Chimípu's attack but slid across the horse's back, nearly slipping down its neck. Only a frantic grab of the mane kept him seated.

Chimípu screamed in rage as she swung at his face, punching him across the jaw.

The horse reared up. Tsubàyo flowed with the movement, but Chimípu tumbled off. With a jump the creature lurched forward, landed on its front legs, and kicked hard.

The world exploded around the two fighters, a large cloud of sand bursting out with blinding speed. Chimípu's glowing body was flung back across the stone arch and slammed into the far end. Even with the thunder rolling across the sands, he heard the dull crack of the impact.

Her flame died out as she fell to the ground in the darkness, disappearing as the sand rained down around her.

Rutejìmo gasped. He fumbled with the shots at his feet. One slipped through his fingers and he swore violently. He glanced up at the battle.

Tsubàyo stalked Chimípu, and his horse stepped easily around the rocks.

Whimpering with frustration, Rutejìmo grabbed a shot and set it in the sling. He threw himself into a spin as he stood up. He put everything he could into moving and he felt the power ripping through his limbs.

He released the sling and grabbed a second shot without slowing. Wrapping it into the sling, he spun violently and fired before the first attack hit.

Both projectiles tore across the ground. To his surprise, both of them were burning with light as they shone like two stars racing across the desert. He could feel the crack of air as they streaked at Tsubàyo's back.

He held his breath while he watched the shot. It left his lungs in a moan of despair when the first only clipped Tsubàyo's ear before rocketing past the arch and into the rocks beyond.

Startled, Tsubàyo jerked in the opposite direction and stumbled into the path of the second. The world seemed to slow down as the second projectile punched into the side of the black horse and exploded.

The horse stumbled as it staggered back.

Tsubàyo again grabbed the creature's mane to keep his balance.

Chimípu burst into flames as she launched herself out of the darkness. Her scream of rage echoed as she grabbed Tsubàyo with both hands. She tore him off the horse and threw him out into the sunlight.

Tsubàyo scrambled to his feet, but Chimípu raced past him. For a moment, Rutejìmo thought she had passed him, but then he saw that she dragged Tsubàyo by his arms. Tsubàyo's head bounced on the sand as she crested a dune.

Shimusògo appeared in front of Chimípu, already spinning in a circle. Chimípu grabbed Tsubàyo's arm with both hands and wrenched him into a spin of her own.

Tsubàyo's scream rose in waves as he was pulled out from her arm, his legs flailing around helplessly. Rutejìmo found it hard to focus as Chimípu continued to accelerate, spinning Tsubàyo like a slingshot. Their bodies blurred as a vortex of dust surrounded her.

She released him.

Tsubàyo cried out as he sailed across the sands. The air howled around him, rippling as he hit the dune. He struck the first dune on his shoulders, and the impact flipped him over. He bounced off and hit the second, and then the third. The fourth dune exploded as Tsubàyo crashed into it and rolled over the far side.

"Jìmo! Get Pidòhu!" Chimípu spun on her heels and raced back to the far end of the arch. She blurred as she rushed through. On the other side, she was holding the tazágu. She came into a wide circle as she charged after Tsubàyo.

Gasping, Rutejìmo scrambled back to his feet and sprinted for Pidòhu. The air rushed around him as he covered the distance in a few seconds and came to a sliding halt in front of Pidòhu. "Dòhu!"

Pidòhu looked up with tears in his eyes. His hands were bloody as he clutched his leg. The sharp ends of his bones stuck through his fingers. The coppery stench surrounded him as he shook. "J-Jìmo." He gave Rutejìmo a pained smile. "I can't say how happy I am to see you."

Panting, Rutejìmo knelt down next to him. "Yeah, me too." He looked over Pidòhu. "Um, I don't know if I can safely carry you."

"I'll take the pain if it gets me away from Bàyo."

"You should call him Tsubàyo; he isn't a friend anymore."

"Yeah," Pidòhu groaned, "but I'm having trouble remembering through the agony."

With a grim smile, Rutejìmo inched closer and slid his hands underneath Pidòhu.

Hot air struck the back of Rutejìmo's neck. He froze as a horse exhaled again. He could feel the threat, and his body began to shake. Looking up, he saw Pidòhu staring over his shoulder.

"J-Jìmo?" whispered Pidòhu.

Rutejìmo gulped, his body tensing painfully. "I'm about to get hurt, aren't I?"

A soft whimper escaped Pidòhu's throat. He gave a little nod.

"Sands," muttered Rutejìmo. He yanked his hands from Pidòhu and lurched to his feet. He felt the horse launching forward and dodged to the side.

The horse's teeth snapped on empty air.

Rutejìmo saw a flash of movement in the darkness. Dread filled him as he picked out the shadowed image of a second horse just as it kicked out with its back legs. He had a chance to inhale but then the hooves hit his chest.

The world exploded into agony as his ribs cracked and his vision blurred into white. He clutched as his chest, trying to think through the pain. Rutejìmo could feel the wind whistling around him but it took him a heartbeat to realize he wasn't touching the ground.

He tried to inhale to yell out, but his lungs wouldn't work. The world flew past and the desert became a blur of sand and rock. He tried to move, but every twitch ended in agony.

Rutejìmo hit the sand hard, and the impact felt like rock against his skin. It tore the flesh along his back and arms before he bounced off and flew in a low arc to slam hard into the ground. The impact grated his cracked ribs into each other, and he would have sobbed at the agony if he could make a sound. He tore at his chest, trying to force his lungs to work.

Sand crunched as someone walked up to him. "Did you know," asked Tsubàyo with a dry chuckle, "that I found more horses? Very useful, being able to command them without words. They'll do anything for me, including wait in the shadows for an idiot going for a cripple. Though, I was hoping that Mípu would get to Dòhu first. The look on her face would have made this all worth it."

Rutejìmo looked up with surprise. He gaped but he couldn't draw in a breath. He clawed at his abdomen as he crawled back.

Tsubàyo stepped down on Rutejìmo's foot and pinned him. "But I don't have time to chat."

He lifted the spear and held it with both hands.

Air rushed into Rutejìmo's lungs, and he could breathe again. He gasped for breath but couldn't look away from the spear poised to strike.

The flaming shot hit Tsubàyo square in the back, throwing him over Rutejìmo. It exploded in shreds of canvas and bedding. The spikes for the tents clipped Rutejìmo as they slammed into the ground.

A second, third, and fourth shot hit Tsubàyo in rapid succession, each one exploding as they picked Tsubàyo off the ground and tossed him like a pebble.

Stunned, Rutejìmo scrambled to his feet and stared as Tsubàyo hit the sands one last time. Every time Rutejìmo inhaled, he could feel sharp pains in his chest, but seeing Tsubàyo laid out gave him a fierce joy.

Behind him, Rutejìmo heard the rush of both Chimípu and a horse charging toward them.

Tsubàyo picked himself up, stumbling back as he clutched his head. "I'm ending this." His eyes flickered to the side.

Feeling a prickle of fear, Rutejìmo followed the gaze. Tsubàyo had focused on Pidòhu. In the darkness above the injured teenager, the shadows bulged out as the second horse stepped from the darkness.

Rutejìmo jerked. "Mípu! Save Dòhu now!" Even as he yelled, he knew she couldn't turn around fast enough.

In the shadows, the horse stood over Pidòhu and lifted a hoof. It was poised to crush his head.

Pidòhu looked up, his eyes white in the shadows.

Moving instinctively, Rutejìmo reached out for Chimípu. "Spin!"

She grabbed his wrists, and he spun with all his might. Her momentum tore through him, and he felt muscles ripping and his ribs grinding into each other as he brought her around in a tight circle.

Working by instinct, he released her and she shot out in a flash of light for the horse and Pidòhu, moving faster than he had ever seen her move. It was like firing a slingshot, but using her as the ammunition.

The first horse, the one charging Rutejìmo, slammed into his side. The impact blasted him across the sand.

Rutejìmo screamed out in pain as he flew through the air, but then he hit the sand hard, and the impact drove the air out of him again. He flailed on the sand, trying to get purchase.

A hoof slammed down on his left forearm, and he heard bones crack. A heartbeat later, the pain tore through his senses, and he couldn't help but scream in agony. His entire body spasmed as he curled around the hoof, trying to move the massive weight off the grinding bones.

A high-pitched squeal cut through his panic. Rutejìmo jerked and looked over his shoulder as the second horse dropped to the ground. A blood spray arched high into the air before splattering down. It flailed its legs as it struggled to get back up.

Chimípu stood over Pidòhu, guarding him. Her tazágu dripped with blood as she snarled. Her body flickered with flames as she kept the point of her weapon aimed toward the creature's throat.

Tsubàyo let out a growl and swung himself onto the horse pinning Rutejìmo. "Come on, let's save Ryachuikùo before she kills him." He chuckled as he looked down at Rutejìmo. He spoke in a low voice. "But your precious Mípu won't be able to save him from Ganifúma."

Rutejìmo froze when a memory slammed into him. Mikáryo had named Tsubàyo's horse Pabinkue Ryachuikùo. He glanced down at the horse underneath Tsubàyo. It was male and couldn't be Ganifúma, a female's name. With a start, he realized that Tsubàyo had said horses, and Rutejìmo already knew there were more.

He gasped and snapped his head around. The shadow next to Chimípu became suddenly threatening, and he imagined seeing something moving in the darkness. Panic tore through him. He inhaled before yelling as loud as he could. "Run!"

Chimípu's head jerked as she looked at him.

"Run, damn it, run!"

Tsubàyo sighed and shook his head. His horse kicked out, catching Rutejìmo under the chin and slamming him back into the ground.

There was an explosion of movement and a howl of wind from near the arch.

Rutejìmo lifted his head, but his eyes weren't focusing. He saw a light burning across the desert, moving away from him faster than he could ever run. He knew that she carried Pidòhu.

His thoughts ended when Tsubàyo's horse kicked again.
