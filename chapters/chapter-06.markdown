---
when:
  start: 1471/3/29 MTR 12::5
date: 2012-04-18
title: Heading Out
locations:
  primary:
    - Shimusogo Valley
characters:
  primary:
    - Rutejìmo
  secondary:
    - Tejíko
    - Chimípu
    - Pidòhu
    - Karawàbi
    - Tsubàyo
    - Gemènyo
    - Desòchu
    - Hyonèku
    - Jyotekábi
    - Mapábyo
  referenced:
    - Gregor (Epigraph)
    - Chimípu
    - Opōgyo
organizations:
  secondary:
    - Tateshyúso
    - Shimusògo
topics:
  referenced:
    - Ornithological Studies of the Kimīsu Region of the Mifúno Desert (Epigraph)
summary: >
  Tejíko told Rutejìmo that she knew he had been listening to the vote. After promising to beat him more, she sent him to get ready and leave. He did, excited about the trip. On the way to the village entrance, he spotted Chimípu running but he couldn't run faster than her. He also encountered Pidòhu, a weak teenager who spent most of his time alone. Pidòhu made some observations that the trip had been planned for months and that there were a lot of adults coming along for a "simple" rite of passage for one woman. Rutejìmo dismissed Pidòhu's comments.
---

> Tateshyúso is the name for large birds that fly high above the desert, content to ride the winds and play in the clouds. --- Gregor Fansil, *Ornithological Studies of the Kimīsu Region of the Mifúno Desert*

When Rutejìmo woke up, his grandfather was back in his customary place by the fire, mute and staring out into the void. Rutejìmo wanted to walk over and thank him, but then his grandmother would know he had been out of the cave. He didn't want to risk his chance to leave the valley, even if it was just to watch Chimípu going through her rites of passage. Instead, he had to wait until one of them told him he was going on a trip.

"Rutejìmo," announced his grandmother.

He tightened with anticipation, knowing what she would say but not how she would tell him.

"I know you were at the shrine last night."

Rutejìmo's stomach did a slow turn to the side. He took a step back, his bare feet scuffing against the stone ground.

"When you get back"---she focused her bright-green eyes on him---"I will beat you until you scream."

Sweat prickled his brow, and he began to tremble. Rutejìmo glanced toward the entrance of the cave, knowing he could never outrun her. He gulped at his suddenly dry throat.

Tejíko turned her back on him and headed into her map room, moving slower than he had ever seen. "Get packed and leave us."

He watched as she sat down heavily in her customary spot and grabbed a fistful of maps. The paper crinkled loudly as he stared at her. "T-thank you, Great Shimusogo Tejíko."

When she said nothing, he ran to his room and pulled out his travel pack. As he had grown up, the clan had impressed on Rutejìmo what he needed as a courier. Part of the daily rituals was ensuring he could leave at a moment's notice with fresh supplies. It had been only a few days since he last inspected it, but he still pulled out the contents and verified that the rations were fresh, the water clear, and his clothes were in good shape. He also rewrapped a long length of thin rope around a utility knife.

Hoisting the bag over his shoulder, he headed out of the cave and toward the entrance. His heart soared as he ran down to the floor of the valley. It was a familiar path, but somehow it was different. He was leaving to go somewhere other than the nearby valleys for the first time since he was a toddler.

As he jogged through the common cooking area, he saw Pidòhu ahead of him.

Even though they lived in the same valley, Rutejìmo didn't like being near Pidòhu. There was something different about the younger teenager. He was thinner than anyone else, with long, skinny arms and legs. He looked like a leaf about ready to be torn from a branch. As far as Rutejìmo knew, Pidòhu had never won a race, never beaten anyone at wrestling. He was useless in a clan of couriers.

More than once, Rutejìmo wondered why Pidòhu was still in the valley but kept his thoughts to himself. His grandmother beat him the last time he asked, and the lesson still stung along his shoulders and back. Until the rite of passage, Pidòhu was in the clan just like himself.

Rutejìmo accelerated and passed him.

He smiled to himself, enjoying the chance to beat someone. The smile faded as he saw Chimípu racing along a higher path. She jumped off the trail and landed on the one below him. It was the same thing he had done before, but she had leaped further and landed more gracefully than he could ever hope to achieve. Her feet flashed as she sprinted along the next path before she leaped onto the top of one of the storage buildings, over to a second one, and then hit the ground without losing a beat.

Jealousy gnawed at Rutejìmo's heart and he tried to run faster. He would never beat her, but he refused to admit it.

By the time he reached the valley, his heart was pounding and the exertion wrapped around his lungs with a clawed fist. He slowed to a halt as he saw the gathered clan members.

Desòchu was in a knot of a dozen clan couriers and warriors. He was smiling and they were laughing. Hyonèku stood next to him, his hand on Desòchu's shoulder and holding Mapábyo upside down by her waist. His daughter was squealing and thrashing, but laughing just as loud.

Karawàbi and Tsubàyo stood a bit closer to the valley as Gemènyo told a story. Gemènyo's arms waved expansively as he described the scene, and the two watched with rapt fascination.

Rutejìmo scanned the crowds until he spotted Chimípu. She stood right at the threshold of the valley with Jyotekábi and seemed perfectly at ease next to the older woman. Jyotekábi was a tiny and wizened woman with intense green eyes. Unlike everyone else gathered, she didn't wear an outfit for running. Instead, she had a thin silk robe of yellow and green over her shoulders. Underneath, she wore a loin cloth and nothing else. She was also at least two feet shorter than Chimípu but made up the difference in height by standing on a rock to talk to the younger woman.

The yellow was the same shade as the Shimusògo wore, along with red and orange, but the green indicated the Tateshyúso clan. There were only three members of the Tateshyúso in the valley and they kept to themselves along the upper ridges. Rutejìmo had never heard of one of them joining in a mission or a trip, but his grandmother claimed they were guardians of the valley. He didn't believe that since they never showed any clan magic besides occasionally bringing a cool breeze down through the valley on the hottest of days.

"Good morning," panted Pidòhu as he jogged up, "Rutejìmo."

Rutejìmo gave him a distracted nod and stepped aside to let him pass, but Pidòhu stopped next to him. Rutejìmo glanced over at the sweating boy and wondered once again if Shimusògo would ever accept such a weakling into the clan.

"Rutejìmo, I saw you on top of the shrine last night," said Pidòhu.

Rutejìmo flinched and looked over guiltily. "Does everyone know?"

Pidòhu ran a thumb along the ridge of his index finger. He shook his head and took a deep breath. "Gossip is faster than the wind."

Rutejìmo prickled at the comment. "None of us were supposed to be out that late. What were you doing?"

"Fixing Opōgyo. Mapábyo managed to tilt it over one of the paths and crushed the outer casing. My father fixed it, but I had to repair some of the guide wires inside, and that took most of the night."

Rutejìmo didn't understand Pidòhu's obsession with the mechanical dog, but it kept him away from the others. "At least you'll get a break from all that with this trip."

Pidòhu rubbed his hands together, working at the grease stains on his fingertips. "No, I've been dreading this trip for months." He knelt down to dig into his pack.

"Why? Because of Chimípu?" Rutejìmo jerked as he realized what Pidòhu had said. "For months? I thought this was planned last night."

Pidòhu looked up with murky green eyes. "Do you really think that?"

"Yeah," Rutejìmo said with a frown, "how could it be anything else?"

Pidòhu stood up again with a rag in his hand. He gestured with a finger toward the gathered clan members. "Have you ever seen so many for just a simple trip? And did you know that Jyotekábi is coming with us?"

Rutejìmo shook his head.

"This is more than just Chimípu's rite of passage, Rutejìmo."

"Maybe they just want to see her succeed beyond all expectations," Rutejìmo surprised himself with the spite in his voice.

"No," Pidòhu said with a sigh, "if that were the case, there wouldn't be so many. It only takes Shimusògo to make her a true member of the clan. Look at them." He gestured to the ones around Desòchu. "Can't you see the tension? Something has made the clan frightened."

Rutejìmo snorted and turned away. "You're jumping at the shadows, boy." But, as the words sunk in, he peered at the gathered clan and saw what Pidòhu was talking about. Desòchu was the easiest to see the difference in since Rutejìmo had grown up with him. While Desòchu spoke cheerfully, he tugged on his ears and adjusted his knives far more than Rutejìmo had ever seen. Other clan members were toying with their own weapons, and the smiles faded during the lulls in conversation.

"This is a lot more than just Chimípu, Rutejìmo." Pidòhu repeated, as he sighed and turned back to the valley. "You know, sometimes I wonder if I'll ever miss this place."

Startled by the sudden change in conversation, Rutejìmo turned to look back. He didn't see anything, but something in Pidòhu's voice brought a shiver of dread. "You'll be back by the end of the week."

"Do you really think that?"

Rutejìmo squirmed at Pidòhu's sullen words. He lifted his gaze to his own home. An uncomfortable tremble coursed along his spine, and the world felt icy for a moment. He started to doubt himself, but it never occurred to him that the trip was more than just a trip. He gulped to wet his throat. When he turned back, Pidòhu was already jogging toward Desòchu's group.

Grunting with annoyance, Rutejìmo drew his mind back to his journey and raced after Pidòhu.
