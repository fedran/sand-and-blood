---
when:
  start: 1471/4/2 MTR 9::32
date: 2012-09-18
title: Shadows from Sunlight
locations:
  primary:
    - Marriage of Stone and Sand
characters:
  primary:
    - Rutejìmo
  secondary:
    - Chimípu
    - Pidòhu
    - Shimusògo
    - Tateshyúso
  referenced:
    - Nedorómi (Epigraph)
    - Tsubàyo
    - Mikáryo
organizations:
  secondary:
    - Shimusògo
    - Tateshyúso
topics:
  referenced:
    - Courtship of Shimusògo and Tateshyúso (Epigraph)
summary: >
  Chimípu woke Rutejìmo up and insisted they move. It quickly became apparent that the encounter with Tsubàyo worried her. She struggled with her inability to defeat Tsubàyo.

  Pidòhu suggested that Chimípu ran and she did. When she was gone, Pidòhu asked Rutejìmo about running and clan powers. He revealed that he thought the shadows he saw were actually a spirit, Tateshyúso, and showed how he could use it to shade Rutejìmo from the sun and keep him cool.

  Chimípu joined them and they continued along the way as Pidòhu practiced summoning Tateshyúso and shading them.
---

> Tateshyúso cannot be seen, only felt as she passes. --- Shimusogo Nedorómi, *Courtship of Shimusògo and Tateshyúso* (Verse 3)

"Rutejìmo? Jìmo?"

Rutejìmo opened his eyes to see Chimípu crouching at the entrance to his tent.

She was fretting with the handle of the knife. Her body shuddered as she tapped her foot. "Wake up, Jìmo."

He groaned. "I'm up, Mípu. What's wrong?"

"I… we need to get moving."

Rutejìmo yawned and looked past her. It was first light, a beautiful morning right before the spears of light crossed over the sands. It was still cool but the heat was already increasing. "Why?"

"Come on." Her expression faded into a pleading one. "Please?"

He searched her face. She was scared about something. He opened his mouth to speak but stopped at her look. He grunted. "Yes, Great Shimusogo Chimípu."

With relief naked on her face, she stepped back and moved to wake up Pidòhu.

Rutejìmo crawled out of bed and found a spot to relieve himself. He returned to find her digging out breakfast, the wooden boards placed around her. She moved with a frantic energy, practically throwing the plate of food in his lap before serving Pidòhu.

Feeling Chimípu's desperation, Rutejìmo ate quickly.

Pidòhu ate slower, but his movements were steadier, and he wasn't as pale as the day before. He didn't shake, but he still struggled to hold the board up. Rutejìmo reached over and steadied it for him. As he struggled with his food, Pidòhu watched Chimípu carefully. "Mípu? What's wrong?"

"Nothing."

Pidòhu continued to stare at her.

Chimípu glanced up, and then back down at her food. "I said nothing."

His eyebrow rose and his gaze never left her.

After a few seconds of her choking down her food, she slammed her plate down. "Okay, I ruined everything! Better?"

A smile ghosted across Pidòhu's face. "Oh, is that it?"

Chimípu glared at him.

Pidòhu stuck his tongue out at her and resumed eating. Between the bites, he said, "I mean, none of us have screwed up during these rites at all. Of course, forcing us to wake up early is far worse than me falling off a rock and breaking my leg. Or Rutejìmo marking the rocks."

Rutejìmo shot Pidòhu a glare but then focused on eating.

"It isn't that," snapped Chimípu. "It's Tsubàyo."

"What about him?"

"I told him about Mikáryo. I shouldn't have."

"So?"

"He knows that she wants one of us."

Pidòhu nodded and sighed. "And you are worried that he's going to do something stupid like kill one of us to save his own balls?"

Chimípu froze, her fingers inches from her mouth. She sighed and gave a rueful smile. "Damn, yes."

"Well," Pidòhu said with a dramatic shrug of modesty, "I'm pretty observant." He grinned. "Or, I don't have anything better to think about."

Rolling her eyes, she finished her bite.

"Am I right, Great Shimusogo Chimípu?"

"Yes, yes, you are." She stared at her plate. "I shouldn't have told him. And then, when I realized that he had gotten away, I got so angry I had to scream."

"I know. Rutejìmo told me while you were sleeping."

She looked away, her lips pressed into a thin line. "I'm supposed to be the warrior. I don't know if I can protect you against him."

"We grew up with Tsubàyo. He is a bully and a bastard. I have no doubt you will succeed."

"But he did that… thing with the horse. He could be anywhere." She pointed to a large shadow on the side of the rock. "I-I don't know how to defend against that."

"You will."

She sighed and looked out over the rocky plain surrounding them. It was flat in all directions for at least a mile.

Rutejìmo swallowed his food and pointed out to the south. "Why don't you run? It will help."

"No," she said, shaking her head. "We can't leave Pidòhu alone now."

Rutejìmo gestured at her and then to the desert.

"Me?" Chimípu looked surprised. "By myself?"

Rutejìmo grunted. "Yes. Just run. It will clear your head. If Tsubàyo shows up, we'll just… stall."

She stood up, but didn't move. "Are you sure?" There was hope in her voice. "Just a few miles?"

Rutejìmo nodded.

Pidòhu gestured in the same direction. "Go on, but more than a few miles. Just run---"

She disappeared in a rush of air. The wind sucked along her path as she left a trail of blossoming rocks and dust in her path.

"---until you feel better," finished Pidòhu with a grin.

Rutejìmo followed Chimípu's movement with his eyes. She was moving faster than he ever could, ripping along the distance with startling speed. She raced in a straight line for a few minutes, then began to circle around them.

"Jìmo?"

Rutejìmo glanced down at his plate. It was empty. He stood up as he regarded Pidòhu. "Yeah?"

"What does it feel like?"

"Running?" Rutejìmo gathered up the plates and found a spot to scrape them clean. "It is… hard to be angry or upset. When I start moving, I can't really think about anything besides Shimusògo."

"Do you feel anything? Exhaustion? A tickling sensation?"

Rutejìmo thought for a moment before he shook his head.

"I wish I could feel it. You've changed a lot since you started running. You enjoy it, don't you?"

"Yeah," Rutejìmo muttered, "never thought I'd say that."

"Not hard to see why they say that your true self comes out during the rite of passage."

In mid-step, Rutejìmo paused. He wondered who he was, now that he ran with Shimusògo. Shaking his head to clear it, he packed away breakfast, took down the tents, and changed Pidòhu's bandage.

"She's still running," said Pidòhu. He pointed to the cloud of dust circling around them.

"Shimusògo run."

"Should we wait for her?"

Rutejìmo shook his head. "She can catch up no matter how far I drag you. We should get started; it's going to be a hot day."

He made sure Pidòhu was settled into the stretcher. Grabbing the handle, he felt the ache coursing up his raw palms. He could have fashioned makeshift gloves for his hands, but the pain was a constant reminder of his duty. With a grunt, he leaned forward to start moving. After a bit of friction, the ends of the frame started to move and he dragged Pidòhu across the rocky field.

"C-Can I try something?" It was only a few minutes after they started. Pidòhu was stuck on some rocks that looked flat when Rutejìmo headed for them, but the sharp edges caught on the wooden frame, and he had to pull hard to keep moving.

"Of… course…." gasped Rutejìmo.

"Don't tell Chimípu, though, if I fail. I'm not exactly sure what I'm doing."

Rutejìmo chuckled as the sweat dripped down his brow. He freed the frame and resumed dragging Pidòhu. "None of us know what we're doing, Dòhu."

"True."

After a few minutes of silence, Rutejìmo broke it. "Dòhu?"

Pidòhu sighed. "Damn, I almost had it."

"What are you doing?"

"I was thinking about you and Chimípu. And doubting myself."

"Done that, doesn't help."

Pidòhu sighed. "I feel useless."

Rutejìmo chuckled. "Done that. Peed my trousers. Doesn't help, either."

"What if I felt like a burden?" There was an amused tone to Pidòhu's voice.

With a grin, Rutejìmo said, "Done that. Got beaten up. Still didn't help."

Rutejìmo found a smoother path and aimed for it. The stretcher vibrated in his grip over the rough rocks, but dragging grew easier once he reached the path. Mapping out his route, he pulled Pidòhu along.

"All right," Pidòhu said, "let me try again."

"What are you doing?"

Silence.

Rutejìmo shrugged and kept on dragging. Sweat trickled down his back and chest, adding to his discomfort. He couldn't stop, he refused to stop.

Chimípu was still running, circling around them. He wished he would have the chance to do the same. He could see why Pidòhu said he was changing. He had never found joy in moving or running, but he ached for it now. Everything was right when he was chasing the dépa.

"I'm," gasped Pidòhu, "trying to call Tateshyúso. I can see the shadows, but I can't bring them closer."

"How are you calling for her?"

"Calling out… with my mind, I guess. It isn't working."

Rutejìmo pondered it for a moment. "Well, Chimípu and I chase the dépa. I know you can't run with Tateshyúso, but what does she do? Just fly around?"

"Yeah, usually on the hot air up above where the wind is stronger."

He thought about the massive shadows he saw. "How big is Tateshyúso?"

"Bigger than I can describe. Too big to be a real raptor."

"Can you… I don't know. When Chimípu got angry, she said she felt Shimusògo inside her. She was part of Shimusògo. Maybe---"

"Oh!" Pidòhu gasped. "That gave me an idea!"

Rutejìmo smiled and kept on dragging. When Pidòhu didn't say anything else, he let his mind drift off.

And then he saw a shadow in the corner of his eyes. Gasping, he followed it with his gaze as the massive silhouette raced along the ground, spiraling toward them. He looked up, but he couldn't see anything but clear, burning skies.

The shadow continued to circle for a moment, then it passed over him. There was a blessed kiss of shade.

"That felt good for a second."

"Yeah," Pidòhu sounded distracted.

A moment later, the shade of Tateshyúso came back. But, instead of flying past, it hovered over Rutejìmo and Pidòhu, bathing them in coolness despite the heat.

Rutejìmo stumbled and looked up. There was nothing, but the shade felt good against his skin. "I-Is that you, Pidòhu?"

"This… is hard to maintain, Jìmo. I'm going to need practice."

The shadow shot off, and the heat rushed back.

Rutejìmo chuckled. "Well, we have a long walk ahead of us."

"Are you saying I have plenty of time?"

"No, but a bit of shade felt good. And, if someone were to help a fellow clan member drag an injured boy across the desert, I know I'd be appreciative."

"I'm not Shimusògo."

"If our clan spirits are bound together, then so are we. So you can help me deliver you back home."

Tateshyúso's shadow circled around them again, coming closer.

Rutejìmo kept dragging, patiently waiting for the shade he knew would come.
