---
when:
  start: 1471/3/28 MTR 12::19
duration: 1 m
date: 2012-03-04
title: Rivals
locations:
  primary:
    - Shimusogo Valley
characters:
  primary:
    - Rutejìmo
  secondary:
    - Desòchu
    - Karawàbi
    - Tsubàyo
  referenced:
    - Palasaid (Epigraph)
    - Tejíko
organizations:
  secondary:
    - Shimusògo
topics:
  referenced:
    - Rearing Children in the Mifúno Desert (Epigraph)
summary: >
  When Rutejìmo tried to deliver breakfast to his grandparents, he is accosted by Karawàbi and Tsubàyo, two teenagers who bullied him frequently. Both of them forced Rutejìmo to spill the bowls of food, but Desòchu caught them before they hit the ground. Coming back, he gave the two bullies a lecture and sent them off. And then he told Rutejìmo not to get revenge. Rutejìmo promised, even though he planned on doing so anyways.
---

> The desert clans aren't interested in the third or even the second best. All they care about is who is better and for how long. --- Palasaid Markon, *Rearing Children in the Mifúno Desert*

Rutejìmo balanced three heavy stone bowls on a plank as he navigated the twisted path leading up to the family cave. The morning sun seared his skin and it was already heating up the rock beneath his feet. In a few hours, it would be too hot to do anything besides rest in the cave.

He took a deep breath. When he inhaled the smells of roasted meat, eggs, and fresh-baked bread, his stomach twisted into a knot. His grandmother insisted he wait to eat with her instead of with the rest of the clan. He peered back over his shoulder to the eating area where Desòchu wolfed down food and joked with the others. He wanted to go back and listen to his brother's tales, but it would mean more punishments if he disobeyed his grandmother.

When he turned back, someone stood right in front of him. He lurched to a stop to avoid running into the broad-chested teenager. The bowls threatened to tilt over, and Rutejìmo swore as he struggled to keep them balanced on the wooden plank.

"Almost got you, Jìmo," said Karawàbi with a chuckle. Even though he was two months younger than Rutejìmo, he stood several inches taller. He was also considerably stronger and faster. His dark skin glittered with flecks of sand that covered him from head to toe.

Rutejìmo fought the urge to step back. "What do you want, Wàbi?"

Karawàbi shrugged but didn't move out of the way. "I'm bored."

Rutejìmo heard someone crunching on the rock as they walked up behind him. He sighed and didn't look back. "Good morning, Bàyo."

"That's Great Shimusogo Tsubàyo to you." Tsubàyo had a rough, gravelly voice. When he was a young child, he had fallen face-first into an oil-filled pan and the burns never healed properly. Where Karawàbi was tall and looming, Tsubàyo was short and slender. Ripples of hardened flesh covered his chin, throat, and a wedge down his chest.

Rutejìmo stepped to the side, not wanting to be pinned between the two along the crumbling stone path. "You aren't great yet."

Tsubàyo stepped forward, a glower on his face. "I'm your better, boy, and don't you forget it."

Rutejìmo tried to move away, but the clinking of the bowls on the plank halted his movement. He looked down at the steaming food with a sinking feeling. He couldn't fight Tsubàyo while carrying breakfast.

"I thought so," said Tsubàyo in a satisfied voice.

Looking up, Rutejìmo realized the other boy interpreted his silence for agreement. His hands tightened on the plank. "You aren't my better, braggart, and you never will be."

A glare darkened Tsubàyo's face. He stepped forward and swept his hand up.

Rutejìmo dodged Tsubàyo's attempt to knock over the bowls, but he stumbled into Karawàbi and tripped over the larger boy's outstretched foot. Rutejìmo dropped to his knee to avoid falling over the edge of the path, but the bowls slipped from the board and plummeted down the side of the valley.

"Sands!" Rutejìmo screamed as he flailed helplessly.

"Oops," said Tsubàyo in a sardonic tone.

The ground shook as a blast of wind blew and the flash of a bird raced past them. Rocks tore at Rutejìmo's side and face. Coughing, he managed to focus just as Desòchu caught the third bowl. The other two rested in his other hand. Wind eddied around Rutejìmo's older brother as he gracefully spun around to prevent the food from slipping.

Desòchu glanced up and then stepped forward. He disappeared in a cloud of dust, and a plume of wind streaked to the switchback at the end of the trail and up toward them.

Rutejìmo yanked his head around, hoping to see Desòchu racing, but his brother had already come to a halt in front of the three teenagers. A heartbeat later, wind blasted around them from the wake of his speed. Sand fell off the rescued bowls.

"And what are you three boys doing?" There was an easy smile on his face, but a hardness in his voice.

Rutejìmo looked up from the ground, still clutching the plank.

Tsubàyo cleared his throat. "Um, nothing, Great Shimusogo Desòchu."

"Yes," added Karawàbi, "we are doing nothing."

"Funny," Desòchu chuckled, "because I was pretty sure I saw you tripping the brat over there."

Tsubàyo tried to step away from Desòchu, but Rutejìmo's older brother followed him.

Desòchu casually reached out with one of the bowls.

Rutejìmo saw that he was handing it over and he held up his hand to take it.

As soon as the bowl left his grip, Desòchu reached over and dropped his hand on Tsubàyo's shoulder. Tsubàyo winced as he tightened his grip on the joint between the neck and shoulder. "Now, the boy is in trouble right now with my grandmother. Are you sure you really want to annoy Great Shimusogo Tejíko when you spill her breakfast?"

As he spoke, Karawàbi stepped back and held up his hands.

Tsubàyo tried to walk away, but Desòchu yanked him closer and spoke directly to his face. "Well, Bàyo?"

"No, Great Shimusogo Desòchu."

"Good. Now, run along and stay out of trouble."

As soon as Desòchu released Tsubàyo, the teenage boy broke away. He ran down the path toward the bottom of the valley with Karawàbi following. Rutejìmo followed him with his eyes, watching as they headed toward the eastern end of the valley where there were a few caves that the children used as hiding spots. Rutejìmo knew which one Tsubàyo favored. As soon as his grandmother allowed it, Rutejìmo planned on finishing their discussion in private.

"Jìmo."

Rutejìmo set aside his thoughts and glanced over to his brother.

Desòchu clasped his hands together and regarded Rutejìmo. He wasn't smiling anymore, but Rutejìmo didn't recognize his expression. Desòchu looked torn, as if he were struggling with something.

"Are you planning on… finishing that with Bàyo?"

"Of course."

"Don't."

"Why not?" Rutejìmo snapped. "He keeps insisting he is better than me. And I know he isn't. He just thinks if he can trick me into saying it, somehow he will---"

"Jìmo!"

Rutejìmo clamped his mouth shut.

"Tonight"---Desòchu patted him on the shoulder---"just for tonight, you need to behave. Just be better than you normally are. You know… try?"

Rutejìmo shook his head. "But he---"

"Little brother." Desòchu pulled his hand back. "You need to behave. Please? No fighting and no sneaking around. And don't get revenge on Bàyo."

Rutejìmo opened his mouth to respond, but at the look in his brother's eyes, he closed it. "Yes, Great Shimusogo Desòchu."

"Promise me. Promise on Shimusògo."

Rutejìmo's skin prickled at the intensity of his brother's words. He gulped, then nodded. "I promise on the blood of Shimusògo that flows through my veins."

Desòchu nodded with approval. "Go on, Grandmother is waiting."
