---
when:
  start: 1471/3/47 MTR 5::37
date: 2012-09-08
title: Quiet-Voiced Threat
locations:
  primary:
    - Lonely Stretches of Jade
characters:
  primary:
    - Rutejìmo
  secondary:
    - Mikáryo
    - Chimípu
    - Pidòhu
    - Shimusògo
  referenced:
    - Mioshigàma (Epigraph)
    - Tsubàyo
organizations:
  secondary:
    - Shimusògo
    - Pabinkúe
summary: >
  Rutejìmo sat on guard while the others slept. He struggled with his fear of darkness and his own feelings of inadequacy. He also wondered what had changed between him and Chimípu, it felt like she had became a sudden friend with their shared experiences of chasing Shimusògo and learning how to fire rocks at each other.

  While he was thinking, he heard a noise. At first, he was worried but then assumed it was his imagination. But then he was surprised when a woman pressed a knife to his throat. He lost control of his bladder, but was helpless to do anything.

  Chimípu woke up and attacked the intruder, Mikáryo. They fought in the darkness until Mikáryo finally pinned Chimípu to the ground. When the older woman realized that Chimípu, Pidòhu, and Rutejìmo were all teenagers, she relaxed. She claimed she was looking for the Shimusogo who killed her sister and stole her horse. When they figured out it was Tsubàyo that killed her, Mikáryo threatened to take his soul with an unnamed blade.

  Mikáryo gave them three nights to deliver Tsubàyo or she would kill one of them. She disappeared into the night, leaving the three alone.
---

> The Moon Clans, also known as the clans of night, gain power from the icy darkness. --- Kakasaba Mioshigàma

Rutejìmo sat on the ridge of the rock. He stared out into the darkness, seeing nothing but not daring to close his eyes. The last time he did, he woke up minutes later with a surge of guilt and fear. He didn't dare do it again, not with the two sleeping below counting on his vigilance.

The only illumination came from the few flickering stars above him. He spent the first hour amusing himself by counting them and trying to remember their names; there were only a hundred or so visible, but his exhaustion made it difficult for him to remember more than thirty.

He wanted to light up the glow egg, but it would only highlight his location and do nothing to push back the void. Rutejìmo was stuck listening to the wind around them and his own thoughts.

Something had changed that day. Chimípu had smiled at him, an honest smile that wasn't mocking or insulting. And then she taught him how to fire the rocks at high speed. He was horrible at it and nearly took out her foot with one shot, but the rush of using clan magic burned bright in his veins. He had somehow earned Shimusògo's respect, even though he didn't deserve it.

In the distance and to his right, something moved, and he heard the crunch of a weight on rocks.

Rutejìmo gripped his knife, wary of accidentally stabbing Chimípu or Pidòhu. He had heard enough horror stories of accidents while guarding to be careful. His breath came faster, and he strained to listen.

When no other sounds rose up, he relaxed but didn't release the knife. He hated the darkness that smothered him. On the moonless nights, it was worse. There was nothing to see or focus on.

It wasn't something he had ever experienced before. The valley was always lit. And when the clan traveled, they had a fire or glow lamps pushing back the night, not to mention someone standing on guard.

A warm breeze tickled the back of his neck. He spun around, fighting a scream. He flailed his hands out, but he only felt empty air. Realizing his blade was out, he set it carefully down in his lap with a flash of embarrassment. He didn't know how or when Chimípu would take her turn.

Rutejìmo also didn't know if he would detect danger before it attacked them. But the clan always had guards at night, and it felt right to sit there, even blind.

A shiver ran down his spine.

He inhaled a shuddering breath. Straining, he tried to listen but all he could hear was his own pounding heart.

There was sharp prick at his neck. He waved at it, to chase away the insect biting him.

But his hand struck something hard and smooth. With a gasp, he gingerly touched it until he identified it: a tazágu, a fighting spike. A whimper rising in his throat, he reached back to trace his fingers along the weapon. It was about three feet in length with a leather-and-hemp braided handle.

A strong hand covered his mouth, pressing down against his jaw. The gloved fingers dug into the side of his cheek as he was pulled into someone's chest and against small breasts underneath thin, layered fabric. The ridge of the woman's hand pressed against his nostrils, cutting off his breath.

To his embarrassment, Rutejìmo lost control of his bladder. He felt the hot urine pouring down his leg, and the stench of it added to his humiliation.

"Damn the darkness," the woman whispered in his ear, "you're nothing but a kid." She had a light accent from the southern reaches.

He couldn't breathe, not because of her hand---his lungs refused to move. He tried to shift, but a sharp stab stopped him. She didn't break skin, but his entire world was focused on the point poised to drive into his throat.

"Who are you?" she whispered. Her grip loosened over his mouth. "And if you call the others, you'll be dead before they wake."

Rutejìmo sobbed, trying to calm himself and failing. His shoulders shook, and tears ran down his cheeks. He was a failure and he was going to die in the dark.

Suddenly, the woman shoved herself back, the point of her weapon leaving a burning line across his throat.

Rutejìmo clapped his hand against it, terrified she had cut his throat, but only a trickle of blood damped his fingers.

A thud vibrated through the rock as someone landed on it. There was a blast of air as the second person accelerated away, leaving only the scent of her passing---Chimípu. Wind howled around him, and he lost his balance. With a scream, he hit the ground, and the impact drove the air from his lungs.

As Rutejìmo struggled to breathe, he could hear metal crashing into metal. It came fast, rapid parries and attacks, but there was no noise from the fighters themselves. The fight circled around the rock but he could only tell by the grunts from Chimípu and the other woman, the crunch of sand and rock as they spun around, and the occasional hiss of pain.

Rutejìmo didn't know how Chimípu was fighting in the pitch-darkness, but it sounded as though she was holding her own. More importantly, after a few seconds, he could still hear the sound of fighting.

Light burst from an impact, and he saw the runes of Chimípu's blade flare with the clash against the other woman's spike. Each letter was bright as sunlight but faded instantly. With the next attack, the runes flashed again. As Chimípu rained down blows, their attacks became a lighting storm of attack and parry.

Rutejìmo's lungs started to work. He inhaled sharply and struggled to his feet, but Pidòhu held him down.

"No, Jìmo."

"But---"

"You will only get in the way."

"How is she fighting?"

"Shimusògo," Pidòhu said as if it explained everything.

"Dòhu, I don't---"

Metal snapped loudly, halting Rutejìmo's words. Something whizzed past him and hit the rock wall near his head. It rang out loudly, almost deafening him, and then landed in his lap. Reflexively, he reached down, but when he encountered searing metal, he snatched his hand back. Scrambling to his feet, he slammed his head into the rock, and bright sparks exploded across his vision.

Silence crushed them, an overpowering tension as Rutejìmo strained to identify the winner.

"Darkness," came the woman's voice, annoyed and frustrated, "you're all children. This is a damn rite, isn't it?"

Pidòhu called out, his voice a broken whisper. "Is… Is Chimípu… alive?"

The woman scoffed. "Pathetic."

Icy blue light began to glow in the darkness. It was the color of moonlight and came from the runes along the woman's tazágu and quickly formed a pool of light around her and Chimípu.

She was kneeling on Chimípu's stomach, her tazágu against Chimípu's left breast and aimed straight for her heart. The woman held the weapon in place with one hand and had her other palm pressed against the base of the weapon. Her body was a coiled spring, and there was no question that she was ready to kill Chimípu.

Chimípu's head was off the ground as she glared at her opponent. Her hands were balled into fists as she trembled. A tear in the corner of her eye sparkled in the light, but it refused to surrender to gravity.

The woman never took her eyes off Chimípu. "Who are you, girl?"

Chimípu's jaw tightened, but then she answered. "Shimusogo Chimípu."

"And your boys?"

Chimípu glanced over at Rutejìmo, and he shivered at her look. He could see the frustration, helplessness, and rage boiling in her gaze. She tightened her lips into a thin line for a moment and glared up at the woman pinning her. "The injured one is Pidòhu and the other is Rutejìmo, both of Shimusògo."

"I am Mikáryo and I speak for Pabinkúe. I'm looking for my sister's horse."

"We don't have a horse," said Chimípu.

"I know that, but one of your clan stole her. And I can smell that one." She pulled the spike away and used the point to aim directly at Rutejìmo.

Rutejìmo froze, his entire body clenching tightly with fear.

"Which means that he was there. And that is guilt, if only by association."

Mikáryo still wore the dark outfit Rutejìmo saw at the campsite. It consisted of long lengths of a thin, black fabric wrapped around her body, granting her protection from the sun while sacrificing mobility. He had seen a similar outfit before, from a traveling smith, and knew there would be wires in the fabric. When she stood up from Chimípu, there wasn't even a whisper of sound.

The woman tossed her tazágu into the ground, stepped back, and then put her arms behind her back. She turned back to Chimípu, who was scrambling to her feet. "Girl, this is your rite of passage?"

Chimípu flushed. "Yes," she said. She dropped the shattered hilt of her knife to the ground.

Behind Mikáryo, a horse stepped out of the darkness. He was as black as the cloth around her. Dark eyes glittered as he took in the people, then snorted. His tail snapped back and forth but made no noise.

Mikáryo stepped to the side of the horse as she addressed Chimípu. "Then tell the adults following you that Pabinkúe demands a life for a life. If you don't bring me the horse thief and my sister's murderer, I will take one of yours."

Both Rutejìmo and Chimípu gasped.

Rutejìmo stared at Mikáryo in shock. "Tsubàyo…?"

Everyone turned toward him.

The woman had a look of distaste. She had dark tattoos covering her face and arms. They were elegant dark prints of horses and runes, and he could barely see her brown skin underneath the black ink. "Shimusogo Tsubàyo? Is he one of yours? Another child?"

Chimípu glared at Rutejìmo, but nodded. "Yes, Great Pabinkue Mikáryo. He is also on his rite of passage." The muscles in her jaw jumped at using the honorific version of Mikáryo's name.

"Then, if he is the one, I will take his name." Mikáryo pulled a second tazágu from behind her back. It was different than the first one, wrapped in a dark blue leather with black rope, but Rutejìmo also saw that it was unnamed. No runes identified the length of the spike. According to tradition, a blade was named for the first thing it killed. It would also keep a portion of the victim's soul.

Rutejìmo shivered, then he remembered something Mikáryo said. "There are no adults. They abandoned us."

Mikáryo scoffed and sheathed her weapon. She kept her other hand behind her back. "It would be best, boy, if you just kept your mouth shut and let the big girls speak."

Rutejìmo closed his mouth with a snap, his cheeks burning with humiliation.

"Your elders are close enough, near enough to watch the stupid things you've done but far enough you can't hear their laughter. And, between a boy who can't control his dick---"

Rutejìmo blushed even hotter and clamped his jaw tight to avoid yelling at her.

"---and one who managed to break his leg probably in the first hour, I'm guessing they are laughing so hard they are bent over in pain." She turned her attention back to Chimípu, looking her over with a sneer. "Of course, they say the more suffering during your rite, the closer you'll reach your spirit. I bet babysitting these two screwups"---she gestured to Rutejìmo and Pidòhu---"is getting you real close to your Shimusògo, isn't it?"

Chimípu stood there, back straight and hands balled into fists.

"Don't worry, girl, I'm not going to kill any of you tonight."

In the uncomfortable silence, Mikáryo picked up the glowing tazágu and caressed its length. Darkness plunged across their shelter as the runes snuffed out.

"Remember," her voice drifted from the darkness, "you may be nothing but children, but that won't stop me from killing one of you. You have three nights to find my sister's murderer. Until then, you have no reason to fear me or my clan. But, if you don't… I don't care which one of you dies."

And then nothing. No scrunch of sands or the nicker of a horse. Just the faint breeze.

Rutejìmo held himself still for a long moment, heart pounding.

When a flickering glow filled the site, he jumped back and slammed his head against the rock. Clutching his head, he sank to the ground.

Chimípu held up a glow egg. "No reason to hide if she found us so easily. We'll use light for the rest of the night. Jìmo?"

"Yes, Great Shimusogo Chimípu."

"Clean yourself up," she sniffed, "and then help me move Pidòhu. No reason we have to smell you all night, either."

Humiliated, Rutejìmo activated his own glow egg and sulked into the darkness. He waited until he was out of sight and hearing before he began to cry.
