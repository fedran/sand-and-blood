---
when:
  start: 1471/3/28 MTR 11::71
duration: 2 m
date: 2012-02-26
title: Morning
locations:
  primary:
    - Shimusogo Valley
  referenced:
    - Wamifuko City
    - Ryayusuki Valley
characters:
  primary:
    - Rutejìmo
  secondary:
    - Desòchu
    - Gemènyo
    - Hyonèku
    - Mapábyo
    - Opōgyo
    - Panédo
  referenced:
    - Laminar (Epigraph)
    - Tachìra
    - Ojinkomàsu
    - Tejíko
    - Yutsupazéso
    - Nidohána
    - Kizúchi
    - Tachìra
    - Chimípu
    - Mifuníko
organizations:
  secondary:
    - Shimusògo
  referenced:
    - Ryayusúki
topics:
  referenced:
    - Growing Up in the Desert (Epigraph)
purpose:
  - Introduce a number of new characters
  - Hint at the beginning of the industrial age
  - The concept of no accents on adjectives and ownership
  - Show that the clan is caring, even when kids are in trouble
  - Establish that there are loving relationships
  - Show that Gemènyo and Hyonèku are best friends
  - Hint at events in Flight of the Scions
  - Elders testing Rutejìmo at which girls he would be interested in
  - Introduce how the clan magic works
  - Introduce Shimusògo
  - Show that males and females are equal
  - Hint at ideas of night and day clans
summary: >
  Rutejìmo was sent out to get breakfast for his grandparents. Along the way to the valley floor, he got distracted by Mapábyo, Hyonèku's adopted daughter, riding on top of Opōgyo, one of the clan's mechanical dogs used for dragging heavy loads around. They talk for a short while before she asked him to come with her to take the load up to her father, who is at the lookout above the valley entrance.

  Even though Hyonèku had caught him the night before, Rutejìmo agreed. When they get there, Hyonèku started to read a naughty letter from his wife before Gemènyo interrupted him. Embarrassed, Hyonèku focused on his daughter while Gemènyo talked with Rutejìmo more.

  As they are speaking, runners from the clan arrive. The lead runner is Desòchu, Rutejìmo's brother. Rutejìmo ran down to meet him.
---

> In a culture that prides itself on survival and relationships, punishments frequently involve isolation. --- Laminar Gold, *Growing Up in the Desert*

Yawning, Rutejìmo pushed aside the heavy curtain covering the entrance to his family's cave and stepped out into the brilliant desert sunlight. Automatically, he whispered a quick prayer to Tachìra, the sun spirit. He still did the pointless ritual because if someone caught him skipping the well-remembered words, he would spend a week doing the more noxious chores around the clan's valley. He was already in a great deal of trouble and had no reason to add more.

He looked down into the valley. Only a few dozen people, all of them elderly or children, were making their way toward the cooking fires. The able adults were away, spread out across the desert as they delivered messages, contracts, and mail to the other clans.

Rutejìmo couldn't wait until the rite of passage would let him join his brother Desòchu on the sands. There was no set time when the clan elders would allow him to take the rite. He wasn't even sure he would know in advance that it had started. He'd heard of children being plucked from their beds in the middle of the night and tossed into the desert. Gemènyo's rite started when he was caught drinking too much fermented mare's milk, but Chimípu's father started his with pomp and ceremony.

He sighed and tore his thoughts back to the present. It would happen when he least expected it, and there was nothing he could do to speed it up.

To his right, he heard rhythmic thumping and the hiss of steam. He watched as Opōgyo, the oldest of the clan's mechanical dogs, came tromping up the beaten path. Made of iron and brass and powered by an arcane fire device, it stood shoulder-to-shoulder with Rutejìmo and easily weighed ten times his own weight. It was mostly legs and pistons with a pitted metal barrel for a chest. Despite water being precious in the desert, Opōgyo remained valuable for its tireless strength and ability to haul tons from one end of the valley to the other.

The dog struggled with the steep trail as it dragged a large sled covered in boxes and bundles. Steam escaped from the joints on its shoulders and back right leg as it steadily chugged forward.

"Come on, walk faster!" cried a young girl, Mapábyo. She bounced on Opōgyo's back as she encouraged it to walk faster by smacking its metal ears. Her movement caused the mechanical dog to stagger and jerk.

Rutejìmo shook his head. "Pábyo! Get off Opōgyo and let it do its job!"

Mapábyo, Hyonèku's adopted daughter, slid off. Like all desert folk, she had dark skin and green eyes. But where she was as dark as obsidian rock, Rutejìmo was the softer brown of sun-bright soil. She wore a simple dress of white, which was startling against her dark skin. A bright yellow ribbon cinched it around her waist, and she had a matching one in her long, black hair.

In contrast, Rutejìmo wore a pair of white cotton trousers and remained bare-chested. A few sparse black hairs dusted his pectorals. The only representative traits of the Shimusògo were hard, muscular legs and lean bodies adapted to running across the desert for hours.

Mapábyo bowed her head as she said, "Sorry, Jìmo. Mípu said I could ride."

Rutejìmo tensed at Chimípu's nickname. He forced himself to be polite. "How is Chimípu's mother? Has she beaten the poison?"

He and everyone else in the clan knew Chimípu's mother wouldn't survive, which was why Chimípu asked to hold off on her own rites of passage for her mother's final days. Unlike Rutejìmo, she was important enough to dictate the terms of her rites.

"No," sighed the girl. She patted the mechanical dog. "And that makes Shimusògo Opōgyo sad."

He fought a sudden urge of annoyance. She had been struggling with speaking clearly for months and none of the elders were correcting her. "It's Shimusogo Opōgyo. You only say Shimusògo when you talk about the spirit or the clan itself."

"Why?"

He shrugged. "It's just the way it is. You are Shimusogo Mapábyo and you are a Shimusògo. Otherwise, you are saying all of the Shimusògo and also Opōgyo are sad."

She looked up at him, her eyes shimmering. "But, isn't the entire clan sad too because she's dying?"

Rutejìmo saw tears welling in her eyes and he felt a pang of despair himself. He changed topics before Mapábyo started to cry and he found himself sputtering. "Well, still, don't jump on Opōgyo."

"J-Jìmo?" She ran over and grabbed his leg. "Would you come with me? Up to Papa?"

He looked down at the girl clinging to his leg. He had been told by his grandmother to get breakfast for his family, but he had no desire to return to his furious grandmother. He made a point of sighing dramatically. "All right, but only if you grab something off the sled."

With a brilliant smile and no hint of her sorrow from seconds before, Mapábyo ran over and hauled a large bucket off. It hit the ground with a thud. Grunting, she dragged it behind her. Rutejìmo reached picked up another two buckets. He staggered under the weight, but the mechanical dog picked up the pace as it continued to plod up the path. Behind it, the sled scraped along the stone.

By the time they reached the end of the valley, Rutejìmo's stomach was rumbling and his mood had darkened. His back hurt from carrying the two buckets. Mapábyo had swapped out her own bucket for a much lighter box, but she was also trudging. They came around the switchback that led to the outcropping over the entrance of the valley. There were always guards there, warriors and couriers of the Shimusògo clan who were healing from injuries or just resting between jobs.

Even though he knew that Hyonèku would be there, Rutejìmo's stomach lurched when he saw him standing at the end of the path. Gemènyo sat next to Hyonèku and the two were chatting while they kept an eye out on the desert stretching out beyond them.

Mapábyo saw Hyonèku and dropped her box. "Papa!" She raced forward to grab his leg.

Hyonèku knelt down and swept her into a tight hug. "Hello, my little desert flower. Did you come up to help me guard?"

"No… but I brought food! And supplies---and Mama gave you a letter."

Rutejìmo set down his buckets, thankful for a small break. He ducked his head to avoid attracting attention.

"A letter? Why would she write a letter?" Hyonèku looked curious as Mapábyo dug into her simple shift and pulled out her travel pouch. It was small and filled with mementos of her life from before her parents died and she was adopted by Hyonèku and his wife. It took a second for her to find what she was looking for.

As she handed him a balled-up piece of paper, Hyonèku gave her a reproaching look. He smoothed it over his thigh and flipped it over. His cactus-green eyes moved back and forth as he read the neat script.

"What does it say?" asked the little girl.

Hyonèku's cheeks grew darker. "It's, um, a story."

"About what?"

"About Ojinkomàsu, one of the four horses of Tachìra."

"Really!?" Mapábyo's voice grew excited as she hopped up and down. "Can you read it to me?"

Her father didn't start reading aloud. Instead, he shifted his feet and his face darkened with embarrassment. Rutejìmo turned so his smile couldn't be easily seen.

Gemènyo peeked over Hyonèku's shoulder for a moment. Then, he smiled broadly. "I'm not really sure one should---"

Hyonèku yanked the letter away and balled it up.

"---ride a horse that way. Of course, I didn't know you named a horse after your wife."

Hyonèku spun around, his face flushed with embarrassment. "Gemènyo!"

Gemènyo glanced over at Rutejìmo and gave him a wink. He turned back to Hyonèku. "No, I want to hear about this story. It sounds… fascinating." His voice dripped with amusement.

Hyonèku shoved the letter into his belt. "Later. Never."

"Papa?" asked Mapábyo, obviously not seeing the significance of Gemènyo's comments.

"Later, flower." Hyonèku looked embarrassed as he turned and seemed to notice Rutejìmo for the first time. "Rutejìmo? I would have thought after last night your grandmother wouldn't have let you out of the cave."

It was Rutejìmo's turn to look uncomfortable. "I was told to get breakfast." He sighed. "And not to delay."

"You can't get much farther from the cooking fires than out here." Hyonèku gestured to the wide expanses of desert behind them. From the height of the perch, Rutejìmo could see miles of sand and rock. To his right, smoke rose from the Ryayusúki clan's valley. A pair of horseback riders raced from the valley as they followed a trail toward Wamifuko City.

"Mapábyo was having trouble with Opōgyo. I decided to help."

"You did, did you?" Hyonèku looked surprised and happy.

"You mean," said Gemènyo as he came over to clap Rutejìmo on the shoulder, "you were actually being responsible for once? Nèku, I think I'm dying. Mapábyo, run down to the village and get the old witch. I must be poisoned, for I'm hallucinating. If Rutejìmo is behaving, the world is about---"

Rutejìmo glared at Gemènyo. "Drown in sands, old man."

Gemènyo chuckled. "Sure you want to try that? I can run circles around you."

Rutejìmo realized he was dangerously close to disrespecting one of the clan's warriors. He bowed his head. "Sorry."

"Don't be," said Gemènyo, "I remember what it was like before my rites. I wanted to go out there"---he pointed to the desert---"so badly I was causing trouble up and down. Just ask Hyonèku when he's drunk. He was right next to me, getting bitched out by Yutsupazéso. You would think the old woman would be nice to her youngest grandson, but Hyonèku always got the worse of the punishments. Of course, he was also the one---"

"Gemènyo," said Hyonèku in a tense voice, "help Jìmo unload the sled so they can get breakfast."

Mapábyo tugged on Hyonèku's shirt. "Papa, do I have to go back? I want to eat with you."

Hyonèku swept up his daughter and kissed her on the nose. "Can I eat you?"

She giggled. "No."

"I bet you'd be delicious."

"No, I'm not!" she said, still giggling.

Rutejìmo smiled and turned back to the sled. He ran his hands along the mechanical dog and found the lever that would switch it to standby mode. He flipped it and the dog shuddered once before dropping to its knees. It curled up to arch its back. Two panels opened along its spine, coils of metal rose out of the device's chest, and a loud hum rang out. The air shimmered with heat as steam hissed from its joints.

"That thing needs repairs again, huh?" muttered Gemènyo as the warrior came around to grab a stack of boxes from the sled.

"I guess," said Rutejìmo as he picked up his buckets, "I don't really understand things like that. I know it isn't supposed to be leaking steam like that."

Gemènyo took him to a small hut built into the side of the valley. He set down his boxes and pointed to an empty spot.

Rutejìmo set down the buckets in the indicated spot.

The older man stopped Rutejìmo before they left. "I'm serious, Jìmo, just give it a little time. You'll be a man soon enough."

"I know, I'm just…." He didn't have the words.

"You'll be able to impress Mípu."

Rutejìmo glared at the older man. "I'm not interested in Mípu."

"Oh, looking to bind with Hána? Zúchi?"

Rutejìmo grew more embarrassed as Gemènyo listed all the girls around his age. He was old enough that he was uncomfortable with the differences between male and female. He didn't like to be reminded about his awkwardness.

"You know, if you are waiting for Mapábyo, you're going to wait a long time. She's at least nine years younger than---"

"Gemènyo!" The blush burned hotly on his cheeks.

Gemènyo winked again. "Come on, I'm just giving you a hard time. Let's get this sled cleared off as fast as we can. I'm hungry."

It took three more trips to finish unloading. As Gemènyo took the last load, Rutejìmo untwisted the bolts that held the sled together and broke it down. The individual slats of wood folded neatly together, and a rope bound the entire thing.

"Anything else?"

Hyonèku looked up from where he and Mapábyo were wrestling. "Thank you, Jìmo."

Rutejìmo bowed. As he stood, he caught movement across the desert. Four lines of dust rose as runners sprinted toward the valley. He felt his heart beat stronger as he saw two of the plumes pull ahead of the others.

"You know, Hyonèku," said Gemènyo as he watched the runners with a grin, "if you weren't wasting time with your flower, you'd know the village was being attacked."

Hyonèku was on his feet in a flash. "What the… sands! Gemènyo, those aren't attackers! That's clan!"

Gemènyo grinned but said nothing.

Rutejìmo stepped forward, peering over the bright desert. He could just make out people running across the sands in front of the plumes. "Is it Sòchu?"

There was a flash of light over one of the runners. The translucent shape of a shimusogo dépa, the flightless bird named after the clan, appeared and shrank into the body of the runner. As it faded away, the runner accelerated and left the others in a cloud of dust.

Gemènyo laughed. "Yeah, it's your brother."

Rutejìmo's heart lurched in his chest. "Desòchu!" He ran back along the trail, then stopped as he looked over his shoulder at the resting mechanical dog. He knew he shouldn't leave the device behind, but he wanted to meet his brother.

Hyonèku waved him away. "Go on, I'll take care of Opōgyo."

Rutejìmo bowed. "Thank you, Great Shimusogo Hyonèku." He waited until Hyonèku waved again, then spun on his heels to race down the path. He found a safe spot and jumped over the side. His bare feet caught the rocks, and he slid down until he hit the valley's main trail with an impact that rattled his bones. He ran past the threshold of the valley and into the rock-covered sand beyond.

Racing toward him was Desòchu, his arms and legs pumping as he sped across the sands. The cloud beneath him flashed with lights and power as the ghostly images of the dépa appeared and faded over the other runners.

"Sòchu!" Rutejìmo waved for his brother.

Desòchu turned toward him, and there was another flash of light. Translucent feathers appeared in the dust cloud as his form blurred, then he disappeared. A line of footsteps shot across the desert, and the sand rose up in a dark cloud.

Rutejìmo braced himself and shielded his face as the cloud slammed into him. Grains of sand peppered his arms and body. The air was hot and tight in his lungs, making it hard to breathe. Then a wave of force knocked him off his feet. As the wind spun around him he saw a flash of dark limbs, hands, and feet.

Desòchu reappeared only a few feet away. His arms spread wide as he swept Jìmo into a powerful hug and picked him up. The impact of his movement carried them almost a chain into the valley before Rutejìmo could get his feet underneath him and brace them.

"Jìmo!" laughed Desòchu, "it's been weeks!"

"Big brother." Rutejìmo looked up at his older brother and smiled.

Desòchu was muscular but slender. His hair was long and black, pulled into a tail wrapped in leather. He was bare-chested and glistening with sweat. "You are taller, aren't you?"

"No." Rutejìmo blushed.

"No, I'm sure you're taller. Your legs are in good shape too. Soon, you'll be running with me. Though"---he pressed two fingers to one of Rutejìmo's bruises---"I see that Great Shimusogo Tejíko was beating you again."

As he spoke, Desòchu's companions came running up. They were all female, small-breasted, and dark-skinned. All of them were armed like his brother, with throwing knives and hunting bolas. Sweat darkened their pants and the cloth wrapped around their chests. They all wore the colors of the clan: yellow, orange, and red. Sweat ran in rivulets along their skin as they jogged in place to cool down.

Rutejìmo returned his attention to Desòchu. "Big brother, I thought you were in Wamifuko City?"

"Ah, we were. But Nédo wanted to get home to her husband." One of the warriors bowed as her name was mentioned. "So we ran through the night. Shimusògo kept us company the entire time."

Shimusògo's magic was running and speed. It flowed through older warriors' veins like blood, and very little could outrace the runners of Shimusògo.

"What about the clans of night?" asked Rutejìmo. "What if they attacked you?"

His brother laughed and clapped Rutejìmo on the shoulder. "They could never catch us. Come on, I'm hungry and I want to hear what trouble you got into."

Rutejìmo blushed even though he was excited for his brother to be home. He tugged on Desòchu's arm and dragged him toward the cooking fires.
