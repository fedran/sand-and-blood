---
when:
  start: 1471/3/43 MTR 10::77
date: 2012-05-04
title: The Morning Sun
locations:
  primary:
    - Three Falls Teeth
characters:
  primary:
    - Rutejìmo
  secondary:
    - Pidòhu
    - Karawàbi
    - Tsubàyo
    - Chimípu
    - Tachìra
  referenced:
    - Byomími (Epigraph)
    - Desòchu
    - Hyonèku
    - Gemènyo
    - Chobìre
    - Jyotekábi
organizations:
  secondary:
    - Shimusògo
  referenced:
    - Tateshyúso
summary: >
  Rutejìmo is woken up by Chimípu when she found out that all of the adults had abandoned the teenagers in the middle of the night. Only Rutejìmo, Chimípu, Karawàbi, Tsubàyo, and Pidòhu were left behind. After some fighting, Chimípu ran off to look for the elders and left the others behind. Tsubàyo took charge and began to order Pidòhu and Rutejìmo to get breakfast and find food.

  The adults had taken the supplies except for a bag hanging from a tall cliff. Tsubàyo made Pidòhu climb the rock to get it. Rutejìmo wanted to help, but didn't have the courage to speak up. As Pidòhu struggled with the rocks, Karawàbi started to throw rocks at Pidòhu to Tsubàyo's amusement. Rutejìmo struggled with his emotions.

  One of the rocks hit Pidòhu and he fell, breaking his leg.
---

> The true nature of a woman cannot be found with simple questions or tests. It can only be found when her child is under the knife's edge or when she must choose between her husband and family. --- Yunujyoraze Byomími

"Wake up, Jìmo!" Chimípu's voice cut through the side of his tent.

Rutejìmo groaned as he cracked open his eyes. His head hurt, and his legs ached from yesterday's run. He wanted just a few more hours to sleep. "Go away!"

He rolled over and closed his eyes.

"Now!" snapped Chimípu. There was a strange tone to her voice---sharpness at the edge of panic. Rutejìmo had never heard her sound like that, and he felt a prickle of fear crawl down his spine.

He sat up sharply and looked around. Spears of sun bore down on the thin fabric of the tent, and the transient coolness from the night was quickly turning into a stifling heat. Not even a trickle of breeze ran through space between the flaps.

He peered out through the opening, but Chimípu was already striding away. Scratching his head, he pulled on a fresh pair of shorts and carefully rolled his dirty clothes into a tube before shoving them into his travel pack. Groaning, he dragged his pack out of his tent and stood up.

The sun was brighter than he expected. He shaded his eyes and peered up. Tachìra, the sun spirit, was a fist's height from the horizon. "They actually let us sleep in?"

"No," snapped Chimípu as she spun around, "they left us in the middle of the night!"

Rutejìmo froze, the prickling along his skin turning into the buzz of a thousand insects. The world spun around him as he stared at her, wondering if he misheard. "W-What?"

"You heard me, sand-blasted incontinent! I said they left us."

"No, Desòchu would never do that. He would never… " Rutejìmo peered around but he didn't see the other tents. "…leave… me…?"

There were only four tents still set up: Rutejìmo's, Pidòhu's, Tsubàyo's, and Karawàbi's. He didn't know where Chimípu's tent had gone, and he wondered where she had set it up. After a few seconds, he guessed that she'd already packed her tent before she woke him up.

The concern cut into him as he scanned the camping area around the three rocks. His brother's tent was gone. So were Gemènyo's and Hyonèku's and all the others. In fact, if it wasn't for the dead fire pit in the center of the three rocks, there would be no sign the clan had spent the night.

Rutejìmo turned back and asked, "Where is everyone?"

Chimípu let out an exasperated sigh. "Do you really think I know the answer to that?" She scanned the horizon, no doubt looking for signs of movement.

"Why not, Chimípu? You know everything else."

"That's because you're an idiot with sand for brains."

Tsubàyo groaned as he came up to them, scratching his ass. "So? They left you behind also, Mípu."

Chimípu glared at him and pointed her longest finger at him. "Listen, Chobìre's shit for a skull, I don't have time for your crap or posturing. I'm going to jog out to that dune," she pointed to a taller ridge of sand, "and see if I can find them. Don't do anything stupid."

Tsubàyo shrugged and scratched the scars on his face and throat.

Chimípu made an exasperated sound and started across the sand. Rutejìmo followed her with his eyes. He was stunned by how fast she had lost her calm and wondered if he shouldn't be more worried himself. Chimípu was always collected and smug. Rutejìmo wasn't sure if he liked the change.

"Damn the sands, it's late," grumbled Karawàbi as he staggered up. "What is going on?"

Rutejìmo kept his gaze locked on Chimípu as she accelerated into a run. The sand kicked up behind her as she disappeared behind a smaller dune.

Tsubàyo grunted and gestured to the empty site. "The clan ran off without us."

Karawàbi looked surprised and then turned around. For a long moment, he stared at the empty campsite before he turned back. "When did that happen?"

"Last night."

"Why didn't we hear them?"

Tsubàyo reached over and smacked Karawàbi on the shoulder. "Because I couldn't hear anything over your snoring, idiot."

"Oh." Karawàbi toed the ground for a moment. "I'm hungry."

Tsubàyo shook his head slowly before pointing to Rutejìmo's and Pidòhu's tent. "Then get Dòhu up and have him make us breakfast."

"Okay, Bàyo." Karawàbi turned on his heels and walked toward the tent. His feet scuffed against the sand.

"Jìmo…," started Tsubàyo.

Rutejìmo turned to focus on him. "Yes?"

"You going to obey the uptight bitch, or me?"

"What?" Rutejìmo shook his head in confusion.

Tsubàyo stepped forward and gestured in the direction of Chimípu. "That girl has rocks jammed up her ass. If the clan really abandoned us, she's going to insist on being in charge. She is always lording over us, and that won't change out here. You know that, right?"

There was no question Chimípu was already taking charge when she had woken Rutejìmo up. He let out a long sigh. "Yeah, she will."

"Do you really want to run in her shadow for the rest of your life?"

Rutejìmo shook his head.

Tsubàyo smiled triumphantly. "Good. I knew you'd see it my way."

Rutejìmo felt uncomfortable at Tsubàyo's declaration, but he said nothing.

"Now"---Tsubàyo clapped Rutejìmo on the shoulder---"I'm hungry. Dòhu! Where's Dòhu?"

Rutejìmo looked around, but didn't see Pidòhu.

Karawàbi had reached Pidòhu's tent and peered inside. With a chortle, he reached in and pulled Pidòhu from inside.

The frail teenager kicked and lashed out, both uselessly, as the much larger boy set him down on his feet.

Pidòhu shook his arm free from Karawàbi's grip. His eyes were sad but not surprised as he looked around at the empty site. Instead, there was despair in his expression.

Karawàbi grunted. "I'm hungry. Go on, make breakfast for us, boy." He stepped over to Pidòhu and shoved him to where the fire used to be.

The slender boy stumbled forward and dropped to his knees. The sand rolled away from him as he looked up at Rutejìmo, Tsubàyo, and Karawàbi standing over him.

"With what?" Pidòhu gestured to the empty site. "The only thing we have is the food in our packs."

"So?" grunted Karawàbi.

"Rations don't need to be cooked, Wàbi," Pidòhu gave Karawàbi an annoyed look.

"I don't care." Karawàbi balled his hands into fists, and the sound of cracking knuckles snapped through the air. "I'm hungry. Find something, Dòhu."

Pidòhu sighed and shook his head. "Give me a few minutes."

"Hurry up, runt."

Tsubàyo gestured to Karawàbi, and the larger boy headed that way. Tsubàyo cleared his throat and gestured for Rutejìmo to also follow.

Rutejìmo looked back and forth between Tsubàyo and Pidòhu. The frail boy was digging through his travel pack, pulling out a few kindling sticks and a sparker. The device was a flint wheel attached to a long coil of wire. When pulled, it would create a shower of sparks to start a fire. He padded over to the pit and brushed away the sand to get to the ash beneath. Heat rose in shimmers around his fingers as he jammed the kindling into the ashes and set the device next to it. Grabbing a wire on the top, he planted his hand and yanked it. A shower of sparks poured out but the kindling didn't catch. He rewound the wire and tried again.

"Jìmo," called Tsubàyo.

Rutejìmo glanced back to the two boys, but then Desòchu's words came back. He shook his head and took a cautious step toward Pidòhu. When Tsubàyo didn't say anything, he headed over and squatted next to Pidòhu; the similarity to Chimípu wasn't lost on him, and he tried not to think about it. "What can I do, Dòhu?"

Pidòhu looked surprised. "I-I---" He glanced over to Tsubàyo and then back to Rutejìmo. He pointed to the side of one of the towering rocks. "Over there, there should be a food bag hanging on a ledge. Assuming they didn't take it. Could you find it?"

Rutejìmo jogged to the rock and looked around. When he didn't see anything, he paced back and forth and scanned the ragged rocks with a sinking feeling. He continued to circle the rocks but his mind began to wander.

Desòchu must have known about this as the leader, but Rutejìmo's older brother would never leave them alone. It was dangerous out in the sun. No shelter, very little water, and after endless days of hard running, none of them had the energy to do anything.

Rutejìmo scanned the horizon, looking for Chimípu. She was just a black dot a few dunes over, running along one of the many ridges that crossed the desert. Chimípu had more energy than anyone else. She was a better runner and warrior. Jealousy rose up in him, and he wished he was as good as she.

"Damn the sands," he muttered, and gave up looking for the bag. "My own brother abandoned me."

He didn't want to go back to Pidòhu without the bag, or go near Tsubàyo. With a long sigh, he kicked the sand and headed for the farthest rock.

Rutejìmo stopped in the shade and slowly raised his gaze up the towering stone to focus on the sharp point sticking high in the air. It was easily over a chain in height, and in a happier time he would have climbed it in a heartbeat. But now, abandoned by his own clan, left to die in the cruel desert, he didn't know what to do.

"Damn my brother."

"Jìmo!" yelled Tsubàyo. "Where did you go!?"

Rutejìmo looked across the endless waves of the desert and shook his head. "Damn my brother," he repeated and headed back.

He made it only a rod before movement on the rock caught his attention. He stepped back and shielded his eyes to peer up the towering Wind's Tooth. It was the food bag, but it was halfway up the backside of the rock, about thirty feet off the sands.

"Jìmo," called Pidòhu as he came around the rock, "did you find it?"

Rutejìmo mutely pointed to the bag.

Pidòhu looked up, then did a double take. "How did it get up there?"

Rutejìmo shrugged helplessly.

"Can you get it, Jìmo?"

Nodding, Rutejìmo padded up and inspected the rock. It was rough with sharp edges. He didn't want to climb up the face, but both of them knew who was better at scaling rocks. He tried to calm his quickening breath. "Give me a second."

"Thank you, Jìmo."

"For what, Dòhu?"

"For helping."

Rutejìmo peeked over at the slender boy before turning his attention to the bag. He mapped out the rocks, trying to find a place to climb. "Did you know this was going to happen?"

A sharp intake of breath answered his question.

"Was it planned?" He felt a strange sense of euphoria and betrayal tightening his throat. His brother would never abandon him.

"I'm guessing yes."

"Why?"

Pidòhu turned to him. "Didn't you think it was strange we had so many adults in this group?"

"No…." Rutejìmo hated being reminded that Pidòhu saw something long before he did.

"It's a trial."

"I know, this is Chimípu's---"

"No, it's a rite of passage for all of us."

Rutejìmo froze, his eyes not seeing for a painful minute. "F-For all of us?"

"Yes, that's why there were so many adults. I didn't think they were going to just leave us here, but this entire trip was for more than just---"

"What," snapped Tsubàyo as he came around the rock, "is going on?"

Pidòhu clamped his mouth shut.

"Well?" growled Tsubàyo as he stopped in front of Pidòhu. Karawàbi strolled behind him, scratching his ass.

Rutejìmo cleared his throat. "Pidòhu thinks this is a rite of passage for all of us."

Tsubàyo scoffed and glared at Pidòhu. "You aren't a man and will never be one. They are letting the sands take us because none of them want to be kin-killers."

Pidòhu stepped back, holding his hand.

Rutejìmo said, "No, he thinks that is why they brought so many on the trip." But Rutejìmo still didn't believe that is why they were in such a large group. He thought about the unexpected member also with them---Jyotekábi, the strangely dressed woman from Tateshyúso clan. The Tateshyúso never left the valley.

"Oh," drawled Tsubàyo as he took a step toward Pidòhu, "you knew this was going to happen?"

Paling, Pidòhu held up both hands. "I-I didn't know, I just guessed."

Tsubàyo growled and took another step closer. Pidòhu tried to step back, but Tsubàyo followed him. "You knew they were going to abandon us? And you didn't think it was important to say anything? Are you working for them? A spy?" As he spoke, his voice grew louder and angrier.

"No, Tsubàyo, I didn't know. It was just an idea. I would never---"

Tsubàyo struck Pidòhu with his open palm, the crack of flesh on flesh echoed against the rocks. Rutejìmo jerked violently at the sound. Pidòhu spun once before he crumpled to the ground.

"I'm your better, boy," screamed Tsubàyo, "and don't you forget it! You call me great or don't speak!"

Pidòhu whimpered as he held his face with his hand.

Tsubàyo kicked sand on him. A few rocks bounced against Pidòhu's face before landing on the ground with little dull thumps. "Now, make my sand-damned breakfast."

Uncomfortable, Rutejìmo turned to the rock and steeled himself for climbing.

"No, Jìmo, let Dòhu do that."

Rutejìmo stopped at Tsubàyo's command. He turned slowly back to the scarred boy.

Tsubàyo gestured angrily at Pidòhu, who was getting to his feet. "He probably put it there just to see someone fall. Let him climb up."

"But---"

"I said," growled Tsubàyo, "let him"---he stepped toward Rutejìmo---"climb up there and get his own damn bag."

"Dòhu doesn't climb that well."

"Like shit he doesn't. I bet it was just one more lie, right?"

"No," whispered Pidòhu, "I---"

"Climb the damn rock!"

Rutejìmo and Pidòhu jerked at Tsubàyo's shout.

Tears glittering in his eyes, Pidòhu slowly made his way to the rock. He stared up at it with trepidation, and Rutejìmo could see him trembling.

Rutejìmo stepped forward, but hesitated when Tsubàyo let out a hiss. He wanted to help, but couldn't.

"Let him climb, Jìmo."

Rutejìmo pressed forward.

"I said---"

"I'm giving him my gloves!" he snapped. As soon as he said it, he was shamed he didn't say what was on his mind, that he wanted to do it for Pidòhu. Committed to his new lie, he peeled off his gloves and handed them to Pidòhu. He whispered to the slender boy. "I'm sorry."

Pidòhu nodded. "Thank you, Great Shimusogo Rutejìmo."

Rutejìmo turned away with a flush of embarrassment. He didn't deserve being called "great" anything; that was reserved for those who deserved respect. He walked from his own failure, but only a few rods before turning back to watch.

Pidòhu slowly picked his way up. He strained to reach the rocks, and when he pulled his trembling frame up, he grunted with exertion. He struggled to remain in place as he reached up for the next handhold.

"Move, boy," said Tsubàyo. "I'm hungry."

"I'm trying."

"Try harder."

Rutejìmo looked away but listened to Pidòhu climbing the rock. It took long, painful minutes, and Rutejìmo was reminded he could have done it in half the time. Even the idea of looking at the ground thirty feet below scared him, but he couldn't imagine what Pidòhu was thinking.

"Finally!"

Rutejìmo glanced up to see Pidòhu easing the bag off the ledge. Surprised, Rutejìmo let out his held breath before he realized he was holding it.

"Hurry up, boy," snapped Tsubàyo.

To climb down, Pidòhu worked blindly as he felt for the toeholds and tested each one before putting his weight on it. Rutejìmo could see him shaking with the effort to keep from falling. The bag swayed back and forth with every movement, threatening to fall.

A stone bounced off the rock next to Pidòhu. It clattered down the Tooth before hitting the ground with a thud.

Rutejìmo gasped and looked to where Karawàbi was hefting another rock with a grin on his face. "Wàbi!"

Karawàbi shot Rutejìmo a glare and flicked a larger rock. It bounced off a few inches from Pidòhu's hand. Pidòhu gasped and flinched away a moment later. He would have been too late to avoid being struck, if it had hit him. His lower foot scraped against the stone as he renewed his attempts to find a foothold.

Rutejìmo stormed forward. "Stop that, Karawàbi!"

"What?" Karawàbi threw another rock, and it hit the stone near Pidòhu's feet. "I'm not going to hit him."

"He's twenty feet off the ground!"

Another rock. This one struck the stone near Pidòhu's shoulder, and flecks of sand poured down. "The sand is soft. He won't get hurt."

Rutejìmo halted in front of Karawàbi. He was several inches shorter than the larger boy and couldn't tower over him the way he wanted to. "He is clan."

"Not for long," chuckled Karawàbi. He drew back and threw the rock. It whistled past Rutejìmo's ear.

Rutejìmo balled his fist to strike Karawàbi, but then he saw a grin stretch across Karawàbi's face. Rutejìmo spun around with a feeling of dread clenching around his heart.

Pidòhu was clawing at the rocks with one hand as he fell back. One foot was flailing in the air, and the rapid thumping of his toes striking the stone was a drumbeat that matched Rutejìmo's own suddenly quickened pulse. The sound of fingernails scratching against stone filled the air as Pidòhu's weight snatched him from the stone cliff.

At first, he moved with sickening slowness as he fell back, but he quickly accelerated as he peeled off the stone wall and plummeted to the sand below. Over the thump of his body hitting the ground, Rutejìmo heard the sound of cracking bone, and it sent a pang of fear through him.
