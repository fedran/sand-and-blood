---
when:
  start: 1471/3/44 MTR 1::22
date: 2012-08-23
title: Standing Alone
locations:
  primary:
    - Nisanto Finger Stop
  referenced:
    - Three Falls Teeth
characters:
  primary:
    - Rutejìmo
  secondary:
    - Karawàbi
    - Tsubàyo
  referenced:
    - Gutèmo (Epigraph)
    - Chimípu
    - Pidòhu
organizations:
  secondary:
    - Shimusògo
    - Metokāchyu # Scorpion builders
topics:
  secondary:
    - togomakēnyu
summary: >
  At the end of the day, they reached a rock shelter for the night. Neither Tsubàyo or Karawàbi had any intend in helping set up and they ordered Rutejìmo to serve them. It was the same behavior that the two had treated Pidòhu before they left.
---

> Time alone gives a man a chance to consider the mistakes he made. --- Heyojyunashi Gutèmo

They came to a gasping halt at a pile of rocks. The stones didn't tower over them like the Wind's Teeth, but the outcropping gave some shelter against the wind kicking up sand from the west. It was near the end of the day, and the shadows formed by the setting sun sent long fingers of shade across the red-tinted sands.

Rutejìmo leaned against the rock and panted for air. Sweat trickled down his entire body and his legs trembled, but elation burned brightly in his veins. He had kept up. For the first time since they started this accursed trip, Rutejìmo managed to run with the rest of the clan. Even if the clan was only three teenage boys. He wondered if, somehow, the others had slowed down for him. He glanced at Tsubàyo with a silent question.

The teenager scratched the scars on his face and caught the look. "What?"

"Bàyo, did you---" He cleared his throat and inhaled sharply before he said, "Were you running slower?"

Tsubàyo shook his head. "No, why would I do that?"

"Because… I…." He felt embarrassed talking about it. Rutejìmo stood up and shook his head. "Don't worry about it, I was thinking about something." He turned and headed around the far side of the rocks.

"Jìmo! Don't go too far. You'll need to get a fire started for dinner."

Rutejìmo tensed with annoyance. He had just spent the entire day running with the others. Despite chasing the dépa and feeling more energetic earlier, running was still exhausting and he felt the ache seeping into his limbs.

When he went with Tsubàyo, he never expected to be the one serving the others. It felt demeaning when Tsubàyo should have been glad that Rutejìmo had gone with him instead of staying with Chimípu.

Rutejìmo glanced at Karawàbi, but there was no compassion from the other teenager. The larger boy tossed down the bags and pointed to them. "Don't forget these too. Put my tent by the rocks."

Setting his jaw, Rutejìmo stalked around the rocks to get away from both of them. He continued to walk until he could no longer hear Tsubàyo or Karawàbi. Taking a deep breath, he tried to calm down and fight the sick regret building in his stomach.

The view of the desert was both familiar and strange at the same time. The sun was an angry red blob on the horizon, the edges wavering with the heat that rose up from the sunbaked sands. Dunes and patches of rocks spread out as far as he could see until they disappeared into the haze of sand and wind. It was the same thing he saw every time he went to the edge of the clan's valley. But the swells of sand and the piled rocks were in the wrong places. It wasn't home and it brought a pang of homesickness.

His mind drifted to the run. The dépa was real, but neither Tsubàyo nor Karawàbi had reacted to it when it ran between their legs. He wanted to ask if either of them had seen it, but he didn't want to be mocked if they were ignoring it or if they thought he was seeing things.

Chimípu wouldn't have been much better. She always acted superior to him, both in her attitude and the casual way she excelled at everything. He knew both would humiliate him, but he didn't know if her actions would cut as deeply as Tsubàyo's words. In less than a day, he was questioning his decision.

To his surprise, Rutejìmo worried about Pidòhu. When Pidòhu had quietly explained how to bandage his own broken leg, Rutejìmo was surprised at his strength of will. If Rutejìmo were in his place, he would have been screaming and sobbing far more than Pidòhu's quiet cries.

There was more to Pidòhu than Rutejìmo even imagined, but there would never be a chance to find out more about him. Looking back, Rutejìmo realized he had abandoned Chimípu and Pidòhu just like the rest of the clan did. His thoughts grew dark as he recalled the events of the day, seeing the endless places where he had made a mistake. But, despite all of the rest, he would have never considered that his own brother would have abandoned him.

He sighed and let his head swing back. It struck the rock with a little burst of pain, but it was nothing compared to the ache in his legs. "Why couldn't this be easy?"

No answer came except for the wind sending grains of sand bouncing against his skin. He breathed in the dusty, arid air. A moment later, he caught the scent of wood smoke. Frowning, he lifted his head and looked back to where Tsubàyo and Karawàbi were. There was no smoke, and none of them carried wood to burn; it was too heavy. The Shimusògo used travel fires, tins filled with an alchemical substance that was far lighter and less precious than wood.

Turning in a slow circle, he scanned the horizon for the source of the smoke. He turned around twice before he gave it up as a product of his imagination. But, as he was coming back around to leave, he spotted a flash of light in the distance. It came from one of the long shadows formed by distant mountains and the setting sun. He focused on it, watching as it flickered. A moment later, he identified it: it was a campfire.

The clans of the desert were rarely friendly. When they met in the middle of the sands it frequently began with tense words, and if someone wasn't careful it ended in bloodshed and violence. Only efforts on both sides could allow such an encounter to end peaceably.

Rutejìmo turned away from the fires and let his thoughts return to his self-doubt and confusion. He slid down to the ground and watched the last of the sun dip behind the horizon.

"Jìmo, start the fire." Tsubàyo's command preceded the teenager as he came around the curve of the outcropping. He tossed the brass container with the alchemical fire at Rutejìmo's feet.

Rutejìmo glanced at the distant lights. He had counted at least six fires since he first noticed them. And if he could see them, then they could see any fire he created. He shook his head. "I-I don't think we should."

Tsubàyo stepped closer, towering over him. "Call me Great Shimusogo Tsubàyo. I'm in charge here, boy."

Rutejìmo dropped his far hand behind his thigh as he clenched it into a fist. He wanted to lash out at Tsubàyo, to teach him a lesson, but they were all tired, and he had no doubt that Karawàbi would join in just to beat Rutejìmo into submission.

His chest muscles spasmed with stress and sullen anger. "Great Shimusogo Tsubàyo. But, what about those fires?" He felt sick to his stomach as he pointed across the sands to the other clan. "Do we really want to get their attention?"

Tsubàyo frowned and peered across the darkening world. For a long moment, he said nothing, but then he sighed. "Fine, get the salted meat out for us. I'm hungry." With a kick that caught Rutejìmo in the hip, Tsubàyo spun around and marched back around the rock.

Groaning, Rutejìmo grabbed the brass box and staggered to his feet. Giving the other camp one last look, he followed after Tsubàyo.

While Rutejìmo was on the other side of the rocks, neither Tsubàyo nor Karawàbi had set up the tents or brought out the food. Instead, the four packs were piled in mute testimony that spoke volumes. Feeling his muscles growing tight with suppressed anger, Rutejìmo knelt down at the packs and began to dig into them.

He started with Pidòhu's bag, partially out of curiosity. It was a mess after Tsubàyo pawed through it. He pushed aside the clothes and felt around the bottom. A carved rock bounced on his fingers. Glancing up to ensure neither of the others were looking, he chased it out of the bag and into his palm.

It was a rock of a color that Rutejìmo had never seen around the valley. On one side, it was polished to a mirror smoothness. On the other, Pidòhu had somehow mounted a metal gear. Rutejìmo ran his finger along the bottom and felt the faint ridge where stone stopped and metal started.

He was about to toss it away when he saw three others in the bottom of the bag. With a start, he realized what they were: voting stones. As teenagers, neither Pidòhu nor Rutejìmo could vote in the clan affairs, but that didn't stop them from gathering the rocks they planned to first toss into a bowl. He had no doubt that Tsubàyo and Karawàbi had rocks of their own, but it was considered bad luck to reveal them before they were acknowledged as an adult.

Rutejìmo felt as though he was prying into Pidòhu's private life. He gathered up the remaining stones from the bag. If the others saw them, they would no doubt toss them into a fire or desecrate them in some way. He opened his own pack and set them in with his own, secreted in a small pouch at the bottom.

He might never see Pidòhu again, but at least his legacy would remain untouched by their cruel jokes.

A blush on his cheeks, Rutejìmo continued to work his way through Pidòhu's pack. He found water and supplies, which he set aside. He also found a thin, cloth-bound book, but a quick glance showed lines of Miwāfu script that Rutejìmo couldn't read.

"What's that?" grunted Karawàbi. The larger boy strode over and snatched the book from Rutejìmo's hand.

Rutejìmo reached out for it, but stopped when Karawàbi glared at him.

"Give that to me, Wàbi," Tsubàyo said. When Karawàbi obediently handed it over, Tsubàyo opened it and read a few pages. A slow smile crossed his lips. "Listen to this: the day sun warming, dancing off the sky, no more crying now."

Karawàbi rolled his eyes. "Poetry? Why is he bothering?"

Tsubàyo flipped through a few more pages. "Mostly togomakēnyu it looks like. Poetry for a dead boy."

Rutejìmo held out his hand. "Could I have it?"

"Give it to me, Bàyo." Karawàbi stepped closer.

Tsubàyo shot a cruel smile at Rutejìmo and handed the book over to Karawàbi. The larger teenager took it and headed around the rock. A few seconds later, the sound of urine splashing on the pages filled the air.

Rutejìmo lowered his eyes, glad he had hidden Pidòhu's stones from their attention. He finished digging into the packets and found the salted meat. Sitting down on the ground, he opened up the brass box with the alchemical mixture. He dipped his finger in the dark gel and brought out a glob. Carefully, he smeared a thin layer over the hunks of meat before closing the box. Wiping his hands on the sand at his feet, he set the meat on the rock and took a deep breath. Pursing his lips, he breathed hard on it. The gel ignited under the combination of humidity and moving air. A heartbeat later, a translucent flame enveloped the meat. He added dried vegetables, some powdered fruit, and a bit of seasoning to each before delivering plates to the others.

All three ate in silence.

Rutejìmo remained with his back to the others, not wanting to attract their attention, and wondered, once again, if he had made a mistake by following Tsubàyo.
