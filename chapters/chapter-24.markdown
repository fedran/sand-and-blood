---
when:
  start: 1471/4/3 MTR 5::62
date: 2012-09-19
title: Alone in the Dark
locations:
  primary:
    - "Oijogushi's Abandoned Valley"
characters:
  primary:
    - Rutejìmo
  secondary:
    - Mikáryo
    - Chobìre
  referenced:
    - Kapōra (Epigraph)
    - Tsubàyo
    - Chimípu
    - Karawàbi
    - Tachìra
    - Mifúno
    - Pidòhu
    - Desòchu
    - Ryachuikùo # Horse
organizations:
  secondary:
    - Shimusògo
    - Pabinkúe
summary: >
  Rutejìmo sat alone in the dark, staring at a small fire. He was afraid of the dark. He also mulled over various struggles in his head.

  As he was thinking, Mikáryo showed up. She brought meat and roasted it over Rutejìmo's flame. They talked about Chimípu abandoning Rutejìmo in the dark and the dangers of being alone.

  Mikáryo also talked about Tsubàyo. He was in the process of becoming a Pabinkúe. She no longer was content to have one of their lives, she would only accept taking Tsubàyo back into her clan.

  When it gets late, Mikáryo tells Rutejìmo to go to sleep. He resisted, but she promised he would wake up in the morning.
---

> The clans of day and the clans of night will only talk over naked blades. --- Chidomifu Kapōra

Rutejìmo sat alone in the dark.

Only an hour ago, the sun had dipped below the horizon, but it felt like years. He stared at the alchemical flame in the center of the ruined campsite. The normally pale light was painfully bright in the inky darkness of the desert.

Remembering how Tsubàyo stepped out of the night, Rutejìmo had spread out the glow eggs in a large circle. Each one was a tiny pool of flickering blue light, a weak illumination but he hoped enough to give him a second's warning if Tsubàyo came for him.

He couldn't sleep. He didn't realize how much he depended on having the other two near him. Knowing that Chimípu watched over him had given him peace of mind, or at least quelled the fears enough for him to sleep. Alone once again, he didn't dare crawl into either of the remaining tents for fear that he would never wake up. He kept seeing Karawàbi's corpse.

Sighing, he dug into the meat pouch and snapped off a wing. Fitting it on a stick, he set it over the fire and waited for it to warm up. The alchemical mixture inside the pouch had cooked the meat and killed any parasites, but gnawing on cold meat didn't appeal to his clenching stomach. Soon, the meat was warm enough to eat, and he plucked it from the stick.

He glanced up at the night sky, wondering if he should try to name the stars again. To the east, he could see the faintest sliver of the moon. He shivered in fear. Just as Tachìra was the sun, Chobìre was the spirit of the moon.

Legends said that the two spirits used to be best friends, but then they both fell in love with Mifúno, the desert spirit and mother of the world. It quickly became a violent rivalry, and the two clans, clans of day and clans of night, were drawn into the conflict. He shuddered, remembering the horror stories his grandmother had told him about Chobìre and the clans who gained power from the dark spirit.

Tearing his thoughts away, he chewed on his dinner. The meat was heavily spiced, and the travel pouch gave it a metallic taste. He had to force it down his dry throat.

"Sands," he muttered, "I never thought I'd be wishing for salted meat." He chuckled again. "I'm pathetic."

His brother's stories about the horrid alchemical meals hit him, and he laughed. Now he could understand the longing Desòchu had when he relished the meals cooking in the valley. He missed the variety of flavors and the taste of woodsmoke in the food.

The alchemical flame sizzled as grease dripped into it. The smell of roasted meat drifted across his senses, and he breathed in the smoke. He smiled and leaned back, enjoying the memories.

And then Rutejìmo realized the smoke came from burning wood.

"You aren't going to pee again, are you?" asked Mikáryo in a soft voice.

Rutejìmo lowered his head to look over the flames. She was on the far side of the fire, and he shook at the sight of her.

She smirked as she reached over and picked up her tazágu from where it was braced between her foot and a rock. The pointed weapon had a hunk of dripping meat jammed on the top of it. Juices ran down the short spike and gathered at a circular guard above the grip. The liquid dripped from a small hook on the edge of the guard and hit the ground with faint splats.

"P-Please don't kill me."

Mikáryo looked up with a humorless smile. "Shimusogo Rute… jìbo, right?" He could barely see the white of her eyes. Her pupils were unnaturally large.

"Rutejìmo."

"Well, Jìmo"---she leaned back and pulled off the meat with her teeth---"if I wanted to kill you, I probably would have stabbed you in the throat two nights ago. And if, for some reason, I thought you were suddenly a threat, I probably would have Bàpo crush your skull in while you were staring at me."

Rutejìmo felt something looming over him. Mikáryo's horse stepped over his head. Rutejìmo didn't even feel a whisper of hair or breeze of movement as the horse continued past him, around the fire, and then settled on the ground next to Mikáryo.

She didn't look up as she held out an unfamiliar, red fruit for him.

The horse took it and chewed noisily. Rutejìmo couldn't tell if it was staring at him or something else. Like his mistress, his eyes only had the faintest of white rings around the large, black pupils.

Mikáryo leaned over to an open bag and speared more meat on the end of her weapon. She held the weapon against the rock with her boot. She pulled out another fruit, sliced it in quarters, and shoved the four parts on the point.

Rutejìmo found the courage to speak, surprised that he could find words while his body shook in fear. "What do you want, Great Pabinkue Mikáryo?"

She smiled at him. "Your little warrior girl is an idiot, isn't she?"

"Chimípu?"

Mikáryo nodded and said, "I remembered her name, but she doesn't deserve it right now. No matter how fast she runs, she'll never catch a Pabinkúe at night. She was a fool to leave you alone."

"She was---"

The woman held up her hand. "Listen, Jìmo."

Rutejìmo was uncomfortable with her using the familiar form of his name, but he was too afraid to say anything.

"I don't know what they teach the clans up here, but the night is a very dangerous place to be alone. There are things out there that hunt lone fools."

"I-I wasn't sleeping."

"You were going to stay up all night, jumping at every noise?"

"Um…."

Mikáryo shook her head. "You are young"---she grinned suddenly---"and very stupid. But I expect that of you. You're pathetic. But your little warrior girl, I'd be tempted to teach her a lesson."

He inhaled sharply.

"Not you, Jìmo. I have something more important to do than following children barely out of their diapers around the desert."

"What?"

"Your friend, Tsubàyo. He's riding Pabinkue Ryachuikùo."

"The horse?"

"Were you dropped on your head? Of course, the horse."

Rutejìmo flushed hotly. He wanted to stand up and fight, but he knew he would die.

Mikáryo watched him, a slight smile on her lips.

Finally, Rutejìmo regained control of his temper. "Yes, he is riding, Great Pabinkue Mikáryo."

"I thought so. I can smell him in the wind, and he is moving too fast to be on foot."

"The Shimusògo are---"

"He's not one of your clan, is he?"

Rutejìmo hesitated, but then shook his head.

"So, tell me, Jìmo. Can he disappear into shadows?"

He gasped, then nodded.

"Did you ever see him disappear in one shadow and then not be there when you," she snorted, "excuse me, your warrior girl went after him?"

Another nod.

"Only one horse though, right?"

"Yes, Great Pabinkue Mikáryo."

"Call me Káryo, Jìmo." It was her familiar name, only used with children and close friends.

He shook his head. "No, I-I couldn't."

"Suit yourself. I don't enjoy calling anyone great. Kissing ass is for the villages and cities, not for travelers in the dark. The fight between night and day doesn't mean anything out here, does it? Not when Mifúno can keep secrets…." Her eyes glittered as she favored him with a predatory smile.

Rutejìmo tensed up, feeling like prey. There was something in her green eyes that forced him to stare into them. It was a sultry, smoldering look that brought a heat through his body. It sank down into his groin, and sudden thoughts blossomed in his head. He blushed hotly and turned away to hide his expression.

Mikáryo chuckled and relaxed. "Don't worry, boy, you have a long way to go before I consider riding you."

Rutejìmo whimpered and clamped his jaw shut as he tried to tear his thoughts away from the sudden ideas she gave him.

She laughed, a booming noise that was out of place for her slender body. "You are too easy, Jìmo. You also need to relax a lot more when you grow up. Otherwise, you'll rot from the inside."

He stared at the ground, unable to answer or look back.

Mikáryo said nothing, and he listened to her eating. His stomach rumbled at the smells.

She broke the silence after a few minutes. "He's a Pabinkúe, you realize."

"Tsubàyo?" Rutejìmo turned back to her, but kept his gaze averted.

She grunted in agreement. "He rides one of the horses and he can step through shadows. There is no doubt he is riding the spirit of my clan."

He finally looked up at her. "Then why don't you just take him? Then he would leave us alone."

Another smile. "The same reason your clan is watching from a distance. I want to see his measure. I want to know what type of man he's becoming."

"And if he isn't the right"---he gulped---"type of man?"

She lifted up her weapon. "You are at that wonderful crux of being a man and a boy. No one ever says you have to live through your rites."

A strange thought drifted through Rutejìmo's mind. He imagined Karawàbi's death, but it was his brother cutting the teenager's throat. He shook his head to clear it. "No, he couldn't," he whispered before he raised his voice for her. "Mikáryo? Did you kill Karawàbi?"

"Karawàbi? I don't know him, but I haven't killed anyone in months."

The image came up again, and Rutejìmo force it out of his mind. Desòchu would never kill another member of the clan.

Mikáryo added more meat to the end of her tazágu and set it over the fading flames.

Rutejìmo yawned, the exhaustion dragging him down. He blinked and tried to stay awake.

When the meat was done, she picked up her weapon and held it out to him, point first. "Here."

Rutejìmo stared at her in surprise.

"Take it. A boy like you needs food. Maybe when you grow up, you won't be so helpless."

"T-Thank you, Great…." he trailed off at her glare. "Thank you, Káryo." He slipped the meat off, the heat searing his fingers.

She gave an approving nod.

Rutejìmo nibbled on his meal. It was juicy and rich in flavor, but he couldn't place the type. It tasted like roasted snake. In a few seconds, he was licking his fingers clean. "Thank you," he said in a quiet voice.

Mikáryo chuckled, her gaze never leaving him. She had a strange smile on her lips.

"What?"

"Oh," she said, "when you aren't peeing yourself, you are almost adorably pathetic."

Rutejìmo blushed. "I don't mean to."

"I noticed. I've watched you trying to be brave for a while now. The boy I thought I saw earlier would have been cowering in a tent or against the rocks. But you are pretending to be a warrior, standing guard over yourself."

"Um…."

"That's a compliment, Jìmo."

"Oh. Thank you, Káryo."

"Good boy." She smiled again and speared a piece of fruit with her weapon. She lifted it to Rutejìmo, who took it. It had a sweet taste with a hint of spices.

The horse stood up, took a few steps, and sank back down behind Mikáryo. She leaned back against his side and crossed her arms. "Go to sleep, Jìmo. We'll watch over you."

"Why?"

"Because you can't guard yourself while you sleep."

"No, why… why didn't you kill me? Why are you helping?"

Mikáryo smiled and reached up to stroke her horse's mane. "Because, a long time ago, I was also young and stupid. I thought I could take on the sun and hold back the light. I couldn't and I failed." A sad smile crossed her face. "If it wasn't for a kind soul, I would be just bones on the rocks. I'm just returning the favor, Jìmo. Nothing more."

She pointed to the standing tents. "So sleep. You'll wake up safe in the morning. I promise."
