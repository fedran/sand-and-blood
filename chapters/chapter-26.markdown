---
when:
  start: 1471/4/3 MTR 18::74
date: 2012-09-22
title: Preparing for Battle
locations:
  primary:
    - Pabinkue's Lost Tears
characters:
  primary:
    - Rutejìmo
  secondary:
    - Chimípu
    - Pidòh
    - Tsubàyo
    - Shimusògo
  referenced:
    - Tateshyúso
    - Mikáryo
organizations:
  secondary:
    - Shimusògo
    - Pabinkúe
  referenced:
    - Kormar (Epigraph)
summary: >
  They came up to where Tsubàyo had taken Pidòhu, a large arch. Chimípu was prepared to take Tsubàyo by herself, but Rutejìmo insisted on helping. After a short discussion, including Chimípu revealing that she intensely disliked Mikáryo, she agreed to let him help.

  They plan their attack.
---

> Death should never be planned lightly. --- Kormar Proverb

Without having to drag Pidòhu, Rutejìmo ran as fast as he could. He stopped feeling the blisters on his hands and the ache in his back. His fears and self-doubts disappeared under the comforting speed and closeness to the clan spirit. There was nothing but him, Chimípu, and Shimusògo.

Even though he didn't have Tateshyúso's shadow over him, he felt neither the oppressive heat nor the sand against his feet. The heat was there, he knew it, but it didn't sap his strength or steal his breath. His feet never slipped on sand or rocks.

Ahead of him, Chimípu ran in silence. He knew she could go faster, but she was pacing him as she ran along with Shimusògo. No doubt, Mikáryo's lessons were burning on her mind. He felt relieved that she wouldn't leave him, but also felt guilty that she couldn't run as fast as her powers would take her.

Before he knew it, they were slowing. He saw no sign or signal, but the wind no longer ripped along his skin, and the dépa grew hazy with every step. As he shifted from a run into a jog, the spirit disappeared from sight.

Chimípu jogged for a few chains before coming to a stop. She was breathing lightly, and her limbs glistened with a thin sheen of sweat. A triangle of sweat also soaked the collar of her shirt. As she strode up the slope of a dune, she had a set to her jaw that frightened Rutejìmo.

She reached the top and crouched down. Her knee scrunched on the sand. Rutejìmo joined her, kneeling as they peered over the dune.

A quarter mile away was the stone arch Mikáryo had described. It was on the threshold of a mountainous region, with sharp-edged cliffs and ragged rocks. The arch was on a rocky hill. The nearest side of the hill was sandy but behind it was a gentle slope leading up to the cliffs. As the wind blew, it kicked sand into eddies that disappeared beneath the shadows of the outcropping.

The arch was larger than he imagined, stretching a few hundred feet across the sands and reaching about a hundred feet at its peak. Both ends were steeply sloped to the ground; he would have been hard-pressed to climb either end. In the shadow of the arch, he saw the silhouettes of more rocks. He had no doubt they would be sharp and jagged.

Rutejìmo whispered to Chimípu, "Do you know where Dòhu is?"

She nodded silently. Her finger was rock-steady as she pointed to one end of the arch.

He followed her gesture, frowning as he focused into the shadows. He spotted Pidòhu after a few seconds. The injured boy was curled up in the shadows, clutching his leg and rocking back and forth. Rutejìmo thought he saw a puddle underneath Pidòhu and he felt sick to his stomach.

"He's hurt," Chimípu whispered back, "and bleeding."

"Where is Bàyo?"

She gestured to the rocks underneath the arch. "Hiding and waiting. It's a trap. He knows we're coming."

"What do we do?"

"He hurt Dòhu. I'll kill him." She lifted her attention to the sky. "But I need to get started. There is only about thirty minutes of sunlight left, then he gains power as I lose it." She stood up.

Rutejìmo grabbed her arm to stop her. At her glare, he shook his head. "No, not you, we. We need to stop him."

She grunted and knelt back down. "Jìmo, I have to do this. It's my"---she gulped---"duty."

"Your duty to kill… stop Tsubàyo. But that doesn't mean I can't help."

She glanced at the stone arch, then back at Rutejìmo.

"Come on, Mípu. Dòhu is our friend, and after everything, I can't give up on him now."

A smile quirked her lips. "Grew some balls last night, didn't you?"

Rutejìmo blushed.

"With the night bitch? Did you do anything else?" Her voice was sharp and probing.

He thought of Mikáryo crouching over the flames, nearly naked. He shook his head sharply even as the blush grew hotter. "No."

She stared at him for a long moment, then sighed. "Good. Stay away from her. She's evil."

Desperate to change the conversation, Rutejìmo pointed to the rocks. "So let me help."

"Okay, but promise me something. If you have a chance, you grab Pidòhu and run. Just run. Don't turn around, don't try to look for me." She leaned next to him, her breath warm on his face. "All that matters is saving Pidòhu and you."

His heart thumped loudly. Gulping, he nodded.

"I'll"---she glanced toward the arch---"catch up as soon as I can."

He said nothing as he watched the storm of expression across her face. There was doubt and anger, but also fear. He could tell she was still struggling with her duties as the clan warrior, a fighter, to protect him. But the fear was something else. Guardians died protecting the couriers. She was already his guardian, and there was a very real possibility she would die that day.

There was nothing he could say. She was on her own path, and he was on his own. He rested his hand on his pocket, feeling the fang sticking through the fabric.

She would always be better than he. The realization hurt, but he finally needed to acknowledge it. He closed his eyes and sighed. "Tell me what to do, Great Shimusogo Chimípu."

It had been easier to bare his throat to her knife.

Chimípu's eyes widened with surprise. Then she gave a single nod without looking at him. She hesitated for a moment, then pointed to the arch. She spoke in a tense voice, her body trembling with her emotions. "When we were practicing firing rocks at bird's nests, you weren't very accurate beyond two chains. You can run a chain in about two seconds from a dead sprint. That means you need to stay about a chain or so away and fire rocks at Tsubàyo. When you have a chance, grab Dòhu and run. You'll only have thirty, maybe forty seconds."

"That isn't a lot of time."

"He won't give you much. He can come out of the shadows in a charge. He doesn't need to accelerate, which is why he put Pidòhu there."

"You want me to wait here?"

"No, he'll figure it out if you just stand there waiting." She turned and dumped her pack on the ground. "We'll start by using the packs for slingshots. Pull everything apart and make as many shots as you can."

Rutejìmo nodded and dropped next to her. He emptied his bag out on the sands before sorting everything into four piles of equal weight and size. Once finished, he turned his attention to his bag. Using his cooking knife, he tore out long strips of canvas and wrapped the fabric around each pile to create four impromptu projectiles.

Chimípu added six shots from her own bag to the pile. She raised on her knees and shrugged out of her shirt. Underneath, the white band around her chest was stained with dried blood and grime. She tore her shirt in half and braided it together into a sling that could take the strain of firing their improvised shots.

Her muscular body was slick with sweat, and her skin was dark. Rutejìmo peeked at her, but he didn't feel the same attraction that he had for Mikáryo. Tearing his attention away, he pulled off his own shirt and formed a second sling.

When they finished, he leaned back on his heels. Ten shots from the packs and two slings didn't look like a lot. He wasn't sure if he could use them, but he felt determination rising inside him. He had to help. Even if he missed, he would be a distraction.

"Mípu?"

"Yes?" she asked as she looked back over the dune.

"Are you going to kill him?"

Her jaw tightened. "Yes." She toyed with the tazágu, tracing along the spike where a name would be. She glanced at him. "Do you…?"

He shook his head. "No. That's your choice, Great Shimusogo Chimípu."

A muscle twitched in her cheek. She finally looked at him, tears shimmering in her eyes. "But I don't know if I can. I keep thinking about it. He kidnapped Dòhu and he tried to kill me. But, when I imagine stabbing him"---her hand tightened on the hilt of her weapon---"I feel sick."

He reached out and rested his hand on her shoulder. The muscles underneath bunched up for a moment, then she rested her palm over his wrist.

Neither said anything for a long moment.

She broke the silence as she stood up, letting his hand slip from between her palm and shoulder.

He pulled back and stared up at her, frightened but determined to do his part.

"Come on, Jìmo. It is time to fight."

"Yes, Great Shimusogo Chimípu."

She gestured to the ten shots at their feet. "And don't miss."
