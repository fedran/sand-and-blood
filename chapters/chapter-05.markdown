---
when:
  start: 1471/3/28 MTR 20::66
duration: 3 m
date: 2012-04-17
title: Decisions
locations:
  primary:
    - Shimusogo Valley
  referenced:
    - Wamifuko City
characters:
  primary:
    - Rutejìmo
  secondary:
    - Chimípu
    - Desòchu
    - Gemènyo
    - Somiryòki
    - Tejíko
    - Yutsupazéso
  referenced:
    - Goryápe (Epigraph)
    - Tsubàyo
    - Karawàbi
    - Mapábyo
    - Mifuníko
    - Shimusògo
    - Pidòhu
organizations:
  secondary:
    - Shimusògo
topics:
  referenced:
    - The Snake Killer's Betrayal (Epigraph)
summary: >
  After dinner, Rutejìmo found out that all the adults were gathering in the shrine to make a decision. He decided to use that time for revenge on Karawàbi and Tsubàyo. But, as he went to leave, Gemènyo caught him and told him that it was important that he didn't try. Something serious was about to happen. And then he ran to the shrine.

  Rutejìmo decided to eavesdrop on the shrine event. He crawled on top and listened at the opening. Inside, they announced that Chimípu's mother had died and they were sending Chimípu on her rite of passage to accept her as an elder of the clan. As they spoke, Rutejìmo realized that she was kneeling next to him; he didn't hear her coming.

  After everyone agreed on Chimípu, the clan elder asked about the other teenagers. As the votes came in, it was clear that Tsubàyo, Karawàbi, and Pidòhu would be included. The last vote was Rutejìmo, who almost didn't get accepted except for his grandfather insisting that he was ready.
---

> The weight of opinions is measured in years, not from the freedom of a womb but those earned as a true member of the clan. --- Basamiku Goryápe, *The Snake Killer's Betrayal* (Scene 19)

Rutejìmo didn't plan on obeying his brother. During dinner, he listened to Desòchu's tale about his trip to Wamifuko City with only half an ear. He was occupied with how to sneak out of the family cave, find Tsubàyo without Karawàbi protecting him, and then beat the boy until he cried.

"Jìmo!"

Rutejìmo jumped at his grandmother's sharp voice. Blinking, he stared across the low table and dredged his mind back into the present. "Y-yes?"

"Were you paying attention to me?"

Desòchu snorted at the obvious answer, and Tejíko glared at him. Desòchu made a show of bowing respectfully, but as soon as she looked away, he pulled a face.

Rutejìmo gulped and shook his head. "No, Great Shimusogo Tejíko."

She leaned over and brandished a knife at him. "Are you paying attention now?"

"Yes, Great Shimusogo Tejíko."

"Good. There is a clan meeting tonight, and I don't want to see or hear a single step from you outside of the cave. Do you understand?" She waved the knife under his nose.

Rutejìmo remained respectful and bowed his head. "Yes, Great Shimusogo Tejíko."

"Good." She turned to his grandfather. "Get up, old man! We have to go to the shrine!"

"Eh?"

She smoothly stood up and smacked her husband playfully on the head. "Hurry up."

"Eh," his grandfather muttered as he shoved himself out of his seat. He reached out for balance.

The scowl faded from her face and she slipped her arm around his chest. She kissed her husband on the ear as she took his weight. Together, they walked to their sleeping quarters to dress in their formal outfits.

Rutejìmo turned to talk to his brother, but Desòchu was already gone. A faint eddy of wind was all that marked his departure. Rutejìmo sighed and stood to clean the table. If he didn't, his grandmother would no doubt punish him again. She said it was being respectful, but it felt as if he were her slave.

By the time he finished scrubbing the last of the plates, his grandparents were gone. Rutejìmo packed the plates and knives into the wooden chest in the corner of the cooking area and closed it.

The cave was quiet except for the crack and pop of the fire. He headed straight for the entrance. He had to hunt down Tsubàyo and finish their fight.

Outside, the valley was dark with night. Only a few caves were lit from the inside. Mapábyo's was on the opposite side of the valley, but both Tsubàyo and Karawàbi lived closer to the entrance of the valley. Rutejìmo focused on Tsubàyo's home, but no light escaped the opening. Farther along, he spotted a flicker of movement in the caves by the entrance, and a fierce anticipation rose.

"You wouldn't"---Rutejìmo jumped at Gemènyo whispering into his ear---"be thinking about getting in trouble, would you?"

Blushing hotly, Rutejìmo spun around as Gemènyo stepped out of the shadows. He was wearing only dark trousers and shoes. A cloud of smoke clung to his shoulders. "N-no."

Gemènyo chuckled. "Don't lie, boy."

Rutejìmo nodded sheepishly.

"Don't."

"Why not!?"

"Because important things are being decided tonight. And it would be foolish if you were to…," Gemènyo waved his pipe in the air. "Ruin anything good coming your way."

Rutejìmo hesitated at the serious tone in Gemènyo's voice. "What happened?"

"Great Shimusogo Mifuníko died a few hours ago."

A sick feeling slammed into Rutejìmo. He had heard Chimípu's mother had collapsed after arriving home a few days ago. The elders believed she'd been poisoned during her last courier run---delivering a peace treaty between two clans warring with each other. By the end of the day, her ashes would be in the shrine.

"I-I didn't know."

"Of course not," said Gemènyo with a sad smile, "you aren't a man yet."

"That means that"---Rutejìmo had to swallow past the tightness in his throat---"Chimípu is going on her rite?"

But Gemènyo was gone. The smoke from his pipe pointed down the path, sucked by the speed of his running.

He focused through the dissipating haze to the shrine house, now bright with torches and flames. Clouds of dust rose up around it as clan members appeared nearby, slowing down into visibility to politely walk across the threshold.

Gemènyo stopped at the entrance and looked back across the valley. It was too far to see his eyes, but Rutejìmo felt a shiver as he imagined Gemènyo looking straight at him. The courier tapped his pipe clean on the threshold, slipped it into his trousers, and stepped inside and out of sight.

For a moment, Rutejìmo stood in the entrance of the cave, torn between his sudden choices. Returning to the cave was no longer an option, but his revenge on Tsubàyo paled under the realization that something significant was about to happen.

He took a deep breath to calm himself and started toward the shrine. His bare feet slapped against the stone as he followed the shadows to circle around and come up to the back side of the building. As he drew closer to the rough stone wall, he could hear the older clan members talking among themselves. It was louder than he expected.

Rutejìmo stopped by the wall and rested his hand against the cool stone. Above him, the night sky was pitch-black except for the motes of light that stretched out in a line from horizon to horizon. In the endless depths of night, he felt very small.

Turning his thoughts back to his goal, he peered down the length of the back wall to find the spot Gemènyo mentioned. Just as he was wondering if the courier was tugging his leg, he realized it was right in front of him. The space between the shrine and the stone wall was small enough he could pry himself into it and climb. At the bottom, the edge of a barrel of blessed water was just at the right height to give him a step up.

A minute later, he was creeping along the solid stone toward the vent and feeling ashamed that he had never noticed how easy it was to get on top before. He focused on the opening where a thin tendril of smoke rose up. He could hear the voices better and prayed the din would cover any sounds he made. Heart slamming against his ribs, he crouched and peered inside.

The shrine was a single large room carved from the living rock. A statue of Shimusògo dominated the back wall, and Rutejìmo felt humbled in the raw presence that radiated from the stone bird. He didn't know if the clan could see or feel him, but everyone knew when Shimusògo was paying attention.

Around the statue were stone shelves from floor to ceiling. Tiny vases and other charms of the clan dead were piled on them. Those who earned honor in their lives and deaths were represented by more ornate vases for their ashes. Most of them were smaller clay pots for those who had lived unremarkable lives in duty to the clan.

Rutejìmo was right above the elder, Yutsupazéso. She was a decrepit old woman who could barely walk. But where the weight of opinion was based on age, she had the strongest voice as the oldest living clan member. She sat bundled in a pile of blankets with two large bowls and a pile of polished black stones in front of her. Each one had a white eye carved into it and there were sixty-one of them, one for every year since she went through her rites of passage.

When a vote was brought up before the clan, she would throw her stones into either the red bowl if she disagreed or the black one to agree. The others would throw in their own votes and then the results would be tallied.

The shrine grew quiet, and Rutejìmo leaned forward to pay attention. An older man, Chimípu's father, walked into the shrine carrying a large, ornate vase. Rutejìmo held his breath respectfully as it was presented to Yutsupazéso.

Yutsupazéso reached up with shaking hands and took the vase. She brought it down to her lap and whispered a prayer to it. She leaned over to kiss the top of it, and Rutejìmo was surprised to see tears sliding down the side of the vase when she handed it back to Chimípu's father.

Rutejìmo watched as the widower carried it over to the statue of Shimusògo and bowed deeply. He set it on the foot of the statue and backed away.

The quiet was almost painful as the entire clan bowed to the statue and the fallen courier.

Rutejìmo tried to show the same respect, but his bowed head was an empty, useless gesture when no one was watching. He looked away to avoid the tears burning in his eyes, then jerked when he realized he wasn't alone.

Chimípu was kneeling next to him, looking down into the shrine in silence. There were no tears in her dark-green eyes, but Rutejìmo could see tension in her wiry frame. Her light-brown skin was pale in the shrine's light, and she made no noise as she stared down.

Rutejìmo stared at her hatchet nose and the line of her throat as he struggled with his emotions. On one hand, he despised her with a passion because she had everything. On the other, she had just lost her mother and somehow managed not to cry.

He opened his mouth to say something but Yutsupazéso's cracked voice rose up. "Is it time for Chimípu to become a woman?"

Rutejìmo glanced over to Chimípu, but only a single tic along her lithe arm tightened to indicate she had heard the question. She stared down into the shrine with a hard look on her face and a determined set to her jaw.

Chimípu's father walked up and emptied a handful of beads into the black bowl. There were tears still on his face.

After a long count, Yutsupazéso raised her hand. "Any other?"

No one moved.

"Then tomorrow she will go to Wamifuko City for her rite of passage. Great Shimusogo Desòchu will be guardian for her trial. Agreed?"

Desòchu stepped forward and bowed to the gathered members. He had a grim look on his face, and Rutejìmo wondered what kind of trial his brother would give Chimípu, knowing the conflict between Rutejìmo and her.

No one disagreed, and Yutsupazéso dropped one of her own beads into the black bowl. A moment later, she swept it out.

"I think," Yutsupazéso continued, "that some of the other children should have a chance to visit outside the valley. Karawàbi?"

Karawàbi's mother and grandfather dropped their beads into the black bowl. Another clan member dropped theirs into the red, but it wasn't enough to overcome the opinions already in the black bowl.

Yutsupazéso waited until everyone gathered up their beads. "Tsubàyo?"

A few stepped forward to pour their beads into the bowls. When the last person stepped back, they were almost equal in measure.

She counted them out before announcing he would go.

"Pidòhu?" Pidòhu was a slender, frail boy who lived with his mother on the far side of the valley. He had a talent with repairing the mechanical dogs and other devices, but failed at almost every race and challenge.

No one agreed or disagreed with his inclusion. Rutejìmo frowned, wondering what would happen, but then Yutsupazéso dropped one of her beads into the black bowl.

"He goes. Rutejìmo?"

Rutejìmo tensed as he heard his name. He clutched the side of the opening and peered down, wondering who would answer for him.

His grandmother walked out, holding all the beads in her hand. Fifty-seven bright-red rocks, each one polished perfectly smooth. She reached out and poured all of them into the red bowl. "He's a fool and an idiot. Let him wait another year. Maybe by then, he'll learn his place."

Rutejìmo let out a quiet groan, then clapped a hand over his mouth. He peeked over to Chimípu. When he saw her smirk, the anger rose up and he pulled back his hand, but the sound of beads falling into a bowl stopped him. Snapping his head around, he looked down to see Desòchu's hand over the black bowl. "He's young, Great Shimusogo Tejíko, but we all were at one point."

His grandmother snorted but said nothing.

Gemènyo added his own to the black bowl, but then reached in and tossed one of the beads into the red.

More stepped forward to pour their beads into the bowls. Rutejìmo felt a twisting in his stomach as time slowed down. Most of the clan members poured their stones into the red bowl against him. He didn't realize so many didn't trust or like him.

But, even as the red bowl filled up, stones were added to the black. He tried to identify those speaking for him, but it was difficult. Unlike the others, everyone in the clan had an opinion about him going on the trip, and the press of people made it hard to see who voted for or against him.

Too soon, no one else stepped up to the bowls. Rutejìmo clutched the side of the opening tightly as he peered down. Even without counting, it was obvious that most of the clan didn't want him to go with the others. It was a blow to his stomach, and the bile rose up in his throat. He sniffed and wiped the tears burning in his eyes.

Yutsupazéso lifted her hand. "Any other?"

A gasp rippled through the shrine. He could see people looking at something near the entrance, but he couldn't see what or whom had caused the surprise. Desperate to find out, he shifted to the side but bumped into Chimípu.

She made no effort to move aside.

He looked up pleadingly at her, but she was staring inside. And she was smirking.

Rutejìmo grabbed the edge and shoved his head into the opening, trying to get a look at what had caught everyone's attention. As he looked around, he matched gazes with Desòchu.

Desòchu's eyes narrowed and he shook his head. His older brother made a point of looking back into the shrine.

Embarrassed, Rutejìmo pulled back shaking and wondered how fast he could run away. He peeked up to Chimípu, but she didn't even twitch from her location.

Then, as if feeling his gaze, she turned her eyes to him and gestured to the opening.

Trembling, he inched forward and peered inside again.

His grandfather stood in front of the black bowl. His hands shook violently as he tried to empty out his stones into it. The white stones bounced on the edge and rolled across the dirt ground but they kept pouring out. Sixty-one stones for someone who had become an adult the same day as Yutsupazéso.

"Why?" snapped Rutejìmo's grandmother. "He isn't ready."

Rutejìmo's grandfather stopped trying to gather up his stones. Desòchu knelt on the ground and picked them up for him, carefully holding each one out before setting it into the black bowl.

"Why?" repeated Tejíko.

"He's ready," came the only reply.
