---
when:
  start: 1472/4/6 MTR 2::68
date: 2012-10-19
title: A Year Later
locations:
  primary:
    - Shimusogo Valley
characters:
  primary:
    - Rutejìmo
  secondary:
    - Chimípu
    - Desòchu
    - Gemènyo
    - Hyonèku
    - Mapábyo
    - Pidòhu
  referenced:
    - Mikáryo
    - Karawàbi
    - Tsubàyo
organizations:
  secondary:
    - Shimusògo
topics:
  referenced:
    - Cultural Differences in Practice (Epigraph)
summary: >
  One year later, Rutejìmo is watching the rest of the clan celebrate the birth of Shimusògo. He saw on the shrine, not wanting to participate, but then Hyonèku and Gemènyo join him with a bottle of wine to give him grief about acting like an old man.

  Rutejìmo finally got the answer of who killed Karawàbi, his brother. Instead of being upset, he understood why the death was important. He asked how close he was to being killed also and they told him they had already decided to do so but then he returned to Chimípu before they could do it.

  Gemènyo noticed that Mapábyo was trying to sneak into the shrine. Hyonèku started to get his daughter, but then Rutejìmo did.
---

> During clan celebrations, nudity is neither a taboo nor sexual. It is freedom of constraints when viewed in public. --- *Cultural Differences in Practice*

The valley celebrated the birth of Shimusògo, a tradition that had been carried out for twenty-seven generations. A bonfire burned brightly, kicking stars of embers high up into the air.

Around the flames, half the valley danced with wild abandon. Dark flesh glistened with sweat from the heat. No one raced or sprinted, but everyone enjoyed moving with nothing but their own feet. Shimusògo wasn't needed that night; he was the one being celebrated.

Rutejìmo smiled sadly as he sat on the roof of the shrine house. He had a bottle of spirits in one hand and he had stripped down to a pair of shorts. The snake tooth hung around his neck. He wore it always even though Desòchu and Chimípu forbade it. Neither could accept what Mikáryo had done, but Rutejìmo refused to forget the woman who saved his life. Chimípu even abandoned the tazágu, and it was Rutejìmo who named it.

Down by the fire, Chimípu spun from partner to partner, moving with a warrior's grace. She caught Pidòhu and flung him around, laughing loudly as her dark red hair swirled in a crescent.

Pidòhu laughed just as loudly. He was stripped down to a loincloth, his skin shimmering in the light of the bonfire. He grabbed her and spun her around twice before bringing her close. Rutejìmo could only see him limping if he was looking for it.

They weren't lovers, but Rutejìmo knew they had shared a bed at least twice. Chimípu was a warrior; she would never mate. Just like Desòchu.

On the other side of the flames, Desòchu pounded on the drums. He was naked from a dare and laughing as one of the women tried to get a bottle of spirits to his mouth. He snaked one arm around her and kissed her before releasing her. He returned to the drums, smacking the top with a wild beat that shook the air.

Rutejìmo couldn't call him brother anymore. He was Desòchu just as Chimípu was herself. They were things he would never have again, but he had finally accepted it. He was Shimusògo now.

"You," Gemènyo said as he sat down next to Rutejìmo, "are supposed to be naked and dancing around like an idiot."

"Why aren't you?" Rutejìmo shot back.

"Eh, can't taste my pipe with all that laughter." He drew on the pipe and let out a long cloud of smoke. "Besides, that's the last bottle of the good stuff."

Rutejìmo handed it over with a smile. "Enjoy."

Gemènyo drained a quarter of the bottle before holding it up over his head.

Hyonèku took it and drank before he sat down on the far side of Gemènyo. He let out a sigh and handed the bottle back to Rutejìmo.

Rutejìmo took it, toying with the expensive glass. It was from Wamifuko City and a gift from a thankful clan. It was Rutejìmo's fourth courier delivery, and Chimípu had given him the honor of handing the sealed message over to the grateful man.

"So," asked Hyonèku, "why are you sulking, Jìmo?"

"I'm not sulking."

"Sitting in the dark on the shrine? Sounds like sulking."

"No." Rutejìmo watched the dancing. There was so much joy in the clan and he felt it, but there was something still hanging in his thoughts. "I'm just…."

He felt them looking at him. He turned, then rolled his eyes before smiling. "What?"

"You tell us," said Gemènyo.

"Just… thinking."

Hyonèku said, "About what? Sour thoughts are for old men like us, not a boy like you."

Gemènyo smacked his friend. "I'm not that old."

"You're thirty-seven," snapped Hyonèku, "and if you keep sucking on that pipe, you'll be dead before me, old man."

Rutejìmo chuckled as he listened. He knew the smile dropped from his face, but he couldn't tear his thoughts away from the dark spiral they followed.

Gemènyo tapped him on the thigh. "Even the darkest thought can't survive open air."

Rutejìmo looked at him. "Can I ask you… both of you something?"

"Of course."

"Who killed Karawàbi?" He knew the answer, but he wished it wasn't true. Rutejìmo didn't dare ask Desòchu, for fear of knowing.

Hyonèku inhaled sharply.

Gemènyo drew on his pipe and let the smoke out in a long streamer. "Damn, boy, I hoped you would never ask us that."

"It was Desòchu, wasn't it?"

The guilty looks told him the answer.

Leaning forward, Hyonèku peered at him. "Listen, Jìmo, there is---"

Rutejìmo held up his hand. "Desòchu protects the clan, even from itself, right? And if Karawàbi had stayed, he would have been poison to that." He gestured to the celebration below.

Neither said anything, so Rutejìmo continued. "He was a bully and cruel, just like Tsubàyo. I remember when Gemènyo tried to tell me to be a better man and Chimípu told me that Mènyo helped her too. All of you were trying to guide us, weren't you? Some of us just weren't listening very well."

Gemènyo nodded slowly.

"And"---Rutejìmo sighed for a moment as he remembered Karawàbi's corpse---"if Tsubàyo hadn't been claimed, his bones would have been bleaching in the sand somewhere."

Another nod.

Rutejìmo tilted back and looked up at the dark sky. He remembered Mikáryo on the opposite side of the flames. She had started a doubt that continued to grow inside him. For a year it had been festering, and he couldn't hold it in anymore.

"How close was I?"

He tensed as he waited for the answer, unable to look at the others.

"Damn, boy," whispered Hyonèku.

Rutejìmo felt the tears in his eyes. "It would have had to be Desòchu, wouldn't it?"

Gemènyo rested his hand on Rutejìmo's shoulder. "I'm sorry."

A tear ran down Rutejìmo's cheek. "How close?"

"We had already decided."

Rutejìmo's stomach lurched. He tightened his grip on the bottle until his knuckles ached.

"But then you and Tsubàyo headed toward that horse clan. We couldn't get close enough without starting a fight with them. We were going to end it the next day, but then you headed back."

Hyonèku said, "None of us believed it. You did the right thing. And when we saw you baring your throat to her, we decided that you deserved a second chance."

"I'm glad we gave you one. You finally grew up and became a good man."

Rutejìmo smiled grimly. He looked back down at the celebration. His brother was dancing with his grandmother, pulling her into circles as they bounded around the fire. There was no anger in Desòchu's face. He was just having fun and raising everyone's spirits. He would protect the clan with his life.

Gemènyo leaned into him. "What are you going to do? Knowing that?"

Rutejìmo drained the bottle and set it down. He gestured to Chimípu and Desòchu. "Nothing. They protect the clan and will do so for the rest of their lives. The only thing I can do to honor that is… run."

"Shimusògo run," said Gemènyo and Hyonèku.

Rutejìmo smiled. "Shimusògo run."

They sat in silence, watching the celebrations and lost in their own thoughts.

Gemènyo broke the silence. "Nèku, your daughter is trying to sneak into the shrine."

Rutejìmo looked around to see Mapábyo creeping along the shadows toward the front door. Her bare feet rose and fell in quiet movements as she kept to the shadows created by the light spilling out the door. She had a bag in one hand and clutched her dress with the other. She was trembling like a leaf as she headed for the unguarded door.

Hyonèku sighed. "Damn. I'll get her." He started to get to his feet.

Rutejìmo held out his hand to stop Hyonèku and stood up. "No, let me."
