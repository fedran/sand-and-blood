---
when:
  start: 1471/4/1 MTR 19::52
date: 2012-09-17
title: From the Shadows
locations:
  primary:
    - Marriage of Stone and Sand
  referenced:
    - Wamifuko City
characters:
  primary:
    - Rutejìmo
  secondary:
    - Chimípu
    - Pidòhu
    - Tsubàyo
    - Shimusògo
    - Ryachuikùo # The horse
  referenced:
    - Zabīno (Epigraph)
    - Karawàbi
    - Mikáryo
    - Desòchu
    - Gemènyo
    - Hyonèku
organizations:
  secondary:
    - Shimusògo
  referenced:
    - Pabinkúe
topics:
  referenced:
    - Birth of the Pabinkúe (Epigraph)
summary: >
  Haunted by the sight of Karawàbi's death, Rutejìmo continued to draw Pidòhu along the desert. He couldn't figure out who did it.

  Chimípu joined him and asked if he would help her prepare for the night. He agreed and helped change Pidòhu's bandage. It got worse, but it also gave Pidòhu a chance to ask Rutejìmo about why he refused to let Chimípu pull the stretcher. Rutejìmo tried to explain how he was too weak: too weak to fight Mikáryo, too weak to match his brother, to weak for anything else.

  When Rutejìmo realized he had bared his soul, Pidòhu pushed him and Chimípu to go on a run. They did and then talked to each other, each one talking about their fears and personal struggles.

  They were interrupted by Tsubàyo who continued to ride Mikáryo's sister's horse. He spoke of hearing the horses and demonstrated powers of the horse clan. Tsubàyo also pointed out that he could travel through shadows, faster than Chimípu or Rutejìmo could run.

  Finally, Chimípu attacked but Tsubàyo disappeared into the shadows. Chimípu screamed like a bird, the powers of her magic growing, before she passed out. When she recovered, they ran back to Pidòhu.
---

> We step through the shadows on silent hooves of steel. --- Pabinkue Zabīno, *Birth of the Pabinkúe*

Even after a day of walking, the three teenagers had nothing to say to each other. Instead, they approached the end of the day with in mindless trudging, focusing on moving one foot in front of the other and lost in thought. The only sounds were the scuff of sand, the whisper of the breeze, and Pidòhu's labored breathing. The heated wind burned Rutejìmo's skin, and he wished he could run, if just for the breeze, but also to escape his own thoughts.

Even through the pain and exhaustion, Rutejìmo couldn't stop thinking about Karawàbi's corpse. The look of Karawàbi's face and the sight of blood had burned itself into his memories. Even worse, he kept imagining himself in the murdered boy's place. He wondered if Karawàbi knew death was coming or if it was a surprise. If it was Tsubàyo, did he sneak up? Did they talk? Did they fight?

Each scenario made him sicker, but he couldn't stop his morbid imagination. Instead, he just walked in despair and silence.

They reached the Wind's Teeth in early evening. The campsite Rutejìmo had seen was completely obliterated by the wind, but the hunk of rock he had broken off marked their destination. Without a word, he set down Pidòhu and began to pitch the tents.

A moment later, Chimípu joined him.

He was startled by the quiet companionship she gave him. With Tsubàyo and Karawàbi, he had bristled under their constant commands and attitude. But Chimípu worked without question, and he felt the need to keep up. He was reminded of his brother's last advice to him, a suggestion to help Pidòhu make dinner. Now, days later, he could appreciate the advice of simply doing what needed to be done.

"Jìmo," Chimípu asked as they finished the tents, "after dinner, do you want to run? Just around the Teeth." She didn't need to mention Karawàbi or Mikáryo, but Rutejìmo could see the fear in her eyes.

With a grunt, he said, "I'd like that."

They shared a brief smile.

Chimípu looked around. "If you get Dòhu comfortable, I'll start dinner. That way, we'll have time before the sun goes down."

Rutejìmo finished the last tie and headed over to Pidòhu. At his side, he knelt down and loosened the ropes to give Pidòhu a chance to move around---as much as he could with a broken leg.

Pidòhu lifted his gaze to Rutejìmo, his eyes steadier than they had been in a while. He reached out with one hand and swatted a fly trying to burrow into his bloody bandages.

"Feeling better, Dòhu?"

"Yes, much better." Pidòhu stretched his arm out before resting it back on his lap. "The fever-block and the pain killers are helping. Everything hurts, but at least the throb is bearable."

"Do you need me to change the dressing?"

"Please?"

Rutejìmo peeled back the bloody bandage. Seeing it no longer brought the bile up, but the smell was overpowering. It was sweet and coppery; it reminded him too much of Karawàbi's blood. He stopped at the final wrapping, where the blood had turned the bandage crimson.

"I can do this," whispered Pidòhu. "You don't have to."

Rutejìmo gave him a thin smile. "Might as well, right? Just tell me if it hurts."

"Of course, Great Shimusogo Rutejìmo."

"I don't deserve that," snapped Rutejìmo as he focused on pulling back the bandage.

"Of course, Great Shimusogo Rutejìmo."

Rutejìmo gave Pidòhu a glare, but the thinner boy just smiled.

"It doesn't matter if you think you deserve it or not. I'm going to use it."

"Why? I'm…." Rutejìmo couldn't find the words. He found a fresh bandage and began to wrap the wound back up, flicking off some flies that landed on the crusty edges.

"Hopeless? Irresponsible? Disrespectful?"

"Yeah," he replied, "all that." It hurt, but Pidòhu was right. He looped the bandage over and brought it back over the cloth pad, careful to avoid the exposed bone. "So why are you calling me great?"

"Because you came back."

Rutejìmo chuckled. "Just like that?"

Pidòhu didn't answer.

Rutejìmo finished wrapping the bandage. When he tied it off, he looked up to see Pidòhu staring at him. "What?"

"Why won't you let Chimípu pull me?"

Surprised by the question, Rutejìmo looked away.

"Because of Karawàbi? Or something before?"

Rutejìmo felt tears burning in his eyes. He sighed and stood up. "I'll set up the water collection."

Pidòhu stopped him by grabbing his trouser. "Rutejìmo?"

Fighting the tears, Rutejìmo wiped his face. He looked down at the sick, injured teenager. "B-Because I'm not strong."

"You're strong enough."

"No, I'm not. I was worse than useless against Mikáryo. I couldn't… didn't stop Tsubàyo and Karawàbi from knocking you off the rocks. I could have stopped Tsubàyo before he stole that horse and killed a woman. I knew it was wrong, but I just ran away. And, looking back, I realize my brother and Gemènyo and Hyonèku were all trying to help me and I just"---he waved his hands---"didn't see it."

Pidòhu said nothing, but he had a sympathetic look on his face.

"And Chimípu. She can do everything and she's amazing. When I think I finally start to get better than her, she's already blowing past me. Run across the desert, and she'd be at the far end, enjoying the sun. Everything, and I mean everything she does, is better than me. How can I compare to that?" He realized he was venting, but the words kept coming out. "She's going to be a warrior, like my brother. She'll no doubt have an honorable life and be revered. Everyone is going to love her and they'll sing songs about her deeds. But… I'm going to be just a runner. They won't sing songs about me," he sniffed, "not like her."

The hairs on the back of his neck rose. Rutejìmo groaned. "Damn the sands, she's listening, isn't she?"

From behind him, Chimípu coughed into her hand.

Pidòhu shrugged and held out his hands. "You were talking rather loudly." He grinned. "I think they heard you in Wamifuko City with that last bit."

Rutejìmo turned around, blushing.

About a rod away, Chimípu stood by the fire pit. The alchemical fire from Pidòhu's pack was hissing underneath the birds she had stored in her travel pouch. Her green gaze was locked on him, but her face was unreadable.

"Um"---Rutejìmo struggled for a moment---"Great Shimusogo Chimípu, I didn't, I mean, I---" He stopped when she held up her hand.

Clearing her throat, she looked around at the Wind's Teeth, the tents, and Pidòhu. "Pidòhu…?" she left the question unfinished.

Rutejìmo turned back.

"Shimusògo run." Pidòhu grinned, then pointed to another set of Wind's Teeth a number of miles away. "Try there."

"No. That's too far." Chimípu shook her head. "What if Mikáryo comes back?"

"I'm not worried about her, actually. She could have killed Rutejìmo last night. I saw the blood on his neck; it would have taken just a second to kill him and you would have never known."

Rutejìmo tensed at the memory of his shameful behavior.

"But," Chimípu insisted, "what about her threat? She wants to kill one of us."

"Oh," Pidòhu said, "I think that's real. But she said three nights. And those rocks are only, what, two leagues at the most? You'll be back in twenty minutes. I think I can stall that long."

Rutejìmo spoke up. "What about Tsubàyo?"

Pidòhu gave a sudden grin. "I'll scream really loudly and crawl away. You won't get enough speed running in a small circle. You need distance to run, not to just jog around. Just"---he pointed to his leg---"come back?"

Chimípu stepped back. She looked around, then returned her attention to him. "Are you sure, Dòhu? We don't---"

"Shimusògo run," he answered.

Rutejìmo looked at Chimípu. Together, they jogged toward the other Wind's Teeth, quickly accelerating into a run. They followed the curve of the dunes without missing a beat. Everywhere Rutejìmo stepped, the ground was solid and gripped him. He had no fear or pain when he ran, as long as he didn't stop moving.

Ahead of them, the dépa appeared, and he felt joy at the sight of the small bird. The world was right when he ran with Shimusògo. With a grin, he chased after it, letting the world blur until the only things left were Shimusògo, Chimípu, and himself.

The second group of Wind's Teeth were tall, narrow rocks. Each was only ten feet across, but there were over a dozen of them. They looked like weeds sticking out of a plain of rocks.

The dépa slowed a few chains shy of the rocks, and so did Chimípu and Rutejìmo. It disappeared as they shifted from a jog into a walk. But Rutejìmo still felt a connection to Shimusògo, a sense he was loved and cherished.

"Do you," Chimípu asked suddenly, "really think I'm that great?"

Her question wasn't a surprise, but Rutejìmo couldn't answer for a moment. "Yes, Great Shimusogo Chimípu."

She gave him a light punch on the shoulder. "Don't call me that."

He remembered Pidòhu's response and grinned. He bowed deeply. "Yes, Great Shimusogo Chimípu."

She punched him again but, when she looked away, she was smirking.

Rubbing his arm, he walked next to her. He didn't want to stop, but even a simple walk was relaxing. "Ever since we were kids, you were always better than me. A better fighter, a faster runner. You managed to steal the ancestors' ashes three times, and the closest I got earned me a beating by my own grandmother."

She grinned. "Great Shimusogo Tejíko pounds on everyone. You remember the time she caught me stealing food?"

Rutejìmo chuckled and grunted. "I never saw you run so fast. You almost made it to the mouth of the valley before she caught you. They heard your screams in the shrine house."

"She wasn't trying very hard. Only a few bruises and no broken bones."

"She was old."

Chimípu's smile dropped.

Wondering if he said something wrong, Rutejìmo snapped his mouth shut.

She didn't stop walking, so he continued to pace with her. It was a few minutes before she spoke again. "Jìmo?" she asked as they came around one of the stones. "You ever notice that there is only one warrior who made it to old age in the valley? Your grandfather."

Rutejìmo sighed. "I don't notice a lot. But Dòhu mentioned it to me. I never thought about it, really. Not a lot of people live past their forties, do they? But none of the warriors are over thirty."

Her footsteps crunched on the rocks. A few pebbles bounced away as they threaded their way through the Wind's Teeth.

Rutejìmo realized Chimípu doubted her own abilities as much as he doubted himself. It was strange to see her struggling with anything, but there was a storm of emotions behind her expressions, and sadness seemed to be the strongest.

"When I was younger," she started, "I told my mother that I wanted to marry Desòchu."

He stopped, staring at her in surprise. He never thought of Chimípu as desiring anything, much less his brother.

"He was always… there, I guess," she held out her hands. "Strong, powerful, and handsome. Even at twenty-one, he wasn't married, and I hoped---begged Shimusògo, actually---that he was waiting for me."

Rutejìmo felt a prickle of discomfort at the idea of his brother and Chimípu. He wanted to say something, but didn't. Instead, he thought about his own words to Pidòhu. And he guessed that all three of them had struggled with fears and doubts over the last few days.

If Pidòhu could give Rutejìmo an ear to listen and a hand to hold, then he could do the same for her. Rutejìmo shoved aside his own fears and listened.

"My mother told me about the warriors that night. About how Desòchu will never grow old in the valley." A tear ran down her cheek. "Never have children. He will die out there"---she pointed to the desert---"far from home."

Rutejìmo toed the ground. He knew she didn't need him to speak, but remaining silent was uncomfortable.

"I cried when she told me that. I wanted to deny it. But, as the years passed, I saw it was true. Every time he went without the others, you could see it in his eyes. He may not come back. If he ever had to choose between himself and the rest of the clan, he would die with pride. It was terrifying that he could do it. One day… one day, I asked him how he could keep running. You know what he did?" She looked up and gave him a sad smile. "He smiled."

Rutejìmo pictured his brother, his easy attitude and kind words. He was friendly to the entire clan. His brother had given his life to the clan. Not to one woman and not to children he would never have, but to every one of them.

Chimípu started walking again. "A few years ago, I realized I wanted to be the same. I would give up on my legacy so I could be like him. Love and protect everyone, not just myself."

"I-Is it worth it?"

She smiled at him, her eyes shimmering. "Well, you're still an asshole, but you're getting tolerable."

"Oh," Rutejìmo responded with a mock glare, "glad to hear that. I'll try to keep my ass from speaking in the future."

She grinned, then burst into laughter.

Rutejìmo joined in with a snort.

Together, they kept walking around the Teeth.

When they regained their composure, Rutejìmo finally asked one of the questions that was haunting him. "Mípu? How did you fight Mikáryo? It was dark and you didn't have light."

"It was strange." She rubbed her arm. "I woke up when I felt Shimusògo's feathers against my cheek. I almost made a noise, but then the feathers were against my lips. So I held still and listened. As Mikáryo threatened you, I realized I could… feel her. Like a silhouette in the darkness."

"Why did you attack her?"

"When she threatened you, everything became clear as glass. Shimusògo moved and so did I. I did the same thing as when we run, chased after the dépa, but it was… it was…." She sighed. "I can't really explain. I chased Shimusògo and we fought her together."

Rutejìmo chuckled. "I'm glad one of us did."

Chimípu grinned and punched his arm.

He stumbled to the side and rubbed it. "Ow."

"Stop doubting yourself, Jìmo. You see Shimusògo. Even if you think it's a fluke, do you think Shimusògo would show himself to Karawàbi? Or Tsubàyo?"

Rutejìmo sighed. "I know, but---"

"No," said Tsubàyo. "I found something better."

Chimípu responded first. She used her right hand to push Rutejìmo behind her as she pulled out the knife. The blade shone in the sunlight as she aimed the point at the deepest shadow of the Teeth.

Rutejìmo couldn't see what she saw, but he remained behind her. His heart thumped louder as he peered around, just in case Chimípu was wrong. When he didn't see anything, he peered over her shoulder.

Blackness stirred within the shadows before it oozed out into the shape of a massive black horse. Rutejìmo couldn't tell how the horse managed to hide, but somehow it did. Fear clutched his heart as the creature continued to step out of the shadows. It was huge, six feet at the shoulder, and with pitch-black hair. The eyes were two black orbs, but, as the horse stepped into the sun, the pupils contracted into two tiny points of inky darkness.

Tsubàyo crouched over the back of the horse. He clutched to the mane tight enough that his knuckles were almost white. There were dark shadows around his eyes, giving them a sunken appearance. With his scar, he had the mask of a rikunámi---a nuisance, prairie creature known for stealing shiny trinkets. He still wore the same clothes Rutejìmo last saw him in, and he smelled as though he hadn't cleaned himself in days.

"Tsubàyo!?" Chimípu stepped forward. "Where have you been?"

"Riding," he said in a gravelly whisper, "and it feels good."

"You stole a horse."

"Yeah, I guess I did." A smile crossed his lips.

"And the Pabinkúe woman?"

Tsubàyo looked to the side, back in the shadows. His eyes scanned the darkness for a long moment, before he turned back. "I needed the horse." He patted the equine underneath him. "She was in my way. It was easy… easier than I thought it would be."

The horse lifted its head and exhaled hard. It lifted one foot and stamped. Tiny rocks vibrated from the impact.

Chimípu stepped back, pushing Rutejìmo's chest as she moved.

He followed her silent command, unable to take his eyes off the massive equine or the teenage boy on top. He had grown up with Tsubàyo, but there was no familiarity or compassion in Tsubàyo's eyes. There never was, but as he looked at the teenager, Rutejìmo felt as though he was staring at a stranger.

Tsubàyo looked back at the shadows again, a nervous look briefly crossing his face. "Where is Dòhu? Was he honorable and finally die?"

Rutejìmo gasped.

Chimípu snapped at him. "No! He didn't. He's… he's close enough."

"Not that close. I saw you running up." Tsubàyo chuckled. "I'd say he's about two or three leagues that way, isn't he?" He pointed back the way Chimípu and Rutejìmo had come.

"You'll never get to him before we do."

Tsubàyo favored her with a smile, his lips pulled back to show the brightness of his teeth. "Ryachuikùo here is very fast in the night. I don't really have to outrun you, though, since you'd never see me coming."

Chimípu tensed, the hand against Rutejìmo's chest trembling with her emotions. "You will not touch my clan."

"Your clan!? We don't have a clan. That bird abandoned us. The adults left us. They left us to die in the desert!"

Rutejìmo snapped loudly, "No, they didn't!"

Tsubàyo lifted his gaze to stare directly at Rutejìmo. His eyes were angry and dark, hatred almost palpable in his eyes. "Any spirit which would take you, Jìmo, is a fool."

The horse stepped back, a slow and measured movement despite Tsubàyo not saying or commanding it. Its tail disappeared into the shadows, and Rutejìmo couldn't even see a hint of movement in the darkness.

Tsubàyo glared at both of them. "And I don't have time for fools."

The horse took another step back, and Tsubàyo leaned into the movement. As he crossed the threshold of light and shadow, his body disappeared.

"Tsubàyo!" called Chimípu.

Tsubàyo leaned back into the light. "Yes, girl?"

Chimípu didn't rise to the insult.

"You killed that woman, didn't you?" Both her voice and body were tense. Rutejìmo could see her muscles jumping as she crouched down. The knuckles on the knife were pale as she prepared to strike.

Tsubàyo looked into the shadows, his body disappearing into darkness as he did. When he looked back, his eyes were black orbs before they slowly adjusted to the light. "Why?"

Chimípu said, "Pabinkue Mikáryo is looking for a life in return for the one you stole."

"Really?" Tsubàyo smiled. "Then I guess one of you better sacrifice yourself, because I'm not getting off this horse."

Rutejìmo spoke up again. "How? How could you do that? Shimusògo run. We don't ride."

"Yes, I heard that pointless saying my entire life. Shimusògo run," he said sardonically, "but there is power up here. Power to hide in shadows and watch the fools run across the sands. I can feel it every time I ride."

Chimípu stepped forward, brandishing her knife. "You must give yourself up. We only have three nights."

"No, girl, I won't."

"We won't die for you, Tsubàyo."

"No," he said as the horse stepped back, "you're going to die because she's going to kill you." His voice faded along with his body. "Because she won't find me."

With a scream of rage, Chimípu exploded into movement. Dust and rock slammed into Rutejìmo, throwing him back as she charged. Her body glowed with liquid sunlight as she plunged into the shadows. The darkness peeled back as she slashed with the knife, trying to find him with the edge of her blade.

But, despite Tsubàyo and the massive horse stepping into the shadows just moments before, she hit nothing.

"Damn that bastard!" Chimípu threw back her head and screamed in rage. It was a high-pitched screech that sounded uncomfortably like that of a bird. Her body ignited with a golden flame that burned away the shadows around her. She became a blinding sun in an instant as a translucent dépa superimposed itself over her body. The image expanded to twice her height before it dissipated in swirls of golden sparks.

Rutejìmo whimpered at the noise, his hands halfway to his ears, but it was too late to block the sound.

Her scream echoed against the rocks, fading quickly. She took a step out of the shadows. Her body swayed once, and then all the tension left her limbs.

Rutejìmo saw her falling and he panicked. He froze, unable to act. But then all the frustration that he wasn't good enough shoved him into movement. He sprinted forward and covered the short distance between them. He caught her and held her tight to his body as he skidded to a halt.

As he stopped, the dépa that had led him in his short sprint faded away.

His momentum carried him a chain past the Wind's Teeth. He landed on his knees, cradling her. "Mípu? Mípu!"

Groaning, Chimípu opened her eyes. "What happened? Did Tsubàyo hurt me?"

"I-I don't know. You let out a scream, ignited in flames, and then passed out."

"I---" She groaned and pushed him away. Slumping to the ground, she gasped for breath. "I don't remember. I… I was angry, and then I felt Shimusògo inside me."

"I saw that…," his voice trailed off as his mind spun. He gasped. "I've seen that. Desòchu can do that!"

She pushed him away and staggered to her feet.

Rutejìmo stood up with her, holding out his hands in case she fell. "Mípu, we need to go back."

"I think," she groaned again, "that is a good idea."
