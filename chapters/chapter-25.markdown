---
when:
  start: 1471/4/3 MTR 10::20
date: 2012-09-20
title: Lessons Taught
locations:
  primary:
    - "Oijogushi's Abandoned Valley"
  referenced:
    - Pabinkue's Lost Tears
characters:
  primary:
    - Rutejìmo
  secondary:
    - Mikáryo
    - Chimípu
  referenced:
    - Pidòhu
    - Garèki (Epigraph)
    - Tachìra
    - Tsubàyo
    - Pabinkúe
organizations:
  secondary:
    - Shimusògo
    - Pabinkúe
summary: >
  Rutejìmo woke up to Mikáryo's cooking. He asked why she protected him. She told him that she watched over him because he was "pathetic." She also told him that Chimípu had returned in the night.

  He went to wake her, but she was already up. She apologized because she wasn't able to save Pidòhu. After a few short words, he left to answer nature and found that Mikáryo had killed a giant snake that would have attacked him. Mikáryo came back and chipped off a tooth, telling him to wear it until he figured out his position in life.

  They returned to the fire and talked to Chimípu. Mikáryo insisted that Chimípu go after Pidòhu, but take Rutejìmo with her. She told them were Tsubàyo would be and gave Chimípu her fighting weapon.
---

> Children in the desert have a special place, beyond the wars of night and day. --- Kimichyufi Garèki

Rutejìmo woke to the smells of a wood fire and cooking meat. He snapped open his eyes and stared up at the unfamiliar tent above him. For a moment, he didn't know where he was, but as the events of the night drifted through his mind, he remembered crawling into Pidòhu's tent. He rolling on his stomach and peered out of the gap between the tent flaps.

It was still night, but barely. There was a sharp edge where sunlight began to reach over the horizon, but the desert remained in shadows. The air was crisp, and he felt the bite of the night's coolness against his skin.

He crawled out and stood up. He breathed in the exotic scent of wood and followed it to a campfire.

Mikáryo had her back to him as she crouched over the flames. She had stripped down to a loincloth and a black top that wrapped around her chest. She balanced on the balls of her feet, which she had dug into the sand. Black tattoos covered her entire body, except for a bare patch centered on her spine. The unmarked area of her skin was in the shape of a horse head. She had fresh wounds across her back and arms that he hadn't notice the night before. Dark-gray bandages wrapped over them, but there were bright-crimson stains in the center.

Even with her injuries, he remembered the look she gave him over the fire. She was exotic, and he had become painfully aware of her femininity and strength. A strange tingle ran along his skin, an unfamiliar sensation. And then it was gone, leaving behind only a pounding heartbeat.

She looked over her shoulder at him, her eyes dark and a smile across her brown lips. "You aren't ready for that, Jìmo."

"I-I---"

"You should just stop talking," she said wryly. "You'll just embarrass yourself."

Rutejìmo sighed. "Sorry, Great Pabinkue---" He hesitated at the glare ghosting across her face. "Káryo."

"Good. Now, come over here and eat. You need to eat. Tachìra will be rising in a half hour"---she gestured to the brightening horizon---"and he and my clan don't always see eye-to-eye."

He inched forward as she spoke and looked at the hunks of meat sizzling on both of her tazágu. It was the same meal as the previous night, but far better than the rations or the meat from the alchemical bag.

"Eat so I can have my weapons back."

Grabbing a wooden board, he eased the meat from the spike and sat back. "Káryo?"

Mikáryo stood a rod away, wrapping the long black cloth around her body. She had one foot up on her horse as she covered her leg. "Yes?"

"Why are you doing this?"

She looked back at him, shook her head with a smile, and returned to dressing. "You keep asking that. Do you not like the answer?"

"No, but you're a clan of the…."

"Oh," she said, "you want to know why the evil murdering and stealing assassin of the night is helping the noble warrior of light and justice?"

Rutejìmo blushed hotly. "I'm not a noble warrior."

"And that," she said in a whisper as she strolled closer, "is why I'm helping you." She reached down and caught his chin with her fingers.

His heart thumped, and he felt the burn of embarrassment searing his cheeks.

"You are utterly helpless. Pathetically, in fact." She smiled, one corner of her lips curling with her amusement.

Rutejìmo's emotions turned to a sharp anger.

Mikáryo chuckled and released him. She bounded back to her horse, the long cloth trailing behind her. When she reached her mount, she braced her foot on his flanks and drew up the fabric. "Your little warrior girl should have never left you."

"She was going after Pidòhu."

"But you don't leave your clan alone. Especially not one as clueless and vulnerable as yourself." She gestured to the meat in front of him.

Getting used to her insults, Rutejìmo grabbed the food and began to eat.

"I almost didn't come down here," Mikáryo chuckled, "but let's say the decision was made for me."

"By who?"

She gestured toward the rocks. "Finish eating first. And then wake up your warrior girl."

"Chimípu? She's here?" He started to stand up.

"Eat, then wake her up," commanded Mikáryo.

He dropped back down. "Yes, Káryo."

Grinning, she moved to her other leg. Her hands were sure and graceful as she covered her body. By the time she finished dressing in the dark cloth, Rutejìmo was licking the last of the juices from his fingers.

Standing up, he hurried toward the tents just as Chimípu came around the boulder. He stopped. "Mípu?"

Chimípu glanced at him, then at the boulders. Her face was pale. "I'm sorry, Jìmo, I couldn't save Pidòhu."

He cocked his head, trying to figure out the proper response, but she wasn't done.

"And I failed you."

"What? No, you were defending your clan."

Chimípu glanced back around the boulders, in the direction Mikáryo had pointed earlier. She turned back. "I shouldn't have left." Without another word, she pushed past him and headed toward the fire.

Curious, Rutejìmo headed back around the rocks. His feet scuffed along the ground as he walked. He didn't see anything until he came around the far edge, but he smelled blood. A tremor ran down his spine as he inched forward, peeking around the last rock.

Behind the boulder was the largest snake he had ever seen. It was over a hundred feet, maybe even two chains. The creature's jaw was larger than his head. One of the fangs had been snapped off but the other was buried in the ground. The black, unseeing eyes seem to stare at him.

Along the side, in the fleshy underbelly, he could see where Mikáryo had been carving out hunks of the dead snake, his dinner and breakfast. Footsteps marked the sand and rock. He could see swaths carved out from the charge of Mikáryo's horse and even Chimípu's footprints as she went out to investigate it.

"Don't."

He jumped at Mikáryo's voice.

She continued, "Don't say anything to your warrior girl. No reason to beat a corpse I've already killed." She patted him on the shoulder. "I think she figured out the lesson."

Mikáryo walked past him. She made no noise in the sand as she passed, just the black shadow of a body flowing along the ground. She knelt down at the jaw of the snake and forced it open. She kept pulling until there was a muted crack.

Rutejìmo jumped at the noise. He cleared his throat to cover his embarrassment. "What did she learn?" He didn't want the answer, but had to hear it.

"If you leave the weak alone in the dark, there is a good chance you'll come back to a corpse." Mikáryo glanced at him, her eyes hard.

Rutejìmo shivered. "I should thank you, shouldn't I?"

She tore the snake's fang out of its mouth. Blood sluggishly ran from the gaping wound. Holding it with both hands, she slammed it down repeatedly on a rock until the tip of the fang snapped off. She snatched the tip from the ground and walked over to Rutejìmo holding it. She pressed the fang and a length of leather into his palm.

He stared down at it. His throat was dry.

"But you, Jìmo, are a bit too slow for lessons like these. So," she said as she folded his fingers over the fang, "wear this until you learn your place in your clan. You are like a wounded rabbit out here, and if you don't grow up, you'll be a vulture's dinner before long."

Rutejìmo stared into her eyes for a long moment. "Thank you, Great Pabinkue Mikáryo."

She left him to his thoughts.

He walked around the snake twice, trying to remember the fear from being so close to death, and then returned to the dying fire.

Chimípu had finished eating and left Mikáryo's two tazágu on the empty board. She was pulling down the tents and tearing apart Pidòhu's frame, moving with rapid speed, and keeping her back to Mikáryo. When she did look back, it was to glare at the Pabinkúe woman.

"So, little warrior girl---" drawled Mikáryo.

Chimípu stiffened.

"---do you remember which direction you were running blindly in the dark?"

With a nod, Chimípu finished packing Rutejìmo's pack. She tossed it aside and started on her own.

"Do you know where Pabinkue Bàyo will be tonight?"

"I can find him," snapped Chimípu.

"I have no doubt about that. But, for Jìmo's sake, I recommend that you head to a set of Wind's Teeth about thirty leagues in that direction." She pointed to the south. "There is a large arch a number of chains in length and maybe a chain or so in height. He'll be there."

Chimípu glared at her. "How do you know?"

"Some generations back, the Pabinkúe lost a battle there, and a lot of our blood was shed on those sands. He doesn't know it, but he'll be drawn there. The spirits always return to their blood."

"Why should I trust you?"

Rutejìmo opened his mouth, but Chimípu silenced him with a glare.

"You don't or you do"---Mikáryo shrugged---"I don't care either way. But, if you leave Jìmo alone in the dark again, who is going to save him?"

He flushed, but forced himself to remain silent.

Chimípu stalked toward Mikáryo. "Are you threatening him?"

Mikáryo seemed unperturbed. "No, little warrior girl, I'm not. I have no interest in you, Jìmo, or Pidòhu."

"Then why do you still want a sacrifice?" Chimípu dropped her hand down to her knife.

"Pabinkúe doesn't want you anymore, girl. I want Tsubàyo. He has ridden my spirit, and I cannot accept anything else."

"That isn't what you said!" The knife drew out an inch from the sheath.

"Things change like the winds of the desert. I wanted blood for my sister's death, but now Pabinkúe demands what is hers. When it comes down to it, my clan's needs will always be more important than my own."

"You can have him," Chimípu said.

Mikáryo smiled broadly. "I will, but I'd like to know if I'm cutting his throat before he reaches home or not. So, go get your little Pidòhu back and show me the man Tsubàyo will become."

"And you'll just follow to kill us?"

"No," came the reply, "I won't. This is something between the children of the clans, not the elders. I'll watch like the ones watching you, laughing at Jìmo and wondering who will come out on top."

Rutejìmo stared down at the fang in his hand. He closed his fingers over it and felt the sharp edges cutting into his palm.

Chimípu sighed and grabbed her pack. Slinging it over her shoulder, she said, "Rutejìmo, it is time to go."

He grunted and walked around Mikáryo. At her amused snort, he gave her a guilty look but then knelt down to gather up his belongings. When he got to his pack, he discreetly looked inside for his stones. He found them wrapped in some cloth at the bottom. He also took half of Pidòhu's but left the remaining with Chimípu's rocks. He added the tooth to the cloth and shoved everything into his pocket.

Rutejìmo looked over the empty campsite and then turned to the woman who had saved him. He peeked at the furious Chimípu and then gave Mikáryo a deep bow.

Mikáryo smiled and bowed back. "Safe journeys, Jìmo."

He turned and caught a glare from Chimípu. Blushing, he stepped back.

Still glaring, Chimípu turned and inspected her route.

"Warrior girl." Mikáryo walked over to her, her tazágu in her hands.

Chimípu stepped back, her hand on her knife.

Moving faster than Rutejìmo could see, Mikáryo swung the tazágu.

Chimípu parried with a ring of metal.

"Your weapon is a boy's toy."

Rutejìmo flushed. Mikáryo glance at him, then grinned. "An immature boy's toy at that." She flipped the tazágu over and handed it to Chimípu hilt-first. "Warriors need real weapons, not toys."

Chimípu stared at the unmarked blade. "This is nameless."

"Yes, but my other has personal significance. And you need a weapon for the coming days."

The teenage girl's jaw tightened. "I'm about to fight someone claimed by your clan."

"So?"

"Are you telling me to kill him?"

"No," Mikáryo said, "I'm giving a little girl a real weapon because she is about to be fighting not only for her life, but for two others. How she uses it is entirely on her soul, not mine."

Chimípu paled, still staring at the weapon.

"Take it as a gift. No obligations, no promises. And, if you kill someone, name it Shimusògo, not Pabinkúe."

Sheathing the knife, Chimípu took the weapon. "Thank you, Great Pabinkue Mikáryo." The muscles in her neck tightened as she spoke.

As the sharp point slipped from Mikáryo's fingers, her body dissolved into shadows. Darkness flooded across the campsite and poured into the dark spots formed by the rocks and boulders. It passed over Rutejìmo with a wave of coolness, and then Mikáryo was gone.

Chimípu stared down at the weapon, her fingers tight around the hilt.

"Mípu?"

She looked up. Then towards the direction of Tsubàyo. "Come on, Jìmo. We have to save Pidòhu."

He was afraid she was going to say "kill Tsubàyo," but even though she hadn't he knew it was in her thoughts.
