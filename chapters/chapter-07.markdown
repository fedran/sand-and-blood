---
when:
  start: 1471/3/42 MTR 20::30
date: 2012-04-25
title: Middle of the Trip
locations:
  primary:
    - Three Falls Teeth
  referenced:
    - Wamifuko City
characters:
  primary:
    - Rutejìmo
  secondary:
    - Chimípu
    - Tejíko
    - Pidòhu
    - Karawàbi
    - Tsubàyo
    - Gemènyo
    - Desòchu
    - Hyonèku
    - Jyotekábi
  referenced:
    - Paladin (Epigraph)
    - Tachìra
    - Shimusògo
organizations:
  secondary:
    - Shimusògo
topics:
  referenced:
    - War Council of Kormar (Epigraph)
summary: >
  Many days later, Rutejìmo continued to struggle with keeping up with the clan. They all ran across the desert, in the sun, and the effort exhausted him. He was the slowest except for Pidòhu. Desòchu didn't help Rutejìmo, but he did help Pidòhu which invoked some jealousy in Rutejìmo.

  As they arrived at a set of Wind's Teeth, a set of tall rocks sticking out of the ground, Desòchu suggested that Rutejìmo help Pidòhu. The last person to enter camp did most of the work setting it up. Rutejìmo didn't help, but Chimípu did. Chimípu had not been struggling with the run and kept up with the elders easily.
---

> The desert has no time for the weak and sickly. The savages slaughter children with minor deformities and constantly push their youth to their limits. --- Paladin Ruse, War Council of Kormar

Rutejìmo's chest ached, and sweat poured down his neck and shoulders. Sunlight bore down on him, stealing his breath as he struggled to jog along the ridge of the dune. For all the practice, wrestling, and racing in the valley, nothing could prepare him for the difficulty of simply keeping up with the rest of the clan day after day.

He ran strongly for the first day, but he was soon straggling behind everyone but Pidòhu.

By the third day, his cheeks burned with humiliation and he prayed for the end.

After that, he lost track of how long they've been running. It had all blended together, but it felt like weeks. His life had been reduced to running during the day and sleeping fitfully at night, only to be woken at dawn to do it all again.

His only comfort was that he kept up better than Pidòhu. Hiding his smile, he glanced over his shoulder. With every step the other boy struggled to keep moving. As he reached the top of a dune a few chains away he stumbled and sent sheets of sand pouring down in front of him. As much as Rutejìmo strained to keep up, it was nothing compared to Pidòhu's efforts.

Desòchu paced next to Pidòhu, jogging without any effort. He smiled and held his hand out, but wouldn't grab Pidòhu unless the weaker teenager reached out for him.

Jealousy rose up inside Rutejìmo. Desòchu had never given him the same attention as he had Pidòhu. His brother always pushed for him to run faster, even in the baking sun, but then sent him to run on his own. Rutejìmo turned away to avoid the anger that surged up inside him. Focusing on his destination, he strained to find some burst of energy to get him through the last chains until he reached the camp.

The clan had stopped in the space between three narrow columns of rock that rose into the sky. They were called Wind's Teeth, and legends claimed they were the bones of some ancient creature that used to wander the deserts. Now, they were just used for landmarks and shelter for the clans who traveled the sands.

As he jogged down the final dune, he peered through the rocks to the camp. Most of the familiar tents were already set up, the bright colors of the Shimusògo clan comforting. All the tents were small and easily carried by one person. The material was thin but strong; it was also expensive and required a trip to Wamifuko City to obtain. Since he would spend most of his adult life in one, it was an easily justified cost.

Karawàbi and Tsubàyo were gasping for breath with their backs to one of the Teeth. Karawàbi was on the ground, head between his large hands while Tsubàyo bent over and braced himself on his thighs. They were soaked in sweat and shaking violently.

Gemènyo stood next to them, waving his hands wildly as he went on about a dangerous adventure that would, like most of his stories, end with him drunk off his ass. Like most of the adults of the clan, Gemènyo wasn't covered in sweat or even winded. Instead, he hopped back and forth as he tried to cajole Tsubàyo and Karawàbi into standing up.

Beyond the rocks, Chimípu stood calmly as she spoke to Hyonèku with only a triangle of sweat down her shirt and a glistening on her brow. If he didn't know better, Rutejìmo would have sworn she had just started on the trip.

Rutejìmo glared at Chimípu as he came up to the outer edges of the camp. While he struggled to run the last chain without throwing up, she was calm and collected. He slowed down as he fought his jealous thoughts, knowing that being blatant with his opinions would just ask for trouble and mockery from the adults.

"Come on, little brother," called Desòchu from behind, "the last person in camp has to make dinner tonight."

Groaning, Rutejìmo forced himself to speed up again. He had to make dinner the night before and the quips and unceasing demands still wore on his thoughts. His foot caught a thick ridge, and he lost his balance. He barely had a chance to throw his hands in front of him as he slammed face-first into the searing-hot sand.

"You know," said Gemènyo, "if Karawàbi and Tsubàyo weren't gasping for breath, they would be laughing at you."

Rutejìmo glared up at the courier, but took Gemènyo's offered hand to stand up. He sputtered to clear his mouth and wiped the grains from his sweat-soaked face. He looked at Gemènyo, who didn't even have a droplet of sweat on his body. "How do you do it?"

Gemènyo shrugged and held his palms up. "We are the clans of the sun. Just as Tachìra"---he pressed his hands together in prayer and looked up at the sun---"grants Shimusògo the power of speed, so we must honor the great sun that gives us life."

"Besides," Desòchu cheerfully added as he jogged up, "would you really want to run in the dark? You seem to have enough trouble remaining upright when you can see the sand."

Rutejìmo blushed hotly. He watched as Pidòhu staggered into the camp, the last to arrive. It wouldn't be Rutejìmo making dinner. He would have felt a thrill of triumph, except he was too exhausted to care. He gasped for air, then leaned against his brother. "Does it get any easier, great brother?"

"When Shimusògo is before you, the sun stops hurting and the heat doesn't burn quite as much. But, if you don't struggle now, you won't respect the gifts you're given later."

"I would."

Desòchu smiled and clapped him on the shoulder. "I know, little brother, and your day will come. Just a bit longer."

Pidòhu whimpered as he slumped to his knees. Air ripped past Rutejìmo as Gemènyo appeared next to Pidòhu. The tiny eddies of sand spun away as Gemènyo caught him and helped him back to his feet.

"No, no, little one. Just a few more steps."

"I-I---" gasped Pidòhu, "I can't."

"Of course you can," Gemènyo said compassionately, "just take one more step."

Pidòhu whimpered as he took another step, then a second.

Rutejìmo and Desòchu followed mutely behind the two. The scuff of sand shifted underneath Rutejìmo's feet. He almost slipped, but knowing the clan was watching, he wrenched himself back into place. A blush burned on his cheeks, and he fought the urge to gasp for air.

"Rutejìmo," Desòchu said in almost a whisper.

Rutejìmo looked up, surprised. "Y-Yes?"

"You should help Pidòhu with the cooking tonight."

"Why?" he grumbled. "He came in last."

"He's clan. And he is struggling. He could use a friend."

Rutejìmo watched as Pidòhu staggered into the center of the camp near the fires. Gemènyo stepped back as the frail boy dropped to his knees and dug into his bag. Pidòhu's back was covered in sweat, and tiny rivulets ran down to his arms before dropping off. Droplets hit the sun-heated sand, and tiny curls of steam rose up from the impact.

Desòchu clapped Rutejìmo on the back. "Good run, little brother. Just think about what I said. No matter how much you don't like him, clan is everything."

"Yes, Great Shimusogo Desòchu."

Desòchu gave him a smile and jogged over to talk to Gemènyo and Hyonèku.

Rutejìmo stopped at the rocks across from Karawàbi and Tsubàyo. He glanced over to the other teenagers, who glared back.

Tsubàyo stood up and shoved himself off. "Couldn't keep up, huh?"

Bristling at the comment, Rutejìmo balled his fists and took a step forward, but then he felt attention on him. Peering around, he saw both Hyonèku and Gemènyo watching him warily. He let out a hiss of annoyance and forced his fingers to relax.

Smirking with triumph, Tsubàyo gestured for Karawàbi to follow and headed toward the adults.

Rutejìmo watched him leave, hating that he couldn't beat Karawàbi into the ground like the smug bastard deserved. He peeked back and noticed Gemènyo was still watching. With a sigh, he took a step toward Pidòhu, but stopped as Chimípu knelt down next to him.

Without saying a word, Chimípu helped Pidòhu finish gathering up the cooking supplies and carried them over to the fire pit.

Pidòhu followed a few steps behind. "Thank you, Great Shimusogo Chimípu."

She nodded and pulled the cutting board out of Pidòhu's arms.

Rutejìmo stepped back. He didn't want to be near Chimípu at the moment, not after struggling so much to keep up with the clan. He felt a dark cloud gathering over him as he turned his back on the two and headed to the shadows of the Wind's Teeth and away from everyone else.
