---
when:
  start: 1471/3/44 MTR 10::63
date: 2012-08-25
title: Breaking Up
locations:
  primary:
    - Nisanto Finger Stop
characters:
  primary:
    - Rutejìmo
  secondary:
    - Karawàbi
    - Shimusògo
  referenced:
    - Chobāni (Epigraph)
    - Tsubàyo
    - Chimípu
    - Pidòhu
organizations:
  secondary:
    - Shimusògo
summary: >
  Rutejìmo woke up in the desert, alone and helpless. He didn't know where to go, so he walked blindly until he realized that the bird would appear if he ran. He accelerated and then followed the clan spirit back to the camp.

  At the camp, Karawàbi demanded that Rutejìmo make him breakfast. Rutejìmo realized that he couldn't remain with him and started to pack up to return to Chimípu and Pidòhu. Karawàbi realized that and attacked Rutejìmo. It was a short fight until Rutejìmo managed to slam a tent spike into Karawàbi's foot before running away.
---

> Desert spirits cannot be first heard during happiness. Pressure is needed to open up the gates of power. --- Mifukiga Chobāni

Rutejìmo woke up huddled against a rock. The morning sun had not reached the horizon, but the false dawn gave his aching eyes a chance to focus on something besides ever-present darkness.

It had been a night of hell for him. He had only dozed and shivered. Every time he started to drift to sleep, the fear that something was going to rush out of the darkness kept him awake. Every wind, every prickle along his skin, and even the pounding of his heart refused to let him close his eyes for long.

He yawned with exhaustion. As soon as he could distinguish the ground from the sky, he staggered to his feet. Pain radiated from his leg, and he looked down to where he had slashed open his trousers on the rock he used as shelter. The long gouges were bloody, but he had managed to keep them covered long enough for a thin scab to dry over the top. His hands were smeared with dried blood. Disgusted, he used some sand to scour them clean before crawling up on the rock.

His entire body ached from the effort, but he couldn't survive another night alone in the desert. Shielding his eyes, he turned in a slow circle in hopes he could see the camp.

To the one side, he saw smoke billowing up in a lazy cloud. It was too large to be his camp, and he shivered at the nightmare of the immense brass vehicle towering over the campfires. Turning his back, he tried to imagine the route they had walked and peered along the horizon.

He couldn't see anything, and despair gripped his gut. Around him, the wind kicked up sand, and the dry grit scraping against his bare skin encouraged him to move. He yawned again, found a place to relieve himself, and made up his mind to start moving.

One lesson was drilled into him from the moment he could walk. If he was lost, he was to head for the tallest rock. For many places, it would be the Wind's Teeth, but at least it gave him a direction. He looked around again and saw a pair of rocks sticking out. It was in a different direction than the scorpion and the clan, and he considered the distance with trepidation. It was over a mile away, but he didn't know how long he had been running in the dark. He could be a hundred chains or a number of miles away from his tent.

Rutejìmo promised himself he would never run in the dark again. Groaning at the aches and pains, he started walking toward the rocks. It was going to be a long day, but he had to do something to avoid thinking about the very possible future that he would die alone on the sands because of Tsubàyo and his own stupidity.

An hour later the sun had baked his skin, and he was sweating. Rutejìmo stripped off his shirt and draped it over his shoulders. He trudged along the top of a ridge, forcing each step through the sand that enveloped his feet. He ached and he was tired. His stomach gurgled uncomfortably, but there was nothing to eat.

He regretted leaving his pack in the tent. He berated himself for following Tsubàyo, leaving Mípu and Pidòhu. He also wished he had never watched the clan meeting that set them off on the trip. Everything would have been better if he had just remained an innocent boy.

Lost in dark thoughts, he almost missed the dépa racing across the ridge.

Rutejìmo gasped and trailed the bird's footsteps, but the bird was already gone. Frowning, he turned around, but he couldn't find where the dépa could have disappeared. The wind erased its trail, and he was once again lost.

He took a careful step forward, then another. When the dépa didn't reappear, he sighed. Unsure, he started to walk along the ridge again. This time, he kept his eyes out for the flash of feathers or the trail of the bird.

A flutter caught his attention.

He spun around as the dépa raced past him. He lost his balance and dropped to one knee. The scab along his leg tore open and hot blood dribbled down from the scratch. Clutching his wound, he looked up to see the dépa standing only a few feet away from him.

The feathered bird cocked its head. Tiny feet danced along the grains of sand as it stared at him.

Rutejìmo groaned and tried to stand up, but the pain slammed into him and he dropped back to his knees. "I can't."

The dépa took a step back, then forward.

"What do you want?" He felt foolish talking to the dépa, but it was better than talking to himself. Blood trickled through his fingers.

Moving sharply, the dépa spun around and raced a rod away before stopping. It turned around and cocked its head again.

Rutejìmo didn't move.

The bird paced back towards him and chattered. A moment later, it spun and raced back to the same spot and stopped.

"You," Rutejìmo's lips were dry as he spoke, "want me to follow?"

With a blur of movement, the dépa ran another rod and stopped.

Groaning, he pushed himself up to his feet. "What am I doing? How can I follow you? You're a bird!"

The dépa ran back and then away, repeating its movement.

He took a step and swayed from the pain. Fresh blood ran down his leg. He stopped. "No, I can't."

The bird took a step, and then it was gone.

Rutejìmo gasped and looked around, but he couldn't find it. "Wait! Where are you!?"

His voice was alone in the desert.

Rutejìmo whimpered and stared at the spot where the bird had been standing. Except for the little pits on the sand from its feet, there was nothing to indicate it had stood before him. There was something about the bird that drew him forward. It was more than the dépa being named for his clan. It was more than just a tiny creature standing in front of him.

He took a hesitant step after the bird's trail. Nothing changed, but it felt right. He stripped the shirt off his shoulders and tore off a long piece. He wrapped it around his leg as a bandage. He tied the remains back around his waist. With a deep breath, he took a step after the bird, then another.

The dépa didn't reappear, but Rutejìmo knew it wouldn't. He wasn't going fast enough, but the pain in his leg slowed him down. Whimpering, he forced himself to run faster, stumbling over the rocks as he struggled for enough speed to summon the bird.

He found his pace, far slower than anyone else in the clan except for Pidòhu, but he kept on running. Doubt warred in his mind, mixing with humiliation and embarrassment. He struggled with the urge to stop. His feet struck the sand with a steady rhythm, but each step was harder than the previous one.

And then the dépa was there; one moment, he was struggling to run alone, and the next the bird was a few paces ahead of him, matching his speed. It left no trail but he could follow it easily. The quiet scuffing of the dépa's feet was a contrast to the thudding of his soles.

He listened to the sound of the dépa. It had a rhythm to it footsteps. It was peaceful and encouraging. As when he ran the day before, his mind drifted and he lost himself in waves of euphoria. He stopped caring about his gasping breath, the stitch in his side, or even the wound in his leg. All that mattered was the running and keeping up with the nearly silent bird that was always a few steps ahead.

Before he knew it, he was running toward a familiar rock outcropping. Seeing the three tents in the morning light gave him hope, and he headed straight for it.

The dépa continued to pace right before him until he reached the rocks. When he slowed down, it did the same before disappearing between one step and another.

Rutejìmo came to a stop over a chain from the camp and looked back. He could see both trails in the sand, but one stopped where the dépa disappeared. He glanced back up at the camp, then bowed toward the end of the dépa's trail. "Thank you, Great Shimusògo."

He didn't know if it was actually the clan spirit, but there was no doubt that the dépa was more than a simple bird. Just in case, he didn't want to disrespect what could be the clan's spirit. He felt foolish bowing, but held it for a long count before straightening. There was a blush on his cheeks when he walked back into the campsite, his imagination already preparing him for Tsubàyo's sharp words or Karawàbi's insults.

As he circled around the nearest tent, he spotted Karawàbi sitting in front of his. The larger boy was drawing circles in the sand with his fingers. Judging from the squiggles and marks, he had been doing it for a number of hours.

Rutejìmo looked around, but didn't see Tsubàyo. "Wàbi, have you seen Tsubàyo?"

Karawàbi looked up sharply. "Jìmo? Where have you been?"

"I got lost out in the desert all night."

"Took you long enough to get back." Karawàbi scrambled to his feet. "Well, might as well start making breakfast. I'm hungry."

Rutejìmo came to a shuddering halt. "Excuse me? I just spent the entire night sleeping in the desert. Look at this!" He brandished his bandaged leg.

With a yawn, Karawàbi shrugged. "Don't really care what you've been doing. I'm hungry. So"---he cracked his knuckles---"either you start making food, or I'm going to beat the crap out of you."

Rutejìmo's stomach clenched. Balling his hands into fists, he turned and headed straight for the tents. He knelt heavily at the entrance of his tent, yanked open the flaps, and began to pack quickly.

Karawàbi's footsteps crunched on the ground. "What are you doing, Jìmo?"

"I'm going back to Mípu and Dòhu."

A heavy hand landed on his shoulder. Thick fingers dug into the sensitive part of his joints, and Rutejìmo winced at the pain.

Karawàbi turned him around. "I don't think so. We aren't going anywhere until Tsubàyo comes back."

"He's not coming back! He tried to steal a horse."

Karawàbi shrugged. "So?"

"We're Shimusògo! We don't ride horses."

"I don't see why not. They have four legs and big backs. Probably easy enough to get on one. They're animals and stupid." Karawàbi didn't seem upset or even concerned about Tsubàyo's theft.

Rutejìmo tried to pry Karawàbi's fingers from his shoulder. "Let me go."

"No." Karawàbi leaned forward. "You've been whining ever since we left the valley. I'm getting tired of it. Either you shut up and start doing what you're told, or I'm going to beat you until Tsubàyo comes back. And then"---he chuckled, and Rutejìmo winced at the hot breath washing across his face---"I'm going to keep beating you until he stops laughing."

Rutejìmo's arm grew numb as Karawàbi squeezed down. He whimpered and squirmed, but there was no escape from Karawàbi's grip. "Wàbi, let me go."

Karawàbi's fist caught him in the stomach. The impact was a dull thud that shook Rutejìmo to the core. With a chuckle, Karawàbi released his shoulder.

For the briefest of moments, Rutejìmo gaped in shock. And then pain exploded across his senses. It felt as though his insides had been ruptured from the blow. With a gasp, he bent over in agony and then dropped to the ground. His knees slammed into the sand. More pain shot up his legs. He slumped forward before catching himself with his good arm.

"Now, make my breakfast."

Rutejìmo sobbed with tears rolling down his cheeks. His stomach was in agony and his body on fire. The exhaustion of the previous night only added to his pains. He could barely focus on the sand underneath him and the wet patches wavered with every blink.

Through the agony, he realized that Karawàbi wouldn't stop. Now that he had attacked Rutejìmo, the brutality and bullying would continue. It would make his life hell.

He closed his mouth and struggled to calm his sobbing. Peering up, he saw the two packs by the tent. His was filled and closed but Pidòhu's remained open with its contents half spilled out across the ground.

Determination filled him. He planned his actions: grab the two packs and start running. It would be simple, and he could move faster than Karawàbi could respond, if he could move fast enough for the dépa to appear. He had no doubt that he could outrun Karawàbi.

Rutejìmo took a deep breath and launched himself off the ground.

Karawàbi's foot caught him in the ribs. The impact picked him up off the ground. He continued stumbling forward, his body in agony, and slammed face-first into the side of his tent. The thin fabric wrapped around his body, and he struggled to free himself before Karawàbi could kick him again.

He gasped for air and managed to pull himself out. The collapsed tent caught his foot, and it took precious seconds to rip himself free. Frantic, he scrambled to his feet, spun around, and held up his arms in preparation to defend himself.

"Thought you'd be going for that," said Karawàbi with a chuckle. He hooked Rutejìmo's bag with his foot and tossed it aside. The pack landed a few yards away in the middle of the sand. He gestured for Rutejìmo to come closer. "Come on, limp dick. Get ready for your beating."

Rutejìmo lunged over the tent with a snarl.

Karawàbi's backhand caught him across the face. The world exploded into sparks of pain as Rutejìmo staggered back. He tripped on the tent again and he fell. With a whine, he wrenched himself to the side and felt muscles tearing along his side and ribs. He slammed hard on the ground, and the impact left him dizzy.

"Idiot," muttered Karawàbi as he stalked closer. "You haven't beaten me in a fight since… well, ever. You might have grown some balls last night, but the rest of you is still a pathetic wimp."

Rutejìmo tried to crawl away, but his body wasn't responding. He was dizzy and nauseous from the blow. He clawed at the ground until he could get his hands and knees underneath him. Feeling Karawàbi walking closer, he dragged himself over the tent. The tent ropes tugged as his arms and legs as he made his way across the thin fabric. On the far side, his outstretched fingers slammed against a tent spike, and he pulled back his bruised hand with a hiss.

Karawàbi's shadow loomed over him. "Say good-bye, Jìmo."

Rutejìmo rolled over and looked up at Karawàbi. The other teenager's hands were balled into fists. His face was a mask of anger and cruelty, the same look Rutejìmo saw when Karawàbi was throwing rocks at Pidòhu. Deep inside, Rutejìmo knew Karawàbi wouldn't stop at a few bruises. He was going to hurt or even kill him.

Desperate, Rutejìmo grabbed the tent spike. His fingers cracked as he gripped it tightly and yanked it from the ground. With a scream, he sat up and slammed it down into Karawàbi's foot. The spike pierced flesh with only a token resistance. He didn't expect how easy it was to drive the spike clear through the foot; before he knew it, his hand smacked against his opponent.

With a gasp, Rutejìmo yanked his hand away from the spike. He glanced up at Karawàbi and then down at the spike. At first, nothing happened. And then crimson seeped up from the junction of metal and flesh. It pooled in the indention and then coursed down both sides of the foot before soaking into the sand beneath.

Karawàbi let out a long, gasping whine.

Trembling, Rutejìmo stared as a storm of emotions painted themselves across Karawàbi's face. For a moment, it looked as though the teenager was consumed with pain, but then rage took its place.

"I'm going to kill you!"

Crying out, Rutejìmo crawled backward and then stumbled to his feet. He sprinted for his bag, caught the leather strap in his palm, and then threw it over his shoulder. He glanced back, just in time to see Karawàbi lunging after him, the spike still sticking out of his foot.

With another yelp, Rutejìmo spun on his heels and ran off into the sand. "Please," he gasped, "Shimusògo, help me!"

The wind blew into his face, blinding him as he ran up one dune. Reflexively, he turned and followed the ridge, hoping to get away from Karawàbi. He had to go back, even to Mípu. "Please, to Mípu and Pidòhu, please!"

The shimusogo dépa raced up next to him, heading north along the sands. He didn't know if the bird knew where to go, but it wasn't in the same direction as Karawàbi. Praying he was following the clan spirit, Rutejìmo raced after the bird.
