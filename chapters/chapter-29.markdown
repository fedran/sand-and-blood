---
when:
  start: 1471/4/4 MTR 9::39
date: 2012-10-18
title: Rescue
locations:
  primary:
    - Pabinkue's Lost Tears
characters:
  primary:
    - Rutejìmo
  secondary:
    - Mikáryo
    - Tsubàyo
    - Chimípu
    - Desòchu
    - Gemènyo
    - Hyonèku
    - Pidòhu
    - Jyotekábi
    - Tateshyúso
    - Shimusògo
  referenced:
    - Funìgi (Epigraph)
    - Shimusògo
    - Tachìra
organizations:
  secondary:
    - Shimusògo
    - Pabinkúe
    - Tateshyúso
  referenced:
    - Ryozapòti
topics:
  secondary:
    - Kyōti Voting
  referenced:
    - Queen of the Chirodímu (Epigraph)
summary: >
  Rutejìmo had to wait until sunrise before anything else happened. The wait was torture because of his broken bones and concussion. When it did, it came with a windstorm that had a shape of Tateshyúso.

  It ended up being a brutal fight with Chimípu attacking Tsubàyo with speed and fire, Tsubàyo using the shadows and his horses, and Pidòhu using wind powers.

  At one point, Pidòhu falls from a stone arch but this time Rutejìmo caught him.

  When the fight ended, the rest of the Shimusògo elders arrived. With Tsubàyo alone, they were about to kill him but then Mikáryo arrived to claim him. There was about to be a fight, but then Rutejìmo stopped it by asking for a vote. After a moment of stunned silence, they did with the decision to allow Mikáryo and Tsubàyo to leave winning by a small margin.

  However, Desòchu and Chimípu asked for closure from Mikáryo who granted it. They beat Tsubàyo. Rutejìmo couldn't watch and ran away before they finished.
---

> In the end, only the blood on the ground measures a woman. --- Chirodimu Funìgi, *Queen of the Chirodímu* (Act 3, Scene 12)

The wait for sunrise was excruciating. The false dawn refused to become anything more than a razor-thin line of light along the horizon. The dunes remained black waves in the ocean of sand. It was as if time had become fixed in place and he was frozen between the grains falling through an hourglass.

His body, on the other hand, refused to stop shaking. The cracked ribs and broken arm throbbed painfully. If he remained still the bones didn't grind together, but then, sooner or later, a muscle would twitch and fresh waves of pain ripped through him. He suffered; it was the only thing he could do.

Rutejìmo wanted to run. He wanted to scramble to his feet and just shoot out in any direction. Even if he could somehow loosen the ropes, the horses would catch up with him. If he had sun, he could outrace them, but Shimusògo's power came from Tachìra and, without the sun, there would be no magic.

He whispered a prayer to Tachìra and Shimusògo to pass the time. It wasn't empty words anymore, but honest hope that he would see the sun against his face and feel the dépa racing at his feet.

"Oh, just stop muttering, Jìmo."

Rutejìmo glanced over to Tsubàyo. The rites of passage had worn down on all of them and left their mark, but Tsubàyo showed the burden more than even Pidòhu. He was thin and drawn. Rutejìmo hadn't seen him eat or even try to drink. Tsubàyo's eyes were dark and inset, hidden in shadows, as he glared around. Even in the alchemical flames, they were only a thin circle of white in the pitch-black gaze.

The only two things that reflected light were his blades. He had his spear and Rutejìmo's blade. He toyed with both as he watched his prisoner carefully.

"Mikáryo," he said with a hiss, "seemed rather kind to you."

Rutejìmo glanced away and shrugged his one good shoulder. "I guess."

"I hoped she would gut you right there."

He had to fight the urge to deny Tsubàyo's words. Thinking furiously, he lied as smoothly as he could. "She is a clan of night; who knows what they do." He squirmed as a muscle twinged in his arm. "For all I know, she was probably seeing if I was edible."

Tsubàyo chuckled. "Probably not. Maybe I just need to give you and Dòhu to her? Chimípu, I'm going to kill."

"You can't win against her."

Tsubàyo smiled, his face tilting into darkness. "Maybe, maybe not. There are a lot of secrets in these shadows." He gestured to the rocks underneath the arch. The outcroppings were barely visible in the pale light of the alchemical fire and the light along the horizon that refused to brighten.

"More horses?"

He smiled broadly. "I can feel them. There are a lot more coming."

"Why not use all of them earlier? You just had the---"

Tsubàyo's smile dropped sharply "Shut up!" His voice rang out from the rocks.

Rutejìmo said, "You can't, can you? It takes skill to keep the herd."

"What would you know?"

"We live less than a league from the Ryozapòti for decades. I've seen them care for their horses."

"Ryozapòti is a riding spirit, not a herd spirit."

"What's the difference?"

"It is…." Tsubàyo's voice trailed off as his lips parted for a moment. He shook his head. "It is like being surrounded by friends, but they are close. Like lovers, maybe?"

Rutejìmo thought of Mikáryo and blushed at the uncomfortable feeling that rose up inside him.

Tsubàyo shook his head again. "It feels good. When I'm riding, I don't worry about things."

"All the world just seems to melt away, and you are just moving. The pain, hunger, and doubt cease to exist. You are just moving and it feels like you are chasing something that loves you with all his heart."

Tsubàyo looked at him with surprise. "Yeah, but how… how did you know?" He frowned. "You are talking about Shimusògo?"

Rutejìmo nodded. "Pidòhu calls it rapture. Chimípu and I feel it for Shimusògo. Pidòhu feels it for Tateshyúso."

"And you think I can feel it for Pabinkúe?"

Rutejìmo shrugged with his good shoulder.

Tsubàyo stood up. He sheathed Rutejìmo's knife in his belt. "Maybe, but I'm not going to tell that Mikáryo bitch that. She thinks she knows everything, but I'll be damned if I'm just going to roll over for her."

Warmth spread out across Rutejìmo's body. He looked around in surprise, trying to find the source. His eyes scanned along the horizon as the first curve of the sun rose up over the horizon. Sunlight lit up the tips of the dunes. He gasped as he stared at the sun, he hadn't been sure he would see another sunrise.

Tsubàyo spoke without looking at Rutejìmo. "I hate that feeling. Every time Tachìra rises, I get this sick feeling in my gut."

"You can feel it too?"

Tsubàyo nodded.

Rutejìmo glanced up at the arch above him. Thin fingers of light caressed the rock. He thought he saw some movement, but it was too hard to make out anything in the darkness. Sighing, he looked around. They were near the center of the arch. In front of him were the endless waves of sand, behind him the shallow slope of broken rocks leading up to the cliffs.

"Well, Jìmo, enjoy the sunrise. It will be your last."

Rutejìmo shuddered at Tsubàyo's hard words. He focused on the horizon and watched the sun. It was bright as it welled up, stretching into the pale sky. He could imagine the sun spirit, Tachìra, was looking at him and seeing him for who he was.

He felt ashamed in the growing light. As much as he tried to do the right thing, he couldn't see how Tachìra would ever forgive him for hurting Pidòhu. He was nothing but a small sand tick in the desert, a tiny little thing, and he wondered if Tachìra even knew he existed. It was a humbling and depressing thought.

Shimusògo forgave him. The dépa had run with him, and he had felt the rapture of running. He smiled and relaxed. If he was going to die, at least he was going to die as a clan member, not someone who abandoned them.

More sun stretched across the sand, pooling on the tops of the dunes and sinking into the valleys. As if to make up for freezing time, the sun rose quickly and soon it was a quarter above the horizon.

Movement caught his attention. It was a plume of dust coming toward him. At the head, he could see a single runner. He couldn't focus on her from that distance, but his heart swelled knowing she was coming.

And then, shadows spread out from the dust behind her and stretched out. Massive wings sailed along the waves of sand, giving the impression of a bird larger than the mountain but invisible to sight.

Rutejìmo's breath caught in his throat.

A flash of power rose from the plume, and a large dépa appeared and faded. The movement accelerated, the plume turning into a spiraling spear of sand as Chimípu pulled away and charged.

Tateshyúso's shadow rushed forward and overtook her. Behind the shadow, wind yanked sand and dust from the desert and pulled it into the air. In a matter of seconds, it was boiling in a massive cloud rushing toward them.

The air grew tense, and a breeze pushed against Rutejìmo's face. He stared as the rolling cloud of sand charged the arch, kicking up stones and more sand until it was nothing but a wall of incoming storm.

His stomach clenched but he was smiling. Rutejìmo braced his feet on the ground and shoved himself up the rock. The movement sent fresh pains through his arm, but he wanted to be standing when the storm hit them.

Tsubàyo gasped. "What is that? Damn it, it's Mípu! To the shadows!"

The horses scrambled to their feet, but then the storm was on them.

Rutejìmo braced himself, cringing as he waited for the blast of air. The air howled around him, ripping at the ground. The sands below his feed rumbled from the ferocity of the winds that tore at the rocks and Tsubàyo. But, not a single grain hit his face. Surprised, Rutejìmo looked up to see the storm parting less than a foot from him. Turning, he saw he was in a bubble of calm air. The storm raged at the rock all around and behind him, but left him untouched.

To the side, he could see Tsubàyo fighting against the wind. It tore at his face and arms, pushing him back with force that ripped the cloth from his chest.

Someone grabbed Rutejìmo's broken arm. He jerked it back with a scream, almost losing his balance. He backed into the wind and felt it tearing at his back. The air shifted and he was once again the center of the calm area.

Burning with glowing flames, Chimípu stepped into the bubble of calm. "Jìmo?"

"Mípu!"

She pulled out a knife and grabbed his ropes. She sliced them away before pulling him into a tight hug. He groaned at the pain. "You're injured?"

Rutejìmo whimpered. "H-He broke my arm. And ribs."

She yanked away. "Sorry. I didn't see any blood."

"No, but it hurts to move, and I can't really feel my fingers."

Chimípu nodded and sheathed the dagger. Pulling out her tazágu, she toyed with the weapon. "Pidòhu won't be able to keep the storm up for long, so we need to hurry."

"What do I---"

"Run. Dòhu is safe from Tsubàyo, but you need to run away from here. I'll deal with Tsubàyo." She turned away from him as she tightened her grip on her weapon.

Rutejìmo stared at her back for a long moment, then he nodded. "Be safe, Mípu."

"Jìmo?" She looked over her shoulder. "That was very brave of you. Thank you."

He chuckled. "I can't tell you how scared I was."

"Tonight, you'll be the one telling stories."

Rutejìmo bowed. "Be safe, Great Shimusogo Chimípu." He turned toward the storm and stepped into it. He expected it to rip at his face, but the bubble of calm spread out. He gripped his broken arm to reduce the grinding and ran. He didn't need to see where he was going, he just ran.

A moment later, he was racing fast enough to feel Shimusògo, and the pain faded away. A heartbeat later, he was out of the storm and into the sunlight. The light never felt as good as it did in that moment.

He ran just to enjoy the moment of peace, but he couldn't leave Chimípu. Slowing down, he jogged in a wide circle before stopping a few chains from the outer edge of the sandstorm. The pains in his body came back, throbbing and agonizing, but he couldn't stay moving forever.

The cloud was dark, but he saw flashes of sunlight in the depths. He knew it was Chimípu fighting with Tsubàyo. He hoped she was safe against Tsubàyo and his herd.

And then he noticed there was someone else. They were perched on top of the stone arch, hands held above them. At first, he thought it was Mikáryo, but there was energy rising up from the figure. It looked as though waves of heat clung to the form, curling up like smoke before hitting some invisible force. His eyes watched the mirage-like shimmers roll along the shadow of a massive bird hovering above the arch. He had seen it before, but only in shadows.

"Tateshyúso."

Identifying the spirit, he knew there was only one person who could be on top of the arch. Pidòhu was balanced on the narrow rock, his legs stretched along the line of stone. He was chanting with his hands in the air, and the power rose through his fingers as the storm raged below him.

"H-How did he get there?" Rutejìmo was surprised. Then he remembered when Mikáryo had looked up in the darkness and made that cryptic comment. He smiled. It was a clue, one that both he and Tsubàyo had missed.

Rutejìmo looked around for something to use as a sling. He didn't know anything else to do. But he couldn't find anything to use as a shot. Frustrated, he expanded his search and jogged around.

He had run completely around the stone arch, over the rocks and back down into the sand, when the howling wind stopped. He skidded to a halt and looked down at the battle. The sand from the storm was raining down, and Tateshyúso's shadow was gone. He glanced up for Pidòhu.

The boy was still on the arch, but he was slumped forward. The last of the haze had faded from around him, and he looked thin. For a heartbeat, he was worried that Pidòhu had somehow passed out, but then the frail teenager lifted his head.

Blades crashing into each other dragged Rutejìmo's attention down. Chimípu was in the center of a fight, her body surrounded by golden flames as her tazágu flashed with every strike. She was fighting Tsubàyo and two of the horses. The three figures had her surrounded and were lashing out with spear, hooves, and teeth.

Chimípu danced amid the attacks. Her body blurred as she gracefully avoided the attacks. She missed more than she hit, but there was already blood staining the sand around them. Rutejìmo couldn't tell if it was from her or the others.

Rutejìmo whimpered and looked around. He was near the spot Chimípu assigned him the day before. He spotted a dark shadow and rushed over to it. It was one of the shots they made from their pack. He dropped to his knees and dug into the sand. He found two other bullets and one of the slings.

With a surge of elation, he used his one good hand to load a shot. He struggled to push both ends into his palm before standing up. Taking a deep breath, he gave a quick prayer to Shimusògo and began to spin. It was clumsy, working with one hand, but soon he was spinning fast enough for the dépa to circle him and sand to rise up. He felt the power coursing through his body.

He released the shot. It cracked the air as it rocketed toward the fight. The canvas ignited in the same golden flames that surrounded Chimípu. But, as it reached the midpoint, he realized it was going to miss.

"No!"

Chimípu stepped into the path of the shot.

He screamed at the top of his lungs. "Watch out!"

She didn't look up. Instead, she slammed her foot on the ground and spun around. It looked as though she was about to throw one of her own shots, but she didn't have a sling.

He flinched as the shot reached her, but Chimípu plucked it out of the air and rotated even faster. Her body turned into a column of light and wind. It was just a blinding light with a streak for the glowing shot and another for the dépa sprinting around her feet.

Chimípu released the shot, and it shone brilliantly as it rocketed from her hand. It was aimed for Tsubàyo, but both of the pitch-black horses threw themselves in front of him. The shot caught the first horse in the chest, and there was an explosion of sand and flames.

The shot burst out the far end of the sudden cloud and kept going. It shot out of sight in a blink. A moment later, a crack slammed into Rutejìmo, a deafening roar of something moving too fast.

The sand from the explosion hit the ground, but the fight had been interrupted. In the middle was Chimípu, crouched down as she glowed with power. It rolled off her body, rising up in waves of heat and flickering flames.

Tsubàyo was almost a chain away, at the end of a large gouge. One of the horses was scrambling to its feet from where it had shielded Tsubàyo with its body.

There was no sign of the other horse, not even blood or bone.

Tsubàyo swung himself up on the horse. Blood dripped down his face as he gripped his spear tightly.

Chimípu stood up with a glare on her face. She stepped forward, but Tsubàyo stopped her by pointing toward the arch. She peeked over her shoulder just as two horses charged out of the shadows.

Rutejìmo let out a groan. He thought she would have killed one of the horses, but it somehow escaped. He started for another of the shots, but then he saw more movement.

More horses were boiling out of the shadows, two, then four, and then six. They came out as a herd, spreading out as they charged toward Chimípu. Rutejìmo stopped counting at a dozen.

Chimípu burst into movement, sprinting away in a cloud of dust.

The herd followed after her, flowing along the sand like black water.

Wind howled around Rutejìmo as Chimípu stopped next to him. She was panting as she gave him a smile. "Thank you."

"H-How did you do that?"

Chimípu shrugged and held up her hands. "I'm making this up as I go. I have no clue, but it seems to work. I saw it missing and thought I would try. Never thought it would do that much damage." She stepped to the side and turned to watch the horses racing toward them both. "The horses are slow, though, and they can't get behind me if they don't have a shadow to enter."

Rutejìmo looked for Tsubàyo among the herd. When he didn't see the teenager, he looked back to the arch until he saw him. Tsubàyo was racing for the far end. "What is Bàyo doing?"

"I don't know." Chimípu sounded worried.

Tsubàyo rode past the end, but he came to a halt a few rods away. Rearing up, he turned his mount around.

"Mípu? I have a bad feeling."

Chimípu clenched her tazágu. "So do I."

With a yell, Tsubàyo spurred his horse into movement. The black mount charged toward the end of the stone arch.

Rutejìmo pointed. "Is Dòhu okay?"

"Of course. There is no way…." her voice trailed off as the horse reached the end of the arch.

Tsubàyo kicked hard, and the horse jumped up the arch. Its hooves struck the side of the stone in a shower of sparks. Instead of sliding back, the horse held its position. It made a small leap and jumped higher up on the steep rock, climbing.

"Sands!" Chimípu exploded into movement. She raced down toward the nearest end of the stone arch. It was a steeper climb, but Rutejìmo thought she was going to try the same thing.

She passed around the black herd. The horses split in half, spreading out as they circled around. Instead of chasing after Chimípu, they charged into each other. Rutejìmo held his breath as he watched with fascination. He couldn't imagine what they were doing.

The horses ran past each other. But, as they passed, the ones that were shadowed from the sun disappeared.

Rutejìmo gasped and looked up as the horses burst out of the darkness to block Chimípu.

She raced past them to head to the far end of the arch.

The horses raced in a sharp circle and dove back into the darkness. They came out in time to block Chimípu as she approached it.

Chimípu let out a scream of frustration and raced back to the first side, but the rest of the herd had covered the distance and blocked it. There was no way she could get on the arch without going through a gauntlet of horses.

Rutejìmo whimpered as he watched her run back and forth, trying to get past the horses. There was no other way up the arch.

Above, Tsubàyo was working his way up. His mount was struggling, ascending the stone in shorter jumps as it tried to climb the rock. Despite being a large creature with tiny hooves, it was making steady progress toward the peak. It would be only a few moments before Tsubàyo had Pidòhu.

Pidòhu noticed Tsubàyo and crawled back. His broken leg dragged behind him as he limped away, trying to keep his balance on the rock while avoiding the charging horse.

"Jìmo!" screamed Chimípu. "Throw me up!"

He stared at her in shock, but Chimípu sprinted in the opposite direction as she sheathed her tazágu. He didn't understand what she meant, but then he saw Shimusògo race past him. Despite his confusion, he knew how to follow his clan spirit. He sprinted after it, accelerating until the world blurred.

Chimípu had turned around and was rushing toward him. Her own dépa was just as fast as she glowed with brilliant flames. She was faster than he, but they would still meet in the middle of the arch.

And then he figured it out. Bearing down, he pushed himself to his limits. His body ached as he charged forward. As he ran past the horses guarding the end, he threw himself into a slide. The ground tore into his back as he dug through the sand and rock. He skipped across the sand and each impact drove the air from his lungs. He struggled to pull up his arm and leg to give her a platform to jump from.

Chimípu reached him and took a small hop. She landed on his arm and leg, the impact crushing him into the ground. She was searing-hot as her momentum froze a moment.

And with a liquid surge that brought golden flames coursing along his body, he shoved up with both arms and legs. The magic of the clan flowed through him, and he tasted feathers and blood in his mouth. His world turned into a single flash of agony as bones ground into each other.

Energy flared between them as Chimípu flew straight up in an explosion of air and flames. The sand howled after her, sucked up by her passing as she rocketed above Tsubàyo.

Rutejìmo screamed in agony as he was crushed into the desert. The impact had dug him into a crater. Every pulse of his heart sent throbbing pain coursing through his body. He sobbed at the agony, trying to breathe.

Chimípu hit the apex of her flight and came down. She landed hard on the back of Tsubàyo's horse. She grabbed his forehead from behind and slammed her fist into his back. The impact sounded like she had struck solid bone. She punched Tsubàyo hard and fast, and the dull thuds drowned out the pounding of Rutejìmo's heart.

Tsubàyo slumped forward, but then the horse reared back.

She lost her grip but grabbed Tsubàyo's torn shirt. Bouncing off the rump of the horse, she scissored her legs and kicked up. Releasing him, she flung herself up and around him to land on his chest. She kicked him hard in the face before grabbing the mane to hit him again.

The horse tried to clamp its teeth down on her arm.

Chimípu yanked her hand from the equine's mouth, and her blood arced into the air. She punched the horse, snatched hold of its ear, and kicked Tsubàyo again. Grabbing tighter to the creature's ear, she repeatedly slammed her heel into Tsubàyo's face.

Rutejìmo jerked with every wet, meaty thud of her foot.

Tsubàyo tried to parry with his spear, but Chimípu brought her knee down on it and cracked it in half.

The horse surged forward, jumping to its front feet and kicking out.

She made no noise as she lost her grip and tumbled off. For a moment, she slid off the side of the rock and her feet were dangling over open air. She managed to grab the jagged stone edge to stop her fall.

It was a hundred feet to the ground, and below her was nothing but sharp rocks.

Rutejìmo struggled to his feet, but he couldn't move fast enough. He kept struggling, unable to use his broken arm. The agony was intense, and the edges of his vision went red with pain.

Chimípu lost her grip with one hand. Her arm flung back, but instead of trying to get a new grip, she yanked the tazágu from her belt.

Tsubàyo's horse stepped up to her and lifted its hoof to crush her hand.

She surged up and punched the point of her weapon into the equine's leg. It pierced through the black flesh and came out the other side.

The horse's scream would haunt Rutejìmo's nightmares for the rest of his life. It jerked away from the pain and lost its balance. With sickening slowness, it tumbled off the rock and plummeted to the ground.

With her weapon still in the creature's leg, Chimípu was ripped off the stone. She made no noise as she fell after the horse.

Rutejìmo scrambled out of the crater. Everything hurt, but he couldn't let someone fall again. He raced as fast as he could. His broken arm flopped against his hip, and the agony tore through him, but he covered the distance in a heartbeat. Sliding to a halt, he held out his hand to catch Chimípu.

There was only a flash of darkness.

The horse slammed into the rocks next to him, a sickening crunch that shook the ground. He didn't dare look at the corpse as he stood up, trying to find her. He had missed her, had failed her. His mind kept reliving her fall, the memory of Pidòhu's plummet from the cliff overlaying the sight of her slipping, and he almost threw up.

A massive shadow flashed over him. The air rippled, and he saw translucent claws release Chimípu only a foot above the ground. She hit it hard, landing on her hands and knees. Panting, she froze as she tried to get her breath. Bright blood dripped from her face and it splattered on the sand. She was shaking as she tried to push herself up, but she stumbled and slumped back down.

She looked around, groaning as she tried to get up. "Where's Tsubàyo?"

Rutejìmo glanced around and then, with a sickening feeling, looked up.

Tsubàyo stood on top of the arch, his face lost in shadows. He had the broken end of his spear in one hand. "Looking for me, Mípu?" His voice was cracked and gasping.

Pidòhu was only a few feet away, still trying to crawl. The new bandage on his leg was stained crimson with fresh blood.

Chimípu looked up, a glare on her face.

"I'll be right down." The teenager turned and stalked toward Pidòhu.

"No," whimpered Rutejìmo.

But Pidòhu wasn't crawling back. He was on one knee and his hands.

There was a painful stillness in the air. Rutejìmo could hear every footstep as Tsubàyo walked over to Pidòhu.

"Time to die, Dòhu."

Pidòhu shook his head, but there were no tears or fear on his face. Instead, there was only determination. Ripples of power rose off him.

Rutejìmo gasped and looked around. He stood up and he looked for Tateshyúso's shadow.

The massive bird was coming in from the south, the darkness flowing across the sands. And, barely visible in the darkness, was the wind. It wasn't a howling wall but a single line of power forming a spear of swirling sand and rock.

Chimípu groaned and tried to push herself up. "J-Jìmo, I need to get up there."

Rutejìmo shook his head. "No."

"Jìmo! You can't---"

"Mípu"---he gulped---"this is Pidòhu's fight."

"He can't---"

"Tateshyúso's shadow."

On top of the arch, Tsubàyo didn't respond to the shadow as it rushed toward him. Instead, he lifted up his spear and stepped over Pidòhu.

The shadow washed over Rutejìmo and Chimípu. It blotted out the arch as the bird spirit raced past.

And then the wind hit. It was a stream of force that slammed into only Tsubàyo. The sand tore at his side and ripped his spear from his hand. He gasped and turned into it, bracing himself, but it continued to rip at him. Power rippled along the stream and it grew stronger, blasting sand against Tsubàyo's face and body. It tore at his clothes. Rocks slashed past him, and streaks of blood poured into the streaming sand.

"Goodbye, Tsubàyo," Pidòhu said, his voice clear over the wind.

The increased impact of the wind sounded like a punch, and Tsubàyo was thrown off the rock. He screamed as he plummeted to the ground on the far side of the arch. The impact was the same sound Pidòhu had made when he hit the ground, a thud that shook Rutejìmo to the core.

On top of the rock, Pidòhu swayed as the wind cut off.

Chimípu groaned. "Jìmo?"

Rutejìmo surged to his feet. "I'll get him."

He was running even as Pidòhu tumbled off the edge. It was a short fall, no more than a rod or two, but Rutejìmo was there to catch him. The impact crushed them both into the sands. One of Rutejìmo's knees cracked against some rocks. Pidòhu slipped from his arms and hit the ground, but it was a hard blow on the rocks below them instead of a fatal one.

Rutejìmo slammed face-first into the rocks. He sobbed at the pain, flailing with his one good arm as he flipped himself over. "Dòhu? Dòhu!?"

"Yeah," groaned Pidòhu next to him. "You caught me that time. You caught me."

"Y-Yeah?"

"Did that hurt you as much as me?"

Rutejìmo gasped at the sharp burning coursing up his legs. His arm refused to move and his vision was blurred with tears. "Yes. Let's not do that again."

"Deal. Where is Mípu?"

Struggling, Rutejìmo pushed himself into a sitting position. Every part of his body hurt. He wanted to crawl into a hole and never get out again.

Chimípu was twenty feet away, crouching over Tsubàyo's form. She had her tazágu out and held ready to strike. The point was bright on the nameless blade.

"Sands, she's going to kill him." He felt the bile rising up. He couldn't watch.

Rutejìmo, thankful for an excuse to look away and praying she would kill Tsubàyo before he looked back, reached out to help Pidòhu into a sitting position. He let his hand linger, delaying the inevitable.

But he had no luck. When he turned around, she was still poised to strike. Her arm shook with exhaustion. Blood soaked her shoulder. She was covered in cuts and bruises.

Tsubàyo grabbed for her tazágu.

She snatched his wrist and slammed it down on the ground. Their bodies shuddered as they stared at each other, panting for breath with hatred in their eyes.

The silence stretched between them, interrupted only by their rapid breathing. Both of them were glowing, but it was the banked fires of the power inside them. It shimmered in the air around them, rippling like a mirage.

Rutejìmo was afraid to make a noise. He didn't want to set off more fighting. He just wanted it to end. He couldn't handle death right in front of him, not after everything else.

Chimípu jerked her hand up. "Damn you!" she screamed and slammed her weapon down.

Rutejìmo jerked and clamped his eyes shut. He waited for the scream or yell, but there was nothing. Tentatively, he opened his eye, fearing the worst.

Chimípu was crouched over Tsubàyo, panting heavily. Her shoulders rose and fell as sobs tore through her.

"You," Tsubàyo laughed from underneath her, "are just as bad as---"

She sat up and slammed him across the jaw with her fist.

Tsubàyo's hand reached up to block her.

Chimípu knocked his arm aside and punched him again. Tears ran down her cheeks as she struck again and again. The dull impacts of her hammering fists shot straight through Rutejìmo. He was horrified, but he couldn't stop her.

The wind rose up around them, kicking up streamers of sand that curled between the two.

A hand pried Rutejìmo's hand away from Pidòhu. "Let me have Great Tateshyuso Pidòhu, Great Shimusogo Rutejìmo." The speaker was an old woman with a cracked voice.

Rutejìmo gasped and spun around. He stared in shock until recognition dawned. Tateshyuso Jyotekábi crouched next to him, the old woman's thin robe doing nothing to shield her old body from his sight. Despite her thin build, she was strong enough to scoop her hands underneath Pidòhu and lift him.

Pidòhu looked at him, his face showing his pain. "Tateshyúso?"

Rutejìmo glanced around and saw the rest of the clan coming from all directions. Plumes of sand and dust billowed as they raced across the sands, their bodies blurred with their speed and the heat. He could see Shimusògo leading all of them, the delicate-looking dépas sailing across the ground.

He turned as a sob rose in his throat. It was over. The first of the runners reached the two fighters.

The wind crashed into all of them, and the dust swallowed up Chimípu and Tsubàyo. It sank to the ground almost instantly, and Desòchu stood there, holding Chimípu's arm as he looked down at her. "Enough, Chimípu."

Chimípu looked up, tears in her eyes and a torn look on her face. "D-Desòchu?"

He nodded and held out his hand. "Yes."

"H-He tried to kill Dòhu and Jìmo. I-I had…." She rose up. "I had to save them."

"I know," Rutejìmo's brother said, his voice filled with warmth.

Rutejìmo felt a surge of jealousy. He looked down as the rest of the clan arrived. The air crackled with their power, the clan's magic.

Gemènyo knelt down next to Rutejìmo. "You okay?"

Rutejìmo looked up, feeling tears in his own eyes. He shook his head.

With a sympathetic look, Gemènyo patted him lightly on the shoulder.

Rutejìmo winced at the pain.

"Broken arm?"

Rutejìmo nodded and let out a whimper. He felt broken and exhausted. It didn't seem real that the clan was there. "And my ribs."

"And," said Hyonèku as he knelt down on the other side, "you tore up your back. But, that was a nice maneuver to get Mípu up on the stone. You did good, Jìmo."

Startled, Rutejìmo stared in shock. He hadn't been expecting a compliment.

Hyonèku grinned and pulled out a healer's kit. Tossing a few rolls of bandages over to Gemènyo, he gestured for Rutejìmo to raise his arm. "Don't look surprised, boy. Now, don't look at Mènyo, either; this is going to hurt."

Rutejìmo felt Gemènyo easing his arm up as he wrapped the bandage around it. As the runner did, he was testing the wound. Rutejìmo winced at the pain, but kept his mouth shut. He watched as two of the clan runners pulled Tsubàyo to his feet.

Tsubàyo's face was swollen and bruised. Blood trickled down from his nose and one eye. He glared at Chimípu and Desòchu. "Now what?"

Hyonèku spoke up sharply without looking away from Rutejìmo. "Kill him. End it here."

Assent rippled through the gathered clan.

Desòchu held up his hand. "Anyone disagree?"

Silence.

Tsubàyo looked back and forth. There was fear in his eyes.

"Actually," Mikáryo said as she stepped out of the shadows, "I might want a say in this." She was wrapped in darkness, but the cloth was pulled back from her head. Her dark tattoos seemed to suck in the sunlight as she strolled up to the gathered clan.

Growls and hisses filled the air, followed by the rasp of weapons being drawn.

Mikáryo looked around, unperturbed by the drawn weapons. "So many threats against one person. Typical for the clans of day when they stand in Tachìra's light. I am Mikáryo and I speak for Pabinkúe."

Desòchu stepped forward. "I am Desòchu and I speak for Shimusògo."

From behind Rutejìmo, Jyotekábi announced, "I am Jyotekábi and I speak for Tateshyúso."

Mikáryo bowed once to Desòchu and then to Jyotekábi. She pointed to Tsubàyo, who stared at her with a strange combination of anger and hope. "That one is Pabinkúe's, and I'm here to collect him."

One of the other clan adults stepped forward. "I say kill both of them. Rid the sands of the night."

More agreed as they brandished their weapons.

Rutejìmo stared at them and cringed under the hatred rolling off them. He looked at Chimípu, who looked torn, and then at Pidòhu.

Pidòhu clutched to Jyotekábi's body, his face pale and fresh blood dripping from his leg to the sand beneath him.

Jyotekábi pulled him tight to her body as energy rippled around her. Behind them, the broad shadow of Tateshyúso sailed across the sand toward the two. "Great Shimusogo Desòchu, Tateshyúso has no interest in this and Great Tateshyuso Pidòhu needs help. We will meet you at the valley."

Desòchu nodded but didn't take his eyes off Mikáryo. There was a frown on his face, but unlike the others, he had no drawn weapon.

The shadow of Tateshyúso sailed over the clan, leaving behind a ripple of coolness. When it passed, Pidòhu and Jyotekábi were gone.

Mikáryo smiled, her lips curling back until her teeth were visible. "Do you really want to spill more blood, Great Shimusogo Desòchu?"

Another person said, "There are only two of them."

"Yes," Mikáryo purred, "only two of us."

From behind her, the shadows of the arch deepened and turned into pitch-black voids. A coolness rippled from the darkness as a horse stepped out in perfect silence. Behind it, more horses stepped out.

Rutejìmo inhaled as he watched the herd walk out of the darkness. He caught movement to the side and looked. The shadows cast by the surrounding clan were also pitch-black. A horse pulled itself out of a narrow strip as if it was crawling out of a hole. It moved in perfect silence as it turned around.

Gemènyo inhaled sharply. "Behind you!"

The clan spun around. As one, they took a double take at the twenty horses surrounding them. The black bodies were silent but there was no doubt of the threat.

Rutejìmo pushed away from Gemènyo and Hyonèku and scrambled to his feet. "Stop!"

Everyone froze. Mikáryo smiled and cocked her head as the others turned to look at Rutejìmo. There were bared weapons, but Rutejìmo couldn't stomach the idea of watching more blood fall on the sand, not Tsubàyo's or anyone's.

He was the center of attention and he felt ashamed.

And then he saw the corner of Desòchu's lip curled into a smile.

Rutejìmo gulped and held up his good hand. "P-Please? No more fighting. No more blood. Let both of them go."

Hyonèku stood up. "No, kill her!" He stepped away from Rutejìmo with a glare.

Desòchu looked back and forth.

Rutejìmo whimpered and then fumbled with his pocket, digging until he found his voting stone. In front of others, he knew he only deserved to use one. He shook as he pulled it out and held it in his palm. "Please?"

Turning on Rutejìmo, Hyonèku hissed, "What are you doing? This isn't a vote---"

A stone hit the ground at Rutejìmo's feet. It was Chimípu's. Rutejìmo stared down at it, a shiver coursing through his body. He was dizzy and sick to his stomach.

Someone tossed a handful of stones at Hyonèku. More landed in front of Rutejìmo, thrown by someone he didn't see. More stones smacked the ground at Hyonèku's feet. They littered the sands in front of him.

Rutejìmo stared down at the two piles. There were far more in front of Hyonèku than him.

Gemènyo poured over twenty rocks at Rutejìmo's feet. He straightened up and patted Rutejìmo on the shoulder. "You have balls, at least." He chuckled. "Stupid, but you have balls." He kicked one of his rocks in front of Hyonèku before stepping back.

Silence crushed against Rutejìmo. He wanted to crawl away but couldn't. Fighting back his fear, he stood up straight and turned to his brother.

Desòchu stood next to Chimípu, hefting a handful of his own voting stones in his hand. The rocks clinked as he rolled them in his palm. With a sigh, he tossed them in the air.

Rutejìmo flinched as they landed in the sand at his feet.

"She lives just as Tsubàyo will," announced his brother.

Tsubàyo let out his breath in a low gasp. His chest rose and fell as he stared at the rocks.

Hyonèku hissed and gathered up his stones. He turned his back on Rutejìmo and walked away. The other clan members came up to gather their rocks. Some of them patted Rutejìmo on the shoulder as they passed, but most moved in silence as they followed after Hyonèku.

Gemènyo gathered up his voting stones and stood. He handed Rutejìmo his single voting stone.

Rutejìmo took it, unsure of what to do.

Gemènyo leaned into him. "Stay as long as you want, but then join us."

He turned and jogged away. A moment later, the wind rushed as the clan sprinted off towards home.

Rutejìmo turned back. There were only five of them left. Even the horses had disappeared back into the shadows.

Desòchu looked around, his face grim. He bowed to Mikáryo. "I request a favor, Great," he choked on the word, "Pabinkue Mikáryo. He attacked my clan as one of yours. May Shimusògo get closure on the one who abandoned us?"

He spoke not as Rutejìmo's brother but as a warrior of the clan. Rutejìmo felt as if he had just lost his sibling.

Mikáryo chuckled. She rested her hand on the tazágu. She said nothing for a moment.

Tsubàyo looked at her, confusion on his face.

She gave a short nod. "Don't break any bones. We have a long ride."

Tsubàyo turned back to Desòchu, confusion naked in his expression. "What is she---?"

Chimípu's fist caught him across the face. The smack cracked the air.

Tsubàyo staggered to the side, but Desòchu was there. Growling, Desòchu backhanded Tsubàyo and threw him back into Chimípu's other fist.

Rutejìmo jerked as the punch caught Tsubàyo.

Tsubàyo flailed around in an attempt to block, but Desòchu's next strike hit him in the stomach, folding him over.

Chimípu grabbed Tsubàyo's head and slammed her knee into his face. He staggered back to Desòchu, who grabbed Tsubàyo with his right hand and punched him hard across the chin. A tooth flew out across the sand.

Tsubàyo slammed back on the ground. Fresh blood was pouring down his face. He looked up at Mikáryo. He reached out with a shaking hand. "Save me!"

Mikáryo shook her head. "This is a lesson, Bàyo," she spoke in a hard voice. "This time, try to learn it."

Chimípu grabbed Tsubàyo and yanked him to his feet. He tried to pull back, but Desòchu slammed into his back. They alternated punching and kicking him. As they moved, the impacts grew louder. Golden flames grew around Desòchu and Chimípu as they attacked in perfect synchronization.

Rutejìmo was crying. He felt no joy as they brutalized Tsubàyo. For all the pain and bullying he went through, he couldn't take it. Sniffing and wiping at the tears on his cheeks, he stepped back away from the fight.

Mikáryo caught his eye. She gave him a small nod of approval, then blew him a kiss.

The beating continued with brutal efficiency. The strikes rocked the air as Tsubàyo cried out, interrupted only by the sound of fist on flesh or the sound of a kick slamming into him. He was helpless against the Shimusògo warriors as they took their revenge for Rutejìmo and Pidòhu.

Bile rising in his throat, Rutejìmo turned and fled.
